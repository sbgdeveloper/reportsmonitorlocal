<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Apimodel');
    }

    public function fetchreports() {
        $search = $_POST['search'];
        $publisher=$_POST['publisher'];
        $reports = $this->Apimodel->posts_search_m($search,$publisher);
        if ($reports) {
            $rData = $reports;
        } else {
            $rData = $this->Apimodel->posts_search_mm($search,$publisher);
        }
        echo json_encode($rData);
    }

}
