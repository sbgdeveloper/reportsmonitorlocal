<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('data_model');
        $this->load->model('paypalIPN');
    }

    public function index() {
        $data['page'] = "Global Market Research and Industry Analysis";
        $data['page_header'] = "page-header";
        $data['letestReports'] = $this->data_model->letestReports();
        $data['Record'] = array(
            'meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts",
            'meta_desc' => "Reports Monitor is one of the leading market research organizations engaged in providing analytics, optimal quality research, and advisory services to clients."
        );
        $this->load->view('web/home', $data);
    }

    public function about() {
        $data['page'] = "Global Market Research Company Offering Qualitative and Quantitative Research";
        $data['page_header'] = "page-header1";
        $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
        $data['Record'] = array(
            'meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts",
            'meta_desc' => "At Reports Monitor, we guarantee a future-oriented outlook, high-quality conceptualization, and elucidation skills on all of our customer's research needs."
        );
        $data['Record']['canonical'] = base_url() . 'about-us';
        $this->load->view('web/about', $data);
    }

    public function contact() {
        if ($_POST) {
            if (strpos($_POST['msg'], 'http') !== false) {
                redirect($_SERVER['HTTP_REFERER']);
            }
            $ip = $this->input->ip_address();
            /* if ( isset( $_SERVER[ "HTTP_X_FORWARDED_FOR" ] ) ) 
              {
              $_SERVER[ 'REMOTE_ADDR' ] = $_SERVER[ "HTTP_X_FORWARDED_FOR" ];
              }
              $ip =$_SERVER[ 'REMOTE_ADDR' ] ;
             */
            // $ip = $this->session->userdata('ip_address');
              $captcha = $this->input->post('g-recaptcha-response');
              $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcvGGYUAAAAADEFOJdLq6VfOiAcotlFSEkdHaAk &response=" . $captcha . "&remoteip=" . $ip);
              if (!empty($captcha)) {
                if ($response . 'success' == false) {
                    $this->session->set_flashdata('flashSuccess', 'Sorry Google Recaptcha Unsuccessful!!');
                } else {
                    $to = $_POST['email'];
                    $subject = 'Thank you for Contacting';
                    $message = '<p>Dear ' . ucfirst($_POST['name']) . ',</p>';
                    $message .= '<p>Thank you for contacting Reports Monitor.</p>';
                    $message .= '<p>We have received your message and our representative will reach out to you at the earliest.<br/>Meanwhile, if you have any specific concerns/questions, please drop a line at <br/>sales@reportsmonitor.com <br/>Have a good day ahead!</p><br/>';
                    $message .= '<p><b>Best Regards,</b><br/>';
                    $message .= 'Reports Monitor Team<br/>';
                    $message .= 'US: +1-513-549-5911<br/>';
                    $message .= 'UK: +44 203-318-2846<br/>';
                    $message .= 'India: +91 808-708-5354<br/>';
                    $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

                    $this->data_model->sendMail($to, $subject, $message);

                    $to = 'sales@reportsmonitor.com';
                    // $to = 'azar@straitsresearch.com';
                    $subject = 'Contact Us Form:';
                    $message = '<b>Dear Admin,</b><br/>';
                    $message .= '<table border = "1">';
                    $message .= '<tr><td>Name</td>';
                    $message .= '<td>' . $_POST['name'] . '</td></tr>';
                    $message .= '<tr><td>Email</td>';
                    $message .= '<td>' . $_POST['email'] . '</td></tr>';
                    $message .= '<tr><td>Phone</td>';
                    $message .= '<td>' . $_POST['phone'] . '</td></tr>';
                    $message .= '<tr><td>Job Title</td>';
                    $message .= '<td>' . $_POST['job-title'] . '</td></tr>';
                    $message .= '<tr><td>Company</td>';
                    $message .= '<td>' . $_POST['company'] . '</td></tr>';
                    $message .= '<tr><td>Subject</td>';
                    $message .= '<td>' . $_POST['sub'] . '</td></tr>';
                    $message .= '<tr><td>Message</td>';
                    $message .= '<td>' . $_POST['msg'] . '</td></tr>';
                    if ($_POST['source']) {
                        $message .= '<tr><td>Source</td>';
                        $message .= '<td>' . $_POST['source'] . '</td></tr>';
                    } else {
                        $message .= '<tr><td>Source</td>';
                        $message .= '<td>Reports Moniter Contact Form</td></tr>';
                    }
                    $message .= '<tr><td>IP</td>';
                    $message .= '<td>' . $ip . '</td></tr>';
                    $message .= '</table></p>';

                    $this->data_model->sendMail($to, $subject, $message);
                    $this->session->set_flashdata('flashSuccess', 'Google Recaptcha Successful');
                }
            } else {
                $this->session->set_flashdata('flashSuccess', 'Not clicked');
            }
            redirect(base_url() . 'thankYou');
        } else {
            $data['page'] = "Contact Us";
            $data['page_header'] = "page-header1";
            $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
            $data['Record'] = array('meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts, market intelligence",
                'meta_desc' => "Reports Monitor: Please fill out the form below to enable our research representatives to contact you at the earliest."
            );
            $data['Record']['canonical'] = base_url() . 'contact-us';
            $this->load->view('web/contact', $data);
        }
    }

    public function services() {
        $data['page'] = "Global Market Analysis, Business Research and Market Intelligence";
        $data['page_header'] = "page-header2";
        $cond = array('title' => 'Services');
        $tbls = NULL;
        $fields = array('page_content');
        $data['Records'] = $this->data_model->get($cond, $tbls, $fields, NULL, 'pages')[0]['page_content'];
        $data['Record'] = array('meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts, market intelligence",
            'meta_desc' => "Research Monitor not only strives to provide the best quality customer service to every one of the customers but also continuously improving with regards to the integration of newest technologies into our best practices and ensuring every one of our staff members is extremely qualified in the use of these technologies."
        );
        $this->load->view('web/services', $data);
    }

    public function termsConditions() {
        $data['page'] = "Terms and Conditions";
        $data['page_header'] = "page-header2";
        $cond = array('title' => 'terms');
        $tbls = NULL;
        $fields = array('page_content');
        $data['Record'] = $this->data_model->get($cond, $tbls, $fields, NULL, 'pages')[0]['page_content'];

        $this->load->view('web/terms', $data);
    }

    public function privacyPolicy() {
        $data['page'] = "Privacy Policy";
        $data['page_header'] = "page-header2";
        $cond = array('title' => 'policy');
        $tbls = NULL;
        $fields = array('page_content');
        $data['Record'] = $this->data_model->get($cond, $tbls, $fields, NULL, 'pages')[0]['page_content'];

        $this->load->view('web/policy', $data);
    }

    public function return_policy() {
        $data['page'] = "Return Policy";
        $data['page_header'] = "page-header2";

        $this->load->view('web/return_policy', $data);
    }

    public function disclaimer() {
        $data['page'] = "Disclaimer";
        $data['page_header'] = "page-header2";

        $this->load->view('web/disclaimer', $data);
    }

    public function format_delivery() {
        $data['page'] = "Format and Delivery";
        $data['page_header'] = "page-header2";

        $this->load->view('web/format_delivery', $data);
    }

    public function how_to_order() {
        $data['page'] = "How To Order";
        $data['page_header'] = "page-header2";

        $this->load->view('web/how_to_order', $data);
    }

    public function categories($cat_id = 0, $title = "", $page = 1) {
        if ($_POST) {
            print_r($_POST);
        } else {
            $per_page = 6;
            $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

            $data['page'] = "Global Market Research Reports, Market Survey, and Market Segmentation";
            $data['page_header'] = "page-header1";
            $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name', 'img'), NULL, 'category');
            $data['regions'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'regions');
            if ($cat_id == 0) {
                $data['key_desc'] = "yes";
            }
            $res = $this->data_model->get(array('cat_id' => $cat_id), $tbls, array('report.id', 'title', 'report_desc', 'meta_keyword', 'published_date'), NULL, 'report');
            if ($res) {
                $total = count($res);
            } else {
                $total = 0;
            }

            $limit = array('limit' => $per_page, 'start' => ($per_page * $page) - $per_page);

            if ($cat_id != 0) {
                $Record = $this->data_model->get(array('id' => $cat_id), NULL, array('id', 'name', 'description', 'img', 'meta_keyword', 'meta_desc'), NULL, 'category')[0];
                $data['Records'] = $this->data_model->get(array('cat_id' => $cat_id), $tbls, array('report.id', 'title', 'meta_keyword', 'report_desc', 'published_date'), $limit, 'report', 'desc');
                $data['Record'] = $Record;
                $data['page'] = $data['Record']['meta_keyword'];
                $data['Record']['canonical'] = base_url() . 'categories/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['name']);
            }

            if ($total % $per_page == 0) {
                $data['pages'] = floor(($total / $per_page));
            } else {
                $data['pages'] = floor(($total / $per_page)) + 1;
            }

            if ($total == 0) {
                $data['start'] = 0;
            } else {
                $data['start'] = $limit['start'] + 1;
            }

            if ($total < $limit['start'] + $per_page) {
                $data['end'] = $total;
            } else {
                $data['end'] = $limit['start'] + $per_page;
            }

            $data['total'] = $total;
            $data['current'] = $page;
            $data['page_name'] = 'Categories';
            // print_r($data['Record']);
            $this->load->view('web/category', $data);
        }
    }

    public function report($id, $keyword = '') {
        if ($_POST) {
            if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@ssemarketing.net"){
                redirect(base_url() . 'thankYou');
            }
            if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@comcast.net"){
                redirect(base_url() . 'thankYou');
            }
            if($_POST['phone']=="+1 213 425 1453"){
                redirect(base_url() . 'thankYou');
            }
            $ip = $this->input->ip_address();
            $res = file_get_contents('https://www.iplocate.io/api/lookup/' . $ip);
            $res = json_decode($res);


            if (isset($_POST['country'])) {
                $country = $_POST['country'];
            } else {
                $country = $res->country;
            }
            // curl call for CRM
            $Array['name'] = $_POST['name'];
            $Array['mail'] = $_POST['email'];
            $Array['job_title'] = isset($_POST['job_title']) ? $_POST['job_title'] : "NA";
            $Array['company'] = isset($_POST['company']) ? $_POST['company'] : "NA";
            $Array['phone'] = $_POST['phone'];
            $Array['country'] = $country;
            $Array['message'] = isset($_POST['message']) ? $_POST['message'] : "Enquire Before Buying";
            $Array['report'] = isset($_POST['report']) ? $_POST['report'] : "NA";
            $Array['region'] = $res->continent;
            $Array['ip'] = $ip;
            $Array['website'] = "reportsmonitor.com";
            $Array['source'] = "Enquire Before Buying";
            $url = "http://165.227.205.30/addLead";
            $postData = $Array;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $output = curl_exec($ch);
//            print_r($output);die();
            curl_close($ch);

            $to = $_POST['email'];
            $subject = 'Enquiry:' . $_POST['report'];
            $message = '<p>Hi ' . ucfirst($_POST['name']) . ',</p>';
            $message .= '<p>Thank you for showing interest in this report.</p>';
            $message .= '<p>Our Sales representative will get in touch with you at the earliest and assist you with the purchase of this report.<br/>Meanwhile, if you have any specific concerns/questions, please drop a line at <br/>sales@reportsmonitor.com <br/>Have a good day ahead!</p><br/>';
            $message .= '<p><b>Best Regards,</b><br/>';
            $message .= 'Reports Monitor Team<br/>';
            $message .= 'US: +1-513-549-5911<br/>';
            $message .= 'UK: +44 203-318-2846<br/>';
            $message .= 'India: +91 808-708-5354<br/>';
            $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

            $this->data_model->sendMail($to, $subject, $message);

            $to = 'sales@reportsmonitor.com';
            // $to = 'azar@straitsresearch.com';
            $subject = 'Enquire Before Buying:' . $_POST['report'];
            $message = '<b>Dear Admin,</b><br/>';
            $message .= '<table border = "1">';
            $message .= '<tr><td>Report Title</td>';
            $message .= '<td>' . $_POST['report'] . '</td></tr>';
            $message .= '<tr><td>Name</td>';
            $message .= '<td>' . $_POST['name'] . '</td></tr>';
            $message .= '<tr><td>Email</td>';
            $message .= '<td>' . $_POST['email'] . '</td></tr>';
            $message .= '<tr><td>Phone</td>';
            $message .= '<td>' . $_POST['phone'] . '</td></tr>';
            $message .= '<tr><td>Source</td>';
            $message .= '<td>Reports Moniter Enquire Before Buying</td></tr>';
            $message .= '<tr><td>IP</td>';
            $message .= '<td>' . $ip . '</td></tr>';
            $message .= '</table></p>';

            $this->data_model->sendMail($to, $subject, $message);

            redirect(base_url() . 'thankYou');
        } else {
            $cond = array('report.id' => $id, 'report.status' => "1");
            $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
            $fields = array('report.id', 'title', 'report_desc', 'cat_id', 'publisher_id', 'region_id', 'toc', 'list_tbl_fig', 'published_date', 'pages', 'single_price', 'multi_price', 'ent_price', 'published_status', 'meta_title', 'meta_desc', 'meta_keyword');
            $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'report')[0];
            $data['Record'] = $Record;

            $data['page'] = $Record['meta_title'];
            $data['page_header'] = "page-header4";
            $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');

            $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

            $limit = array('limit' => 5, 'start' => 0);

            $data['Related'] = $this->data_model->get(array('cat_id' => $Record['cat_id'], 'report.id !=' => $Record['id']), $tbls, array('report.id', 'title', 'report_desc', 'meta_keyword', 'published_date'), $limit, 'report', 'desc');
            /* Added For Canonical Tag */
            $data['Record']['canonical'] = base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
            /* End Canonical Tag */
            if ($Record) {
                $this->load->view('web/single_report', $data);
            } else {
                redirect(base_url(), 'refresh');
            }
        }
    }

    public function thankYou() {
        $data['title'] = "Thank you";
        $data['page'] = "Thank you";
        $data['page_header'] = "page-header2";
        $this->load->view('web/thankYou', $data);
    }

    public function publishers() {
        $data['page'] = "Publishers";
        $data['page_header'] = "page-header2";
        $cond = NULL;
        $tbls = NULL;
        $fields = array('*');
        $data['Records'] = $this->data_model->get($cond, $tbls, $fields, NULL, 'publisher');
        $data['Record'] = array(
            'meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts",
            'meta_desc' => "Reports Monitor features business market research reports from various leading publishers. Visit us for quality industry analysis and consulting solutions."
        );
        $this->load->view('web/publishers', $data);
    }

    public function publisher($id, $page = 1) {
        $per_page = 6;
        $data['page_header'] = "page-header1";
        $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
        $cond = array('id' => $id);
        $tbls = NULL;
        $fields = array('id', 'name', 'img');
        $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'publisher')[0];
        if (!isset($Record['name'])) {
           redirect(base_url() . 'publishers');
       }
       $data['Record'] = $Record;
       $data['Record']['canonical'] = base_url() . 'publisher/' . $id;

       $data['page'] = $Record['name'];
       $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
       $limit = array('limit' => $per_page, 'start' => ($per_page * $page) - $per_page);
       $Records = $this->data_model->get(array('publisher_id' => $id), $tbls, array('report.id', 'title', 'meta_keyword', 'report_desc'), $limit, 'report', 'desc');
       $res = $this->data_model->get(array('publisher_id' => $id), NULL, array('id', 'title', 'report_desc'), NULL, 'report');
       $data['Records'] = $Records;

       if ($Record) {
        if ($res) {
            $total = count($res);
        } else {
            $total = 0;
        }
    } else {
        $total = 0;
    }

    if ($total % $per_page == 0) {
        $data['pages'] = floor(($total / $per_page));
    } else {
        $data['pages'] = floor(($total / $per_page)) + 1;
    }

    if ($total == 0) {
        $data['start'] = 0;
    } else {
        $data['start'] = $limit['start'] + 1;
    }

    if ($total < $limit['start'] + $per_page) {
        $data['end'] = $total;
    } else {
        $data['end'] = $limit['start'] + $per_page;
    }

    $data['total'] = $total;
    $data['current'] = $page;
    $data['page_name'] = "publisher";
    $this->load->view('web/publisher', $data);
}

public function pressRelease() {
    $config = array();
    $config["base_url"] = base_url() . "web/pressRelease";
    $config["per_page"] = 5;
    $config["uri_segment"] = 3;
    $config["total_rows"] = $this->data_model->record_count('press_release');
    $this->pagination->initialize($config);
    $data['page'] = "Press Release";
    $data['page_header'] = "page-header1";
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data['Records'] = $this->data_model->get_pagi($config["per_page"], $page, 'press_release');
    $data["links"] = $this->pagination->create_links();
    $data['Record'] = array(
        'meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts",
        'meta_desc' => "Stay updated with recent press releases from Reports Monitor, covering various industries worldwide."
    );
    $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
    $data['Record']['canonical'] = base_url() . 'pressRelease';
    $data['Recent'] = $this->data_model->letestReports();
    $this->load->view('web/press_releases', $data);
}

public function press_release($id) {
    $Record = $this->data_model->get(array('id' => $id), NULL, array('id', 'title', 'desc', 'meta_title', 'meta_desc', 'meta_keyword', 'created_at'), NULL, 'press_release')[0];
    $data['Record'] = $Record;
    $data['Record']['canonical'] = base_url() . 'press_release/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
        // print_r($Record);
    $data['page'] = $Record['meta_title'];
    $data['page_header'] = "page-header1";
    $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
    $limit = array('limit' => 6, 'start' => 0);
    $data['press'] = $this->data_model->get(NULL, NULL, array('id', 'title', 'meta_keyword'), $limit, 'press_release', 'desc');
    $this->load->view('web/press_release', $data);
}

public function buyNow($id) {
    if ($_POST) {
        $ip = $this->input->ip_address();
        if ($_POST['optionsRadios'] == "wireTransfer") {
            $to = $_POST['email'];
            $subject = 'Order_Reports Monitor :' . $_POST['reportTitle'];
            $message = '<p>Thank you for ordering ' . $_POST['reportTitle'] . '<br/>';
            $message .= 'Our sales representative will reach out to you shortly with the Bank Details for wire transfer!</p><br/><br/>';
            $message .= '<p><b>Warm Regards,</b><br/>';
            $message .= 'Jay Matthews | Corporate Sales Specialist<br/>';
            $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

//                $this->data_model->sendMail($to, $subject, $message);

            $to = 'sales@reportsmonitor.com';
                // $to = 'azar@straitsresearch.com';
            $subject = 'Buy Now- Wire Transfer:' . $_POST['reportTitle'];
            $message = '<b>Dear Admin,</b><br/>';
            $message .= '<table border = "1">';
            $message .= '<tr><td>Report ID</td>';
            $message .= '<td>' . $_POST['reportid'] . '</td></tr>';
            $message .= '<tr><td>Report Title</td>';
            $message .= '<td>' . $_POST['reportTitle'] . '</td></tr>';
            $message .= '<tr><td>Report Amount</td>';
            $message .= '<td>' . $_POST['price'] . '</td></tr>';
            $message .= '<tr><td>Name</td>';
            $message .= '<td>' . $_POST['name'] . '</td></tr>';
            $message .= '<tr><td>Email</td>';
            $message .= '<td>' . $_POST['email'] . '</td></tr>';
            $message .= '<tr><td>Phone</td>';
            $message .= '<td>' . $_POST['contact'] . '</td></tr>';
            $message .= '<tr><td>Company</td>';
            $message .= '<td>' . $_POST['company'] . '</td></tr>';
            $message .= '<tr><td>Designation</td>';
            $message .= '<td>' . $_POST['title'] . '</td></tr>';
            $message .= '<tr><td>Address</td>';
            $message .= '<td>' . $_POST['address'] . '</td></tr>';
            $message .= '<tr><td>City</td>';
            $message .= '<td>' . $_POST['city'] . '</td></tr>';
            $message .= '<tr><td>State</td>';
            $message .= '<td>' . $_POST['state'] . '</td></tr>';
            $message .= '<tr><td>Country</td>';
            $message .= '<td>' . $_POST['country'] . '</td></tr>';
            $message .= '<tr><td>Zip</td>';
            $message .= '<td>' . $_POST['zip'] . '</td></tr>';
            $message .= '<tr><td>IP</td>';
            $message .= '<td>' . $ip . '</td></tr>';
            $message .= '</table></p>';
            $this->data_model->sendMail($to, $subject, $message);

            redirect(base_url() . 'thankYou');
        } else {
            $oid = $this->data_model->getLast('order_id', array('field' => 'tid', 'type' => 'desc'), 'transactions')['order_id'];

            if ($oid) {
                $Array['order_id'] = $oid + 1;
            } else {
                $Array['order_id'] = 1201;
            }
            $Array['name'] = $_POST['name'];
            $Array['methord'] = $_POST['optionsRadios'];
            $Array['mail'] = $_POST['email'];
            $Array['contact'] = $_POST['contact'];
            $Array['company'] = $_POST['company'];
            $Array['title'] = $_POST['title'];
            $Array['report'] = $_POST['reportTitle'];
            $Array['address'] = $_POST['address'];
            $Array['city'] = $_POST['city'];
            $Array['state'] = $_POST['state'];
            $Array['country'] = $_POST['country'];
            $Array['zip'] = $_POST['zip'];


            $id = $this->data_model->add($Array, 'transactions');

            $Record = $this->data_model->get(array('tid' => $id), NULL, array('tid', 'order_id', 'methord', 'name', 'mail', 'contact', 'city', 'state', 'address', 'country', 'zip'), NULL, 'transactions')[0];

            if ($_POST['optionsRadios'] == 'ccAvenue') {

                $to = $_POST['email'];
                $subject = 'Order_Reports Monitor :' . $_POST['reportTitle'];
                $message = '<p>Thank you for ordering ' . $_POST['reportTitle'] . '<br/>';
                $message .= 'Our sales representative will reach out to you shortly for further communication.</p><br/><br/>';
                $message .= '<p><b>Warm Regards,</b><br/>';
                $message .= 'Jay Matthews | Corporate Sales Specialist<br/>';
                $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

//                    $this->data_model->sendMail($to, $subject, $message);

                $to = 'sales@reportsmonitor.com';
                    // $to = 'azar@straitsresearch.com';
                $subject = 'Buy Now- CCAvenue:' . $_POST['reportTitle'];
                $message = '<b>Dear Admin,</b><br/>';
                $message .= '<table border = "1">';
                $message .= '<tr><td>Report ID</td>';
                $message .= '<td>' . $_POST['reportid'] . '</td></tr>';
                $message .= '<tr><td>Report Title</td>';
                $message .= '<td>' . $_POST['reportTitle'] . '</td></tr>';
                $message .= '<tr><td>Report Amount</td>';
                $message .= '<td>' . $_POST['price'] . '</td></tr>';
                $message .= '<tr><td>Name</td>';
                $message .= '<td>' . $_POST['name'] . '</td></tr>';
                $message .= '<tr><td>Email</td>';
                $message .= '<td>' . $_POST['email'] . '</td></tr>';
                $message .= '<tr><td>Phone</td>';
                $message .= '<td>' . $_POST['contact'] . '</td></tr>';
                $message .= '<tr><td>Company</td>';
                $message .= '<td>' . $_POST['company'] . '</td></tr>';
                $message .= '<tr><td>Designation</td>';
                $message .= '<td>' . $_POST['title'] . '</td></tr>';
                $message .= '<tr><td>Address</td>';
                $message .= '<td>' . $_POST['address'] . '</td></tr>';
                $message .= '<tr><td>City</td>';
                $message .= '<td>' . $_POST['city'] . '</td></tr>';
                $message .= '<tr><td>State</td>';
                $message .= '<td>' . $_POST['state'] . '</td></tr>';
                $message .= '<tr><td>Country</td>';
                $message .= '<td>' . $_POST['country'] . '</td></tr>';
                $message .= '<tr><td>Zip</td>';
                $message .= '<td>' . $_POST['zip'] . '</td></tr>';
                $message .= '<tr><td>IP</td>';
                $message .= '<td>' . $ip . '</td></tr>';
                $message .= '</table></p>';
                $this->data_model->sendMail($to, $subject, $message);

                $Array1['tid'] = $Record['tid'];
                $Array1['merchant_id'] = '142405';
                $Array1['order_id'] = $Record['order_id'];
                $Array1['amount'] = $_POST['price'];
                $Array1['currency'] = 'USD';
                $Array1['redirect_url'] = base_url() . 'paymentResponce';
                $Array1['cancel_url'] = base_url() . 'paymentResponce';
                $Array1['language'] = 'EN';
                $Array1['billing_name'] = $Record['name'];
                $Array1['billing_email'] = $Record['mail'];
                $Array1['billing_tel'] = $Record['contact'];
                $Array1['billing_city'] = $Record['city'];
                $Array1['billing_state'] = $Record['state'];
                $Array1['billing_address'] = $Record['address'];
                $Array1['billing_country'] = $Record['country'];
                $Array1['billing_zip'] = $Record['zip'];

                $this->data_model->makePayment($Array1);
            } elseif ($_POST['optionsRadios'] == 'payPal') { {
                        // print_r($_POST);
                $to = $_POST['email'];
                $subject = 'Order_Reports Monitor :' . $_POST['reportTitle'];
                $message = '<p>Thank you for ordering ' . $_POST['reportTitle'] . '<br/>';
                $message .= 'Our sales representative will reach out to you shortly for further communication.</p><br/><br/>';
                $message .= '<p><b>Warm Regards,</b><br/>';
                $message .= 'Jay Matthews | Corporate Sales Specialist<br/>';
                $message .= 'E-mail:�sales@reportsmonitor.com�| Web:�www.reportsmonitor.com</p>';

//                        $this->data_model->sendMail($to, $subject, $message);

                $to = 'sales@reportsmonitor.com';
//                        $to = 'yuvraj.j@straitsresearch.net';
                $subject = 'Buy Now- PayPal:' . $_POST['reportTitle'];
                $message = '<table border = "1">';
                $message .= '<tr><td>Report ID</td>';
                $message .= '<td>' . $_POST['reportid'] . '</td></tr>';
                $message .= '<tr><td>Report Title</td>';
                $message .= '<td>' . $_POST['reportTitle'] . '</td></tr>';
                $message .= '<tr><td>Report Amount</td>';
                $message .= '<td>' . $_POST['price'] . '</td></tr>';
                $message .= '<tr><td>Name</td>';
                $message .= '<td>' . $_POST['name'] . '</td></tr>';
                $message .= '<tr><td>Email</td>';
                $message .= '<td>' . $_POST['email'] . '</td></tr>';
                $message .= '<tr><td>Phone</td>';
                $message .= '<td>' . $_POST['contact'] . '</td></tr>';
                $message .= '<tr><td>Company</td>';
                $message .= '<td>' . $_POST['company'] . '</td></tr>';
                $message .= '<tr><td>Designation</td>';
                $message .= '<td>' . $_POST['title'] . '</td></tr>';
                $message .= '<tr><td>Address</td>';
                $message .= '<td>' . $_POST['address'] . '</td></tr>';
                $message .= '<tr><td>City</td>';
                $message .= '<td>' . $_POST['city'] . '</td></tr>';
                $message .= '<tr><td>State</td>';
                $message .= '<td>' . $_POST['state'] . '</td></tr>';
                $message .= '<tr><td>Country</td>';
                $message .= '<td>' . $_POST['country'] . '</td></tr>';
                $message .= '<tr><td>Zip</td>';
                $message .= '<td>' . $_POST['zip'] . '</td></tr>';
                $message .= '<tr><td>IP</td>';
                $message .= '<td>' . $ip . '</td></tr>';
                $message .= '</table></p>';
                $this->data_model->sendMail($to, $subject, $message);
                $data['title'] = $_POST['reportTitle'];
                $data['id'] = $_POST['reportid'];
                $data['amount'] = $_POST['price'];
                $data['cancel'] = base_url() . 'paypalResp';
                $data['return'] = base_url() . 'paypalResp';


                $data['page'] = "PayPal Payment";
                $data['page_header'] = "page-header2";

                $this->load->view('web/payPalForm', $data);
            }
        } elseif ($_POST['optionsRadios'] == 'razorPay') {

            $to = $_POST['email'];
            $subject = 'Order_Reports Monitor :' . $_POST['reportTitle'];
            $message = '<p>Thank you for ordering ' . $_POST['reportTitle'] . '<br/>';
            $message .= 'Our sales representative will reach out to you shortly for further communication.</p><br/><br/>';
            $message .= '<p><b>Warm Regards,</b><br/>';
            $message .= 'Jay Matthews | Corporate Sales Specialist<br/>';
            $message .= 'E-mail:�sales@reportsmonitor.com�| Web:�www.reportsmonitor.com</p>';

//                    $this->data_model->sendMail($to, $subject, $message);

            $to = 'sales@reportsmonitor.com';
//                    $to = 'yuvraj.j@straitsresearch.net';
            $subject = 'Buy Now- razorPay:' . $_POST['reportTitle'];
            $message = '<table border = "1">';
            $message .= '<tr><td>Report ID</td>';
            $message .= '<td>' . $_POST['reportid'] . '</td></tr>';
            $message .= '<tr><td>Report Title</td>';
            $message .= '<td>' . $_POST['reportTitle'] . '</td></tr>';
            $message .= '<tr><td>Report Amount</td>';
            $message .= '<td>' . $_POST['price'] . '</td></tr>';
            $message .= '<tr><td>Name</td>';
            $message .= '<td>' . $_POST['name'] . '</td></tr>';
            $message .= '<tr><td>Email</td>';
            $message .= '<td>' . $_POST['email'] . '</td></tr>';
            $message .= '<tr><td>Phone</td>';
            $message .= '<td>' . $_POST['contact'] . '</td></tr>';
            $message .= '<tr><td>Company</td>';
            $message .= '<td>' . $_POST['company'] . '</td></tr>';
            $message .= '<tr><td>Designation</td>';
            $message .= '<td>' . $_POST['title'] . '</td></tr>';
            $message .= '<tr><td>Address</td>';
            $message .= '<td>' . $_POST['address'] . '</td></tr>';
            $message .= '<tr><td>City</td>';
            $message .= '<td>' . $_POST['city'] . '</td></tr>';
            $message .= '<tr><td>State</td>';
            $message .= '<td>' . $_POST['state'] . '</td></tr>';
            $message .= '<tr><td>Country</td>';
            $message .= '<td>' . $_POST['country'] . '</td></tr>';
            $message .= '<tr><td>Zip</td>';
            $message .= '<td>' . $_POST['zip'] . '</td></tr>';
            $message .= '<tr><td>IP</td>';
            $message .= '<td>' . $ip . '</td></tr>';
            $message .= '</table></p>';
            $this->data_model->sendMail($to, $subject, $message);
            $data['title'] = $_POST['reportTitle'];
            $data['id'] = $_POST['reportid'];
            $data['amount'] = $_POST['price'];
            $data['cancel'] = base_url() . 'paypalResp';
            $data['return'] = base_url() . 'paypalResp';


            $data['page'] = "Razor Pay Payment";
            $data['page_header'] = "page-header2";


            $data['itemInfo'] = [
                "key" => RAZOR_KEY_ID,
                "price" => round($_POST['price']),
                "name" => $_POST['name'],
                "currency" => "USD",
                "description" => $_POST['reportTitle'],
                "image" => "https://www.reportsmonitor.com/web_assets/images/logo.png",
                "prefill" => [
                    "name" => $_POST['name'],
                    "email" => $_POST['email'],
                    "contact" => $_POST['contact'],
                ],
                "notes" => [
                    "address" => "INIDA",
                    "merchant_order_id" => $_POST['reportid'],
                ],
                "theme" => [
                    "color" => "#F37254"
                ],
                "order_id" => $_POST['reportid'],
            ];
            $data['return_url'] = site_url() . 'razorpay/callback';
            $data['surl'] = site_url();
            ;
            $data['furl'] = site_url();
            ;
            $data['currency_code'] = 'USD';
            $this->load->view('razorpay/checkout', $data);



//                    $this->load->view('web/payPalForm', $data);
        }
    }
} else {
    $cond = array('report.id' => $id);
    $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
    $fields = array('report.id', 'title', 'single_price', 'multi_price', 'ent_price', 'meta_title', 'meta_keyword');
    $data['Report'] = $this->data_model->get($cond, $tbls, $fields, NULL, 'report')[0];
    $data['price'] = @$_GET['price'] != "" ? @$_GET['price'] : "single_price";
    $data['mode'] = @$_GET['mode'] != "" ? @$_GET['mode'] : "";
    $data['type'] = @$_GET['type'] != "" ? @$_GET['type'] : "";
    $data['page'] = 'Buy Now';
    $data['page_header'] = "page-header2";
    $this->load->view('web/buyNow', $data);
}
}

public function paymentResponce($value = '') {
    redirect(base_url() . 'thankYou');
}

public function paypalResp($value = '') {
    redirect(base_url() . 'thankYou');
}

public function request_sample($id, $keyword = '') {
    if ($_POST) {
        if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@ssemarketing.net"){
            redirect(base_url() . 'thankYou');
        }
        if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@comcast.net"){
            redirect(base_url() . 'thankYou');
        }
        if($_POST['phone']=="+1 213 425 1453"){
            redirect(base_url() . 'thankYou');
        }
        $ip = $this->input->ip_address();

        $res = file_get_contents('https://www.iplocate.io/api/lookup/' . $ip);
        $res = json_decode($res);

            // curl call for CRM
        $Array['name'] = $_POST['name'];
        $Array['mail'] = $_POST['email'];
            //$Array['job_title'] = isset($_POST['job_title']) ? $_POST['job_title'] : "NA";
        $Array['job_title'] = isset($_POST['title']) ? $_POST['title'] : "NA";
        $Array['company'] = isset($_POST['company']) ? $_POST['company'] : "NA";
        $Array['phone'] = isset($_POST['contact']) ? $_POST['contact'] : $_POST['phone'];
        $Array['country'] = isset($_POST['country']) ? $_POST['country'] : "NA";
        $Array['message'] = isset($_POST['message']) ? $_POST['message'] : "Enquire Before Buying";
        $Array['report'] = isset($_POST['report']) ? $_POST['report'] : "NA";
        $Array['region'] = $res->continent;
            // $Array['region'] = "North America";
        $Array['ip'] = $ip;
        $Array['website'] = "reportsmonitor.com";
        $Array['source'] = "Request Sample";

        $url = "http://165.227.205.30/addLead";
        $postData = $Array;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $output = curl_exec($ch);
        curl_close($ch);


        $to = $_POST['email'];
        $subject = 'Request Sample:' . $_POST['report'];
        $message = '<p>Hello ' . ucfirst($_POST['name']) . '</p>';
        $message .= '<p>Thanks for showing interest in the research report ' . $_POST['report'] . '</p>';
        $message .= '<p>We will be sending you the sample copy of the report shortly. Meanwhile, if you have any specific requirement, please let us know.<br/>We look forward to assist you!</p><br/>';
        $message .= '<p><b>Best Regards,</b><br/>';
        $message .= 'Reports Monitor Team<br/>';
        $message .= 'US: +1-513-549-5911<br/>';
        $message .= 'UK: +44 203-318-2846<br/>';
        $message .= 'India: +91 808-708-5354<br/>';
        $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

        $this->data_model->sendMail($to, $subject, $message);

        $to = 'sales@reportsmonitor.com';
        $subject = 'Request Sample:' . $_POST['report'];
        $message = '<b>Dear Admin,</b><br/>';
        $message .= '<table border = "1">';
        $message .= '<tr><td>Report Title</td>';
        $message .= '<td>' . $_POST['report'] . '</td></tr>';
        $message .= '<tr><td>Name</td>';
        $message .= '<td>' . $_POST['name'] . '</td></tr>';
        $message .= '<tr><td>Email</td>';
        $message .= '<td>' . $Array['mail'] . '</td></tr>';
        $message .= '<tr><td>Phone</td>';
        $message .= '<td>' . $Array['phone'] . '</td></tr>';
        $message .= '<tr><td>Company</td>';
        $message .= '<td>' . $Array['company'] . '</td></tr>';
        $message .= '<tr><td>Designation</td>';
        $message .= '<td>' . $Array['job_title'] . '</td></tr>';
        $message .= '<tr><td>Country</td>';
        $message .= '<td>' . $Array['country'] . '</td></tr>';
        $message .= '<tr><td>Source</td>';
        $message .= '<td>Reports Monitor Request Sample</td></tr>';
        $message .= '<tr><td>Message</td>';
        $message .= '<td>' . $_POST['message'] . '</td></tr>';
        $message .= '<tr><td>IP</td>';
        $message .= '<td>' . $ip . '</td></tr>';
        $message .= '</table></p>';


        $this->data_model->sendMail($to, $subject, $message);

        redirect(base_url() . 'thankYou');
    } else {
            /* $data['title'] = $this->data_model->get(array('id' => $id), NULL, array('title'), NULL, 'report')[0]['title'];
              $data['meta_keyword'] = $this->data_model->get(array('id' => $id), NULL, array('meta_keyword'), NULL, 'report_details')[0]['meta_keyword'];
              $data['id'] = $id;
              $fields = array('id','name');
              $data['countries'] = $this->config->item('countries');
              $data['page'] = "Request Sample";
              $data['page_header'] = "page-header2";
              $this->load->view('web/requestSamplenew', $data); */
              $cond = array('report.id' => $id);
              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
              $fields = array('report.id', 'title', 'report_desc', 'cat_id', 'publisher_id', 'region_id', 'toc', 'list_tbl_fig', 'published_date', 'pages', 'single_price', 'multi_price', 'ent_price', 'published_status', 'meta_title', 'meta_desc', 'meta_keyword');
              $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'report')[0];
              $data['Record'] = $Record;

              $data['page'] = "Request Sample";
              $data['page_header'] = "page-header4";
              $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');

              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

              $limit = array('limit' => 5, 'start' => 0);

              $data['Related'] = $this->data_model->get(array('cat_id' => $Record['cat_id'], 'report.id !=' => $Record['id']), $tbls, array('report.id', 'title', 'report_desc', 'meta_keyword', 'published_date'), $limit, 'report');
              $data['Record']['canonical'] = base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
            /* 	print_r($data['Record']['canonical']);
            die; */
            $data['countries'] = $this->config->item('countries');
            if ($Record) {
                $this->load->view('web/requestSamplenew', $data);
            } else {
                redirect(base_url());
            }
        }
    }

    public function check_discount($id, $keyword = '') {
        if ($_POST) {
            if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@ssemarketing.net"){
                redirect(base_url() . 'thankYou');
            }
            if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@comcast.net"){
                redirect(base_url() . 'thankYou');
            }
            if($_POST['phone']=="+1 213 425 1453"){
                redirect(base_url() . 'thankYou');
            }
            $ip = $this->input->ip_address();
            $res = file_get_contents('https://www.iplocate.io/api/lookup/' . $ip);
            $res = json_decode($res);

            // curl call for CRM
            $Array['name'] = $_POST['name'];
            $Array['mail'] = $_POST['email'];
            $Array['job_title'] = isset($_POST['title']) ? $_POST['title'] : "NA";
            $Array['company'] = isset($_POST['company']) ? $_POST['company'] : "NA";
            $Array['phone'] = isset($_POST['country']) ? $_POST['country'] : $_POST['phone'];
            $Array['country'] = isset($_POST['country']) ? $_POST['country'] : "NA";
            $Array['message'] = isset($_POST['message']) ? $_POST['message'] : "NA";
            $Array['report'] = $_POST['report'];
            $Array['region'] = $res->continent;
            $Array['ip'] = $ip;
            $Array['website'] = "reportsmonitor.com";
            $Array['source'] = "Check Discount";


            $url = "http://165.227.205.30/addLead";
            $postData = $Array;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $output = curl_exec($ch);
            curl_close($ch);

            $to = $_POST['email'];
            $subject = 'Thank you for requesting a  Discount on:' . $_POST['report'];
            $message = '<p>Hello ' . ucfirst($_POST['name']) . '</p>';
            $message .= '<p>Thanks for showing interest in the research report ' . $_POST['report'] . '</p>';
            $message .= '<p>Our Sales representative will get in touch with you at the earliest to offer the best possible discount on this report.<br/>We look forward to a long term business relationship with you.</p><br/>';
            $message .= '<p><b>Best Regards,</b><br/>';
            $message .= 'Reports Monitor Team<br/>';
            $message .= 'US: +1-513-549-5911<br/>';
            $message .= 'UK: +44 203-318-2846<br/>';
            $message .= 'India: +91 808-708-5354<br/>';
            $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

            $this->data_model->sendMail($to, $subject, $message);
            $to = 'sales@reportsmonitor.com';
            $subject = 'Discount Enquiry:' . $_POST['report'];
            $message = '<b>Dear Admin,</b><br/>';
            $message .= '<table border = "1">';
            $message .= '<tr><td>Report Title</td>';
            $message .= '<td>' . $_POST['report'] . '</td></tr>';
            $message .= '<tr><td>Name</td>';
            $message .= '<td>' . $_POST['name'] . '</td></tr>';
            $message .= '<tr><td>Email</td>';
            $message .= '<td>' . $Array['mail'] . '</td></tr>';
            $message .= '<tr><td>Phone</td>';
            $message .= '<td>' . $Array['phone'] . '</td></tr>';
            $message .= '<tr><td>Company</td>';
            $message .= '<td>' . $Array['company'] . '</td></tr>';
            $message .= '<tr><td>Designation</td>';
            $message .= '<td>' . $Array['job_title'] . '</td></tr>';
            $message .= '<tr><td>Country</td>';
            $message .= '<td>' . $Array['country'] . '</td></tr>';
            $message .= '<tr><td>Source</td>';
            $message .= '<td>Reports Monitor Discount Enquiry</td></tr>';
            $message .= '<tr><td>Message</td>';
            $message .= '<td>' . $_POST['message'] . '</td></tr>';
            $message .= '<tr><td>IP</td>';
            $message .= '<td>' . $ip . '</td></tr>';
            $message .= '</table></p>';
            $to = 'sales@reportsmonitor.com';
            $this->data_model->sendMail($to, $subject, $message);
            /* }   		
              }
              else
              {
              $this->session->set_flashdata('flashSuccess', 'Not clicked');
          } */
          redirect(base_url() . 'thankYou');
      } else {
            /* $data['title'] = $this->data_model->get(array('id' => $id), NULL, array('title'), NULL, 'report')[0]['title'];
              $data['meta_keyword'] = $this->data_model->get(array('id' => $id), NULL, array('meta_keyword'), NULL, 'report_details')[0]['meta_keyword'];
              $data['id'] = $id;
              $data['page'] = "Check Discount";
              $data['page_header'] = "page-header2";
              $data['countries'] = $this->config->item('countries');
              $this->load->view('web/requestSamplenew', $data); */
              $cond = array('report.id' => $id);
              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
              $fields = array('report.id', 'title', 'report_desc', 'cat_id', 'publisher_id', 'region_id', 'toc', 'list_tbl_fig', 'published_date', 'pages', 'single_price', 'multi_price', 'ent_price', 'published_status', 'meta_title', 'meta_desc', 'meta_keyword');
              $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'report')[0];
              $data['Record'] = $Record;

              $data['page'] = "Check Discount";
              $data['page_header'] = "page-header4";
              $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');

              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

              $limit = array('limit' => 5, 'start' => 0);

              $data['Related'] = $this->data_model->get(array('cat_id' => $Record['cat_id'], 'report.id !=' => $Record['id']), $tbls, array('report.id', 'title', 'report_desc', 'meta_keyword', 'published_date'), $limit, 'report');
              $data['Record']['canonical'] = base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
            /* 	print_r($data['Record']['canonical']);
            die; */
            $data['countries'] = $this->config->item('countries');

            $this->load->view('web/check_discountnew', $data);
        }
    }

    public function search($page = 1) {
        if ($_POST) {
            $ip = $this->input->ip_address();
            $captcha = $this->input->post('g-recaptcha-response');
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcvGGYUAAAAADEFOJdLq6VfOiAcotlFSEkdHaAk &response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
            if (!empty($captcha)) {
                if ($response . 'success' == false) {
                    $this->session->set_flashdata('flashSuccess', 'Sorry Google Recaptcha Unsuccessful!!');
                } else {
                    $to = $_POST['email'];
                    $subject = 'Thank you for Contacting';
                    $message = '<p>Dear ' . ucfirst($_POST['name']) . ',</p>';
                    $message .= '<p>Thank you for contacting Reports Monitor.</p>';
                    $message .= '<p>We have received your message and our representative will reach out to you at the earliest.<br/>Meanwhile, if you have any specific concerns/questions, please drop a line at <br/>sales@reportsmonitor.com <br/>Have a good day ahead!</p><br/>';
                    $message .= '<p><b>Best Regards,</b><br/>';
                    $message .= 'Reports Monitor Team<br/>';
                    $message .= 'US: +1-513-549-5911<br/>';
                    $message .= 'UK: +44 203-318-2846<br/>';
                    $message .= 'India: +91 808-708-5354<br/>';
                    $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

                    $this->data_model->sendMail($to, $subject, $message);

                    $to = 'sales@reportsmonitor.com';
                    // $to = 'azar@straitsresearch.com';
                    $subject = 'Search Form:';
                    $message = '<b>Dear Admin,</b><br/>';
                    $message .= '<table border = "1">';
                    $message .= '<tr><td>Name</td>';
                    $message .= '<td>' . $_POST['name'] . '</td></tr>';
                    $message .= '<tr><td>Email</td>';
                    $message .= '<td>' . $_POST['email'] . '</td></tr>';
                    $message .= '<tr><td>Phone</td>';
                    $message .= '<td>' . $_POST['phone'] . '</td></tr>';
                    $message .= '<tr><td>Job Title</td>';
                    $message .= '<td>' . $_POST['job-title'] . '</td></tr>';
                    $message .= '<tr><td>Company</td>';
                    $message .= '<td>' . $_POST['company'] . '</td></tr>';
                    $message .= '<tr><td>Subject</td>';
                    $message .= '<td>' . $_POST['sub'] . '</td></tr>';
                    $message .= '<tr><td>Message</td>';
                    $message .= '<td>' . $_POST['msg'] . '</td></tr>';
                    $message .= '<tr><td>Source</td>';
                    $message .= '<td>Reports Moniter Search Form</td></tr>';
                    $message .= '<tr><td>IP</td>';
                    $message .= '<td>' . $ip . '</td></tr>';
                    $message .= '</table></p>';

                    $this->data_model->sendMail($to, $subject, $message);
                }
            } else {
                $this->session->set_flashdata('flashSuccess', 'Captcha Failed');
            }
            redirect(base_url() . 'thankYou');
        } elseif ($_GET) {
            $per_page = 20;

            $res = $this->data_model->search(@$_GET['search'], @$_GET['category'], NULL);
            if ($res) {
                $total = count($res);
            } else {
                $total = 0;
            }

            $limit = array('limit' => $per_page, 'start' => ($per_page * $page) - $per_page);

            $data['Records'] = $this->data_model->search(@$_GET['search'], @$_GET['category'], $limit);

            if ($total % $per_page == 0) {
                $data['pages'] = floor(($total / $per_page));
            } else {
                $data['pages'] = floor(($total / $per_page)) + 1;
            }

            if ($total == 0) {
                $data['start'] = 0;
            } else {
                $data['start'] = $limit['start'] + 1;
            }

            if ($total < $limit['start'] + $per_page) {
                $data['end'] = $total;
            } else {
                $data['end'] = $limit['start'] + $per_page;
            }

            $data['total'] = $total;
            $data['current'] = $page;
            $data['searchFor'] = @$_GET['search'];
            $data['cat_id'] = @$_GET['category'];

            $data['page'] = "Search Result";
            $data['page_header'] = "page-header1";
            $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name', 'img'), NULL, 'category');

            $this->load->view('web/search', $data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    public function filter($page = 1) {
        $per_page = 50;
        $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

        $data['page'] = "Categories";
        $data['page_header'] = "page-header1";
        $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name', 'img'), NULL, 'category');
        $data['regions'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'regions');

        if (isset($_POST['categories'])) {
            $cat = $_POST['categories'];
        } else {
            $cat = NULL;
        }

        if (isset($_POST['regions'])) {
            $reg = $_POST['regions'];
        } else {
            $reg = NULL;
        }

        if (isset($_POST['duration'])) {
            $dur = $_POST['duration'];
        } else {
            $dur = NULL;
        }

        // print_r($cat);
        // print_r($reg);
        // print_r($dur);
        $res = $this->data_model->filtre($cat, $reg, $dur, NULL);
        // print_r(count($res));
        if ($res) {
            $total = count($res);
        } else {
            $total = 0;
        }

        $limit = array('limit' => $per_page, 'start' => ($per_page * $page) - $per_page);
        $Records = $this->data_model->filtre($cat, $reg, $dur, $limit);


        if ($total % $per_page == 0) {
            $pages = floor(($total / $per_page));
        } else {
            $pages = floor(($total / $per_page)) + 1;
        }

        if ($total == 0) {
            $start = 0;
        } else {
            $start = $limit['start'] + 1;
        }

        if ($total < $limit['start'] + $per_page) {
            $end = $total;
        } else {
            $end = $limit['start'] + $per_page;
        }

        $total = $total;
        $current = $page;

        echo '<h3><strong>List of Reports</strong></h3>';
        if ($Records) {
            foreach ($Records as $Record) {
                echo '<blockquote>
                <div class="row">
                <div class="col-sm-1 col-xs-2 pad05">
                <img src="' . base_url() . 'web_assets/images/pinit.png" class="img-responsive pt-10">
                </div>
                <div class="col-sm-9 col-xs-10">
                <a class="mb-5" style="color: #3C4858 !important" href="' . base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']) . '"><strong>' . $Record['title'] . '</strong></a>
                <p class="paradesc">
                ' . substr($Record['report_desc'], 0, 100) . ' ... <a href="' . base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']) . '" class="label label-info">read more</a>
                </p>   
                </div>
                <div class="col-sm-2 hidden-xs yrs">
                <h4>' . date('F Y', strtotime($Record['published_date'])) . '</h4>
                </div>
                </div>
                </blockquote>';
            }
        } else {
            echo '<h3 class="text-center">No Records Found</h3>';
        }
        // if ($total != 0)
        // {
        // 	echo '<div class="row mt-40">
        // 		<div class="col-md-5 col-sm-5 col-xs-12 pagidesc pt-20">
        // 			Showing '.$start.' to '.$end .' out of '.$total.'
        // 		</div>
        // 		<div class="col-md-7 col-sm-7 col-xs-12">
        // 			<ul class="pagination pagination-info pull-right">';
        // 	if ($current != 1)
        // 	{	
        // 		echo '<li class="nobg">
        // 			<a href="'.base_url().'filtre/'.($current-1).'">
        // 				<img src="'.base_url().'web_assets/images/pagiLeft.png">
        // 			</a>
        // 		</li>';
        // 	}
        // 	if ($current>5)
        // 	{
        // 		echo'<li><a href="javascript:void(0);">...</a></li>';
        // 	}
        // 	if ($pages>3)
        // 	{
        // 		if ($current<3)
        // 		{
        // 			$start = 1;
        // 			$end = $current +2;
        // 		}
        // 		elseif ($current == $pages && $pages >5)
        // 		{
        // 			$start = $current - 4;
        // 			$end = $current;
        // 		}
        // 		elseif ($current == $pages && $pages <5)
        // 		{
        // 			$start = $current - 3;
        // 			$end = $current;
        // 		}
        // 		elseif ($current >= $pages - 2)
        // 		{
        // 			$start = $current - 2;
        // 			$end = $pages;
        // 		}
        // 		else
        // 		{
        // 			$start = $current - 2;
        // 			$end = $current + 2;
        // 		}
        // 	}
        // 	else
        // 	{
        // 		$start=1;
        // 		$end = $pages;
        // 	}
        // 	for ($i=$start; $i <= $end; $i++) 
        // 	{ 
        // 		echo'<li '.($i==$current) ? 'class="active"' : 'class=""'.'>
        // 			<a href="'.base_url().'filtre/'.$i.'">
        // 				'.$i.'
        // 			</a>
        // 		</li>';
        // 	}
        // 	if ($current < $pages-2)
        // 	{
        // 		echo '<li><a href="javascript:void(0);">...</a></li>';
        // 	}	
        // 	if ($current <= ($pages - 3))
        // 	{
        // 		echo '<li><a href="'.base_url().'filtre/'.$pages.'">'.$pages.'</a></li>';
        // 	}
        // 	if ($current != $pages)
        // 	{
        // 		echo'<li class="nobg">
        // 			<a href="'.base_url().'filtre/'.$Record['id'].'/'.($current+1).'">
        // 				<img src="'.base_url().'web_assets/images/pagiRight.png">
        // 			</a>
        // 		</li>';
        // 	}
        // 	echo'</ul>
        // 		</div>
        // 	</div>';
        // }
    }

    public function getIp() {
        $ip = $this->input->ip_address();
        $to = 'azar.sayyed16@gmail.com';
        $subject = 'IP Address';
        $message = 'IP Address is:' . $ip;

        if ($this->data_model->sendMail($to, $subject, $message)) {
            redirect(base_url());
        }
    }

    public function pnf() {
        if ($_POST) {
            if (strpos($_POST['msg'], 'http') !== false) {
                redirect($_SERVER['HTTP_REFERER']);
            }
            $ip = $this->input->ip_address();
            $captcha = $this->input->post('g-recaptcha-response');
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcvGGYUAAAAADEFOJdLq6VfOiAcotlFSEkdHaAk &response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
            if (!empty($captcha)) {
                if ($response . 'success' == false) {
                    $this->session->set_flashdata('flashSuccess', 'Sorry Google Recaptcha Unsuccessful!!');
                } else {
                    $to = $_POST['email'];
                    $subject = 'Thank you for Contacting';
                    $message = '<p>Dear ' . ucfirst($_POST['name']) . ',</p>';
                    $message .= '<p>Thank you for contacting Reports Monitor.</p>';
                    $message .= '<p>We have received your message and our representative will reach out to you at the earliest.<br/>Meanwhile, if you have any specific concerns/questions, please drop a line at <br/>sales@reportsmonitor.com <br/>Have a good day ahead!</p><br/>';
                    $message .= '<p><b>Best Regards,</b><br/>';
                    $message .= 'Reports Monitor Team<br/>';
                    $message .= 'US: +1-513-549-5911<br/>';
                    $message .= 'UK: +44 203-318-2846<br/>';
                    $message .= 'India: +91 808-708-5354<br/>';
                    $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

                    $this->data_model->sendMail($to, $subject, $message);

                    $to = 'sales@reportsmonitor.com';
                    // $to = 'azar@straitsresearch.com';
                    $subject = '404 Form:';
                    $message = '<b>Dear Admin,</b><br/>';
                    $message .= '<table border = "1">';
                    $message .= '<tr><td>Name</td>';
                    $message .= '<td>' . $_POST['name'] . '</td></tr>';
                    $message .= '<tr><td>Email</td>';
                    $message .= '<td>' . $_POST['email'] . '</td></tr>';
                    $message .= '<tr><td>Phone</td>';
                    $message .= '<td>' . $_POST['phone'] . '</td></tr>';
                    $message .= '<tr><td>Job Title</td>';
                    $message .= '<td>' . $_POST['job-title'] . '</td></tr>';
                    $message .= '<tr><td>Company</td>';
                    $message .= '<td>' . $_POST['company'] . '</td></tr>';
                    $message .= '<tr><td>Subject</td>';
                    $message .= '<td>' . $_POST['sub'] . '</td></tr>';
                    $message .= '<tr><td>Message</td>';
                    $message .= '<td>' . $_POST['msg'] . '</td></tr>';
                    $message .= '<tr><td>Source</td>';
                    $message .= '<td>Reports Moniter 404 Form</td></tr>';
                    $message .= '<tr><td>IP</td>';
                    $message .= '<td>' . $ip . '</td></tr>';
                    $message .= '</table></p>';
                    $this->data_model->sendMail($to, $subject, $message);
                }
            } else {
                $this->session->set_flashdata('flashSuccess', 'Captcha Failed');
            }
            redirect(base_url() . 'thankYou');
        } else {
            $data['page_header'] = "page-header1";
            $this->load->view('templates/404', $data);
        }
    }

    /* Sitemaping Start */

    public function sitemap() {
        $this->db->select('id,name');
        $query = $this->db->get("category");
        $data['items'] = $query->result();
        $this->load->view('sitemap', $data);
    }

    public function sitemap_blog() {
        $this->db->select('id,meta_keyword');
        $query = $this->db->get("blog");
        $data['items'] = $query->result();
        $data['title'] = 'blog';
        $this->load->view('pages', $data);
    }

    public function sitemap_pressRelease() {
        $this->db->select('id,meta_keyword');
        $query = $this->db->get("press_release");
        $data['items'] = $query->result();
        $data['title'] = 'press_release';
        $this->load->view('pages', $data);
    }

    public function main_pages() {
        $this->load->view('main_pages');
    }

    public function sitemaps($id, $cat) {
        $startDate = time();
        $beforeseven = date('Y-m-d', strtotime('-10 day', $startDate));
        $beforeseven = $beforeseven . " 00:00:00";
        $cond = array('report.cat_id' => $id, 'created_at > ' => $beforeseven);
        $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
        $fields = array('report.id', 'meta_keyword', 'report.created_at');
        $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'report', 'desc');
        $data['items'] = $Record;
        $data['title'] = 'report';
        $this->load->view('pages', $data);
    }

    public function catgory_sitemap($id, $count, $cate) {
        $startDate = time();
        $beforeseven = date('Y-m-d', strtotime('-10 day', $startDate));
        $beforeseven = $beforeseven . " 00:00:00";
        $cond = array('report.cat_id' => $id, 'created_at > ' => $beforeseven);
        $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
        $fields = array('report.id', 'meta_keyword', 'report.created_at');
        if ($count == 0) {
            $limit = array('limit' => 10000, 'start' => 0);
        }

        if ($count == 1) {
            $limit = array('limit' => 20000, 'start' => 10000);
        }

        if ($count == 2) {
            $limit = array('limit' => 30000, 'start' => 20000);
        }
        if ($count == 3) {
            $limit = array('limit' => 40000, 'start' => 30000);
        }
        if ($count == 4) {
            $limit = array('limit' => 50000, 'start' => 40000);
        }
        if ($count == 5) {
            $limit = array('limit' => 60000, 'start' => 50000);
        }

        $Record = $this->data_model->get($cond, $tbls, $fields, $limit, 'report', 'desc');
        $data['items'] = $Record;
        $data['title'] = 'report';
        $this->load->view('pages', $data);
    }

    /* End  Sitemap */
    /* Blogs Start */

    public function blogs() {
        $config = array();
        $config["base_url"] = base_url() . "web/blogs";
        $config["per_page"] = 3;
        $config["uri_segment"] = 3;
        $config["total_rows"] = $this->data_model->record_count('blog');
        $this->pagination->initialize($config);
        $data['page'] = "Blogs";
        $data['page_header'] = "page-header2";
        $data['Recent'] = $this->data_model->letestReports();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['Records'] = $this->data_model->get_pagi($config["per_page"], $page, 'blog');
        $data["links"] = $this->pagination->create_links();
        $data['Record'] = array(
            'meta_keyword' => "Reports Monitor, Global Market Research Reports, industry analysis reports, consulting services, syndicated research reports, Business Research, Market Size and Forecasts",
            'meta_desc' => "Stay updated with recent blogs from Reports Monitor, covering various industries worldwide."
        );
        $data['Record']['canonical'] = base_url() . 'blogs';
        $limit = array('limit' => 6, 'start' => 0);
        $data['blogs'] = $this->data_model->get(NULL, NULL, array('id', 'title', 'meta_keyword'), $limit, 'blog', 'desc');
        $this->load->view('web/blogs', $data);
    }

    /* Blogs End */
    /* Blogs Start */

    public function single_blog($id) {
        $cond = array('blog.id' => $id);
        $tbls = NULL;
        $fields = '';
        $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'blog')[0];
        $Records = $this->data_model->get_blog();
        $data['Record'] = $Record;
        $data['Records'] = $Records;
        $data['Record']['canonical'] = base_url() . 'single_blog/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
        $data['page'] = $Record['meta_title'];
        $data['page_header'] = "page-header2";
        $limit = array('limit' => 6, 'start' => 0);
        $data['blogs'] = $this->data_model->get(NULL, NULL, array('id', 'title', 'meta_keyword'), $limit, 'blog', 'desc');
        $this->load->view('web/single_blog', $data);
    }

    /* Blogs End */
    /* public function url()
      {
      $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id' );
      $fields = array('report.id','meta_keyword');
      $data['Record'] = $this->data_model->get(NULL, $tbls, $fields, 20000, 'report',"ord");
      $this->load->view('web/url', $data);
  } */

//    Razor pay Functionss
    // initialized cURL Request
  private function get_curl_handle($payment_id, $amount) {
    $url = 'https://api.razorpay.com/v1/payments/' . $payment_id . '/capture';
    $key_id = RAZOR_KEY_ID;
    $key_secret = RAZOR_KEY_SECRET;
    $fields_string = "amount=$amount";
        //cURL Request
    $ch = curl_init();
        //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, $key_id . ':' . $key_secret);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/ca-bundle.crt');
    return $ch;
}

    // callback method
public function callback() {
    if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
        $razorpay_payment_id = $this->input->post('razorpay_payment_id');
        $merchant_order_id = $this->input->post('merchant_order_id');
        $currency_code = 'USD';
        $amount = $this->input->post('merchant_total');
        $success = false;
        $error = '';
        try {
            $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
            $result = curl_exec($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($result === false) {
                $success = false;
                $error = 'Curl error: ' . curl_error($ch);
            } else {
                $response_array = json_decode($result, true);
                    // echo "<pre>";print_r($response_array);exit;
                    //Check success response
                if ($http_status === 200 and isset($response_array['error']) === false) {
                    $success = true;
                } else {
                    $success = false;
                    if (!empty($response_array['error']['code'])) {
                        $error = $response_array['error']['code'] . ':' . $response_array['error']['description'];
                    } else {
                        $error = 'RAZORPAY_ERROR:Invalid Response <br/>' . $result;
                    }
                }
            }
                //close connection
            curl_close($ch);
        } catch (Exception $e) {
            $success = false;
            $error = 'OPENCART_ERROR:Request to Razorpay Failed';
        }
        if ($success === true) {
            if (!empty($this->session->userdata('ci_subscription_keys'))) {
                $this->session->unset_userdata('ci_subscription_keys');
            }
            if (!$order_info['order_status_id']) {
                redirect($this->input->post('merchant_surl_id'));
            } else {
                redirect($this->input->post('merchant_surl_id'));
            }
        } else {
            redirect($this->input->post('merchant_furl_id'));
        }
    } else {
        echo 'An error occured. Contact site administrator, please!';
    }
}

public function success() {
    $data['title'] = 'Razorpay Success | TechArise';
    $this->load->view('razorpay/success', $data);
}

public function failed() {
    $data['title'] = 'Razorpay Failed | Report Monitor';
    $this->load->view('razorpay/failed', $data);
}

public function privacypolicyOnEnquiery() {
    $data['page'] = "Privacy";
    $data['page_header'] = "page-header2";
    $this->load->view('web/privacypolicy', $data);
}
/*Covid -19 */

public function covidAnalysis($id, $keyword = '') {
    if ($_POST) {
        if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@ssemarketing.net"){
            redirect(base_url() . 'thankYou');
        }
        if(substr($_POST['email'],strpos($_POST['email'],"@"))=="@comcast.net"){
            redirect(base_url() . 'thankYou');
        }
        if($_POST['phone']=="+1 213 425 1453"){
            redirect(base_url() . 'thankYou');
        }
        $ip = $this->input->ip_address();

        $res = file_get_contents('https://www.iplocate.io/api/lookup/' . $ip);
        $res = json_decode($res);

            // curl call for CRM
        $Array['name'] = $_POST['name'];
        $Array['mail'] = $_POST['email'];
            //$Array['job_title'] = isset($_POST['job_title']) ? $_POST['job_title'] : "NA";
        $Array['job_title'] = isset($_POST['title']) ? $_POST['title'] : "NA";
        $Array['company'] = isset($_POST['company']) ? $_POST['company'] : "NA";
        $Array['phone'] = isset($_POST['contact']) ? $_POST['contact'] : $_POST['phone'];
        $Array['country'] = isset($_POST['country']) ? $_POST['country'] : "NA";
        $Array['message'] = isset($_POST['message']) ? $_POST['message'] : "Enquire Before Buying";
        $Array['report'] = isset($_POST['report']) ? $_POST['report'] : "NA";
        $Array['region'] = $res->continent;
            // $Array['region'] = "North America";
        $Array['ip'] = $ip;
        $Array['website'] = "reportsmonitor.com";
        $Array['source'] = "Covid-19 Analysis";

        $url = "http://165.227.205.30/addLead";
        $postData = $Array;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $output = curl_exec($ch);
        curl_close($ch);


        $to = $_POST['email'];
        $subject = 'Covid-19 Impact Analysis:' . $_POST['report'];
        $message = '<p>Hello ' . ucfirst($_POST['name']) . '</p>';
        $message .= '<p>Thanks for showing interest in the research report ' . $_POST['report'] . '</p>';
        $message .= '<p>We will be sending you the sample copy of the report shortly. Meanwhile, if you have any specific requirement, please let us know.<br/>We look forward to assist you!</p><br/>';
        $message .= '<p><b>Best Regards,</b><br/>';
        $message .= 'Reports Monitor Team<br/>';
        $message .= 'US: +1-513-549-5911<br/>';
        $message .= 'UK: +44 203-318-2846<br/>';
        $message .= 'India: +91 808-708-5354<br/>';
        $message .= 'E-mail: sales@reportsmonitor.com | Web: www.reportsmonitor.com</p>';

        $this->data_model->sendMail($to, $subject, $message);

        $to = 'sales@reportsmonitor.com';
        $subject = 'Covid-19 Analysis:' . $_POST['report'];
        $message = '<b>Dear Admin,</b><br/>';
        $message .= '<table border = "1">';
        $message .= '<tr><td>Report Title</td>';
        $message .= '<td>' . $_POST['report'] . '</td></tr>';
        $message .= '<tr><td>Name</td>';
        $message .= '<td>' . $_POST['name'] . '</td></tr>';
        $message .= '<tr><td>Email</td>';
        $message .= '<td>' . $Array['mail'] . '</td></tr>';
        $message .= '<tr><td>Phone</td>';
        $message .= '<td>' . $Array['phone'] . '</td></tr>';
        $message .= '<tr><td>Company</td>';
        $message .= '<td>' . $Array['company'] . '</td></tr>';
        $message .= '<tr><td>Designation</td>';
        $message .= '<td>' . $Array['job_title'] . '</td></tr>';
        $message .= '<tr><td>Country</td>';
        $message .= '<td>' . $Array['country'] . '</td></tr>';
        $message .= '<tr><td>Source</td>';
        $message .= '<td>Reports MonitorCovid-19 Analysis</td></tr>';
        $message .= '<tr><td>Message</td>';
        $message .= '<td>' . $_POST['message'] . '</td></tr>';
        $message .= '<tr><td>IP</td>';
        $message .= '<td>' . $ip . '</td></tr>';
        $message .= '</table></p>';


        $this->data_model->sendMail($to, $subject, $message);

        redirect(base_url() . 'thankYou');
    } else {
            /* $data['title'] = $this->data_model->get(array('id' => $id), NULL, array('title'), NULL, 'report')[0]['title'];
              $data['meta_keyword'] = $this->data_model->get(array('id' => $id), NULL, array('meta_keyword'), NULL, 'report_details')[0]['meta_keyword'];
              $data['id'] = $id;
              $fields = array('id','name');
              $data['countries'] = $this->config->item('countries');
              $data['page'] = "Request Sample";
              $data['page_header'] = "page-header2";
              $this->load->view('web/requestSamplenew', $data); */
              $cond = array('report.id' => $id);
              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');
              $fields = array('report.id', 'title', 'report_desc', 'cat_id', 'publisher_id', 'region_id', 'toc', 'list_tbl_fig', 'published_date', 'pages', 'single_price', 'multi_price', 'ent_price', 'published_status', 'meta_title', 'meta_desc', 'meta_keyword');
              $Record = $this->data_model->get($cond, $tbls, $fields, NULL, 'report')[0];
              $data['Record'] = $Record;

              $data['page'] = "Request Sample";
              $data['page_header'] = "page-header4";
              $data['categories'] = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');

              $tbls[0] = array('name' => 'report_details', 'condi' => 'report.id = report_details.id');

              $limit = array('limit' => 5, 'start' => 0);

              $data['Related'] = $this->data_model->get(array('cat_id' => $Record['cat_id'], 'report.id !=' => $Record['id']), $tbls, array('report.id', 'title', 'report_desc', 'meta_keyword', 'published_date'), $limit, 'report');
              $data['Record']['canonical'] = base_url() . 'report/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']);
            /*  print_r($data['Record']['canonical']);
            die; */
            $data['countries'] = $this->config->item('countries');
            if ($Record) {
                $this->load->view('web/covideanalysis', $data);
            } else {
                redirect(base_url());
            }
        }
    }

}
