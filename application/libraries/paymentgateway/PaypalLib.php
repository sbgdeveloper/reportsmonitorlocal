<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Paypal Class
 *
 * Work with remote servers via Paypal much easier than using the native PHP bindings.
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Yuvraj Jadhav
 */
require_once(APPPATH . 'libraries/paypal-php-sdk/paypal/rest-api-sdk-php/sample/bootstrap.php'); // require paypal files

use PayPal\Api\ItemList;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Amount;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

class PaypalLib {

    public $_api_context;

    function __construct() {
        // paypal credentials
        $this->CI = & get_instance();
        $this->CI->config->load('paypal');
        $this->CI->_api_context = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                $this->CI->config->item('client_id'), $this->CI->config->item('secret')
                )
        );
    }

    function create_payment_with_paypal_libFunc($InputData = array()) {

        // setup PayPal api context
        $this->CI->_api_context->setConfig($this->CI->config->item('settings'));


// ### Payer
// A resource representing a Payer that funds a payment
// For direct credit card payments, set payment method
// to 'credit_card' and add an array of funding instruments.

        $payer['payment_method'] = $InputData['payment_method'];

// ### Itemized information
// (Optional) Lets you specify item wise
// information
        $item1["name"] = $InputData['item_name'];
        $item1["sku"] = $InputData['item_number'];  // Similar to `item_number` in Classic API
        $item1["description"] = $InputData['item_description'];
        $item1["currency"] = $InputData['currency'];
        $item1["quantity"] = $InputData['quantity'];
        $item1["price"] = $InputData['item_price'];

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

// ### Additional payment details
// Use this optional field to set additional
// payment information such as tax, shipping
// charges etc.
        // $details['tax'] = $this->CI->input->post('details_tax');
        $details['subtotal'] = $InputData['item_price'];
// ### Amount
// Lets you specify a payment amount.
// You can also specify additional details
// such as shipping, tax.
        $amount['currency'] = $InputData["currency"];
        // $amount['total'] = $details['tax'] + $details['subtotal'];
        $amount['total'] = $item1["price"];
        $amount['details'] = $details;
// ### Transaction
// A transaction defines the contract of a
// payment - what is the payment for and who
// is fulfilling it.
        $transaction['description'] = $InputData["item_description"];
        $transaction['amount'] = $amount;
        $transaction['invoice_number'] = uniqid();
        $transaction['item_list'] = $itemList;

        // ### Redirect urls
// Set the urls that the buyer must be redirected to after
// payment approval/ cancellation.
        $baseUrl = base_url();
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($baseUrl . "paypal/getPaymentStatus")
                ->setCancelUrl($baseUrl . "paypal/getPaymentStatus");
   
// ### Payment
// A Payment Resource; create one using
// the above types and intent set to sale 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

        try {
    
            $payment->create($this->CI->_api_context);
       
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $ex);
            exit(1);
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        if (isset($redirect_url)) {
            /** redirect to paypal * */
            redirect($redirect_url);
        }

        $this->CI->session->set_flashdata('success_msg', 'Unknown error occurred');
        redirect('data/index');
    }

    public function getPaymentStatus_libFunc() {
        // paypal credentials

        /** Get the payment ID before session clear * */
        $payment_id = $InputData["paymentId"];
        $PayerID = $InputData["PayerID"];
        $token = $InputData["token"];
        /** clear the session payment ID * */
        if (empty($PayerID) || empty($token)) {
            $this->CI->session->set_flashdata('success_msg', 'Payment failed');
            redirect(base_url() . 'thankYou');
        }

        $payment = Payment::get($payment_id, $this->CI->_api_context);


        /** PaymentExecution object includes information necessary * */
        /** to execute a PayPal account payment. * */
        /** The payer_id is added to the request query parameters * */
        /** when the user is redirected from paypal back to your site * */
        $execution = new PaymentExecution();
        $execution->setPayerId($InputData["PayerID"]);

        /*         * Execute the payment * */
        $result = $payment->execute($execution, $this->CI->_api_context);



        //  DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {
            $trans = $result->getTransactions();

            // item info
            $Subtotal = $trans[0]->getAmount()->getDetails()->getSubtotal();
            $Tax = $trans[0]->getAmount()->getDetails()->getTax();

            $payer = $result->getPayer();
            // payer info //
            $PaymentMethod = $payer->getPaymentMethod();
            $PayerStatus = $payer->getStatus();
            $PayerMail = $payer->getPayerInfo()->getEmail();

            $relatedResources = $trans[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            // sale info //
            $saleId = $sale->getId();
            $CreateTime = $sale->getCreateTime();
            $UpdateTime = $sale->getUpdateTime();
            $State = $sale->getState();
            $Total = $sale->getAmount()->getTotal();
            /** it's all right * */
            /** Here Write your database logic like that insert record or value in database if you want * */
            $this->CI->paypal->create($Total, $Subtotal, $Tax, $PaymentMethod, $PayerStatus, $PayerMail, $saleId, $CreateTime, $UpdateTime, $State);
            $this->CI->session->set_flashdata('success_msg', 'Payment success');
            redirect('paypal/success');
        }
        $this->CI->session->set_flashdata('success_msg', 'Payment failed');
        redirect('paypal/cancel');
    }

    function refund_payment_libFunc() {
        $refund_amount = $InputData['refund_amount'];
        $saleId = $InputData['sale_id'];
        $paymentValue = (string) round($refund_amount, 2);

// ### Refund amount
// Includes both the refunded amount (to Payer)
// and refunded fee (to Payee). Use the $amt->details
// field to mention fees refund details.
        $amt = new Amount();
        $amt->setCurrency('USD')
                ->setTotal($paymentValue);

// ### Refund object
        $refundRequest = new RefundRequest();
        $refundRequest->setAmount($amt);

// ###Sale
// A sale transaction.
// Create a Sale object with the
// given sale transaction id.
        $sale = new Sale();
        $sale->setId($saleId);
        try {
            // Refund the sale
            // (See bootstrap.php for more on `ApiContext`)
            $refundedSale = $sale->refundSale($refundRequest, $this->CI->_api_context);
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            ResultPrinter::printError("Refund Sale", "Sale", null, $refundRequest, $ex);
            exit(1);
        }

// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        ResultPrinter::printResult("Refund Sale", "Sale", $refundedSale->getId(), $refundRequest, $refundedSale);

        return $refundedSale;
    }

}
