<?php $this->load->view('templates/web_header'); ?>
<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "BlogPosting",
    "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?= base_url() . 'blog/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']) ?>"
    },
    "headline": "<?= $Record['title'] ?>",
    "image": {
    "@type": "ImageObject",
    "url": "https://www.reportsmonitor.com/web_assets/images/logo.png",
    "height": 647,
    "width": 365  },
    "datePublished": "<?= substr($Record['created_at'], 0, 10) ?>",
    "dateModified": "<?= substr($Record['created_at'], 0, 10) ?>",
    "author": {
    "@type": "Person",
    "name": "Jay Matthews"
    },
    "publisher": {
    "@type": "Organization",
    "name": "Reports Monitor",
    "logo": {
    "@type": "ImageObject",
    "url": "https://www.reportsmonitor.com/web_assets/images/logo.png"
    }
    },
    "description": " <?php echo( substr(strip_tags($Record['desc']), 0, 150) ) ?>"
    }
</script>
<div class="container">
    <div class="row  ">
        <div class="col-md-8 col-sm-12 col-xs-12 mb-0 mb-xs-10">
            <br>
            <a href="<?php echo base_url(); ?>">Home</a> >> <a href="<?php echo base_url(); ?>blogs">Blogs</a> >> <?php print_r($Record['title']); ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                    <br> 
                    <img src="<?= base_url() ?>web_assets/images/Blogs.png" style="width: 80px; height: 100px;" alt="<?php print_r($Record['title']); ?>">    
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                    <h2 ><strong><?php print_r($Record['title']); ?></strong></h2>
                    <?= $Record['created_at'] ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                <div style="border: 1px solid #928F8E;  border-radius: 10px; padding: 20px; ">
                    <?php print_r($Record['desc']); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 mb-20 mb-xs-10">
            <h3><b>More Blogs</b></h3>
            <?php if ($blogs): ?>
                <?php foreach ($blogs as $Rec): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mp-0">

                        <a style="color: #3C4858 !important" href="<?= base_url() . 'blog/' . $Rec['id'] . '/' . str_replace(' ', '-', $Rec['meta_keyword']) ?>"><b><?= $Rec['title'] ?></b></a>
                        <hr>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>
<?php $this->load->view('templates/web_footer') ?>