<?php $this->load->view('templates/web_header') ?>
<?php 
$a = mt_rand(0,9);
$b = mt_rand(0,9);
$c =$a + $b;
?>
<div class="container">
	<div class="col-md-10 col-xs-12 mrgnauto">
		<div class="row mt-30">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<h1 class="cattitle mb-30">Contact Us</h1>
				<?php if ($this->session->flashdata('flashSuccess')): ?>
					<div class="alert alert-danger">
						<strong>Info!</strong> <?php echo $this->session->flashdata('flashSuccess') ?>
					</div>
				<?php endif ?>
				<form method="POST">
					<div class="form-group is-empty">
						<input type="text" name="name" placeholder="Name" value="<?php echo set_value('name'); ?>" class="form-control" required>
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('name'); ?>
					</div>
					<div class="form-group is-empty">
						<input type="email" name="email" placeholder="Email" class="form-control" value="<?php echo set_value('email'); ?>" required>
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('email'); ?>
					</div>
					<div class="form-group is-empty">
						<input type="text" name="job_title" placeholder="Job Title" class="form-control" value="<?php echo set_value('job_title'); ?>" required>
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('job_title'); ?>
					</div>
					<div class="form-group is-empty">
						<input type="text" name="company" placeholder="Company" class="form-control" value="<?php echo set_value('company'); ?>" required="">
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('company'); ?>
					</div>
					<div class="form-group is-empty">
						<input type="text" name="phone" placeholder="Phone No" class="form-control" value="<?php echo set_value('phone'); ?>" required>
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('phone'); ?>
					</div>
					<div class="form-group is-empty">
						<input type="text" name="sub" placeholder="Subject" class="form-control" value="<?php echo set_value('sub'); ?>" required="">
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('sub'); ?>
					</div>
					<div class="form-group is-empty">
						<textarea class="form-control" name="msg" placeholder="Message" rows="5"><?php echo set_value('msg'); ?></textarea>
						<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
						echo form_error('msg'); ?>
					</div>
					<div class="form-group is-empty">
						<div class="g-recaptcha" data-sitekey="6Ld2ziIaAAAAAGpEJ5lmatDxAue0fyyRwshU0mgx"></div>

					</div>
					<input type="submit" id="submit" class="btn btn-info" value="Submit" >  

				</form>
			</div><!--form ends here-->
			<div class="col-md-4 col-sm-4 col-xs-12 adinfo">
				<h2 style="font-size: 36px;" class="cattitle mb-50">Address</h2>
				<p>Office C & D - 4th Floor,<br>
					Siddhi Towers, NIBM Road,<br>
					Pune 411048, Maharashtra, India
				</p>
				<p>
					+1 513 549 5911 (U.S.)<br>
					+91 8087085354 (India)<br>
					+44 203 318 2846 (U.K.)
				</p>
				<p>
					<a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a>
				</p>
			</div>
		</div><!--row ends here-->
	</div>
</div>
<?php $this->load->view('templates/web_footer') ?>