<?php $this->load->view('templates/web_header') ?>
<div class="container">
    <div class="row mt-0">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h1 style="font-size: 36px;" class="authortitle"><?= $page ?></h1>	
            <?php if ($Records): ?>
                <?php foreach ($Records as $Rec): ?>
                    <div class="col-sm-12">
                        <div class="col-item pb-20 pb-xs-10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;"><br>
                                    <img src="<?= base_url() ?>web_assets/images/Press Release.jpg" style="width: 80px; height: 100px;" alt="<?= str_replace(' ', '-', $Rec['meta_keyword']) ?>">
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                                    <h2 style="font-size: 24px;">
                                        <a style="color: #3C4858 !important" href="<?= base_url() . 'press_release/' . $Rec['id'] . '/' . str_replace(' ', '-', $Rec['meta_keyword']) ?>"><b><?= $Rec['title'] ?></b></a>
                                    </h2>
                                    <?= $Rec['created_at'] ?>
                                </div>
                            </div>
                            <p >
                                <?= substr(strip_tags($Rec['desc']), 0, 600) . ' ...' ?>
                                <a href="<?= base_url() ?>press_release/<?= $Rec['id'] ?>" class="label label-info">read more</a>
                                <!-- <a href="#" class="label label-info">read more</a> -->
                            </p>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 ovrflw">
            <h2 style="font-size: 36px;" class="newsptitle">Latest Reports</h2>	
            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                <div class="latestnews">
                    <div class="item2">
                        <div class="row">
                            <?php if ($Recent): ?>
                                <?php $i = 1; ?>
                                <?php foreach ($Recent as $Recent_report): ?>
                                    <div class="col-sm-12">
                                        <div class="col-item pb-20 pb-xs-10">
                                            <h4 class="newstitle">
                                                <a style="color: #3C4858 !important" href="<?= base_url() ?>report/<?= $Recent_report['id'] . '/' . str_replace(' ', '-', $Recent_report['meta_keyword']) ?>"><?= substr($Recent_report['title'], 0, 70) ?>
                                                </a>
                                            </h4>
                                            <p>
                                                <?= substr($Recent_report['report_desc'], 0, 70) . ' ...' ?>
                                                <a class="label label-info" href="<?= base_url() ?>report/<?= $Recent_report['id'] . '/' . str_replace(' ', '-', $Recent_report['meta_keyword']) ?>">read more</a>
                                            </p>
                                        </div>
                                    </div>
                                    <?php if ($i == 3): ?>
                                    </div>
                                </div>
                                <div class="item2">
                                    <div class="row">
                                    <?php endif ?>
                                    <?php $i++; ?>
                                <?php endforeach ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>      
            </div> <!--side bar latest news heds here--> 
        </div><!--right side bar-->

    </div>
    <p style="font-size: 20px;"><?php echo $links; ?></p>
</div>
<?php $this->load->view('templates/web_footer') ?>