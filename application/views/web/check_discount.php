<?php $this->load->view('templates/web_header') ?>
<?php
$a = mt_rand(0, 9);
$b = mt_rand(0, 9);
$c = $a + $b;
?>
<div class="container">
    <div class="row">

        <div class="col-md-9 col-sm-9 col-xs-12 mt-0">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.jpg" class="img-responsive center-block" alt="<?= $Record['meta_keyword'] ?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10 ttlsubcat1">
                    <h1 style="font-size: 20px; color: #246A9F;"><strong><?= $Record['title'] ?></strong></h1>
                    <ul class="list-inline blginfo pt-10 mb-20" row>
                        <?php if ($Record['published_date'] == '1970-01-01'): ?>

                            <li class="col-md-6"><strong>Published Date </strong>: 21 Aug 2018</li>
                            <?php else: ?>
                                <li class="col-md-6"><strong>Published Date </strong>: <?= date('d M Y', strtotime($Record['published_date'])) ?></li>
                            <?php endif ?>

                            <li class="col-md-6"><strong>  <?= $this->data_model->get(array('id' => $Record['publisher_id']), NULL, array('name'), NULL, 'publisher')[0]['name'] ?></strong></li> 
                            <li class="col-md-6"><strong>Category </strong>: <a href="<?= base_url() . 'categories/' . $Record['cat_id'] ?>"><?= $this->data_model->get(array('id' => $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></a></li>
                            <li class="col-md-6"><strong>Pages </strong>: <?= ($Record['pages'] > 0) ? $Record['pages'] : 'N/A'; ?></li>
                        </ul>

                    </div>
                    <div class="col-md-12 mt-20" >	
                        <ul class="nav nav-tabs" style="height: 50px; ">
                            <li  style="margin: 0px 2px;"> <a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>" style="background-color:#f1f1f1;height: 50px; padding: 10px;">Report Details </a></li>
                            <?php if ($Record['toc'] != NULL): ?>
                                <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Table of Content</a></li>
                            <?php endif ?>
                            <?php if ($Record['list_tbl_fig'] != NULL): ?>
                                <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Tables & Figures</a></li>
                            <?php endif ?>

                            <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"  class="" style="background-color:#f1f1f1;height: 50px; padding: 10px;">Get Free Sample <i class="fa fa-download" aria-hidden="true" style="margin-left:5px; "></i></a></li>
                            <li class="active" style="margin: 0px 2px;"><a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" style="background-color:#246A9F; height: 50px; padding: 10px;color: white;" >Check Discount</a></li>
                        </ul>                          
                    </div>
                </div>
                <br>
                <div class="row" style="border: 1px solid #928F8E;  border-radius: 10px;">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" >
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                <h3 class="authortitle">Check Discount</h3>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Name *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
                                        <input type="hidden" name="report" value="<?= $Record['title'] ?>">
                                        <input type="text" value="" placeholder="&nbsp;&nbsp;Type Your Name Here" name="name" class="inp_dsgn11" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Email *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <input type="email" value="" placeholder="&nbsp;&nbsp;Please Provide Your  Business Email ID" name="email" class="inp_dsgn11" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Phone Number *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <input type="text" value="" placeholder="&nbsp;&nbsp;Direct Line Makes it easier to say Hello" name="contact"  id="contact" class="inp_dsgn11" required>
                                        <p class="error" id="error" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Title / Designation *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <input type="text" value="" placeholder="&nbsp;&nbsp;Please Type Here" name="title" class="inp_dsgn11" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Company *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <input type="text" value="" placeholder="&nbsp;&nbsp;Please Type Here" name="company" class="inp_dsgn11" required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Country *</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <select class="inp_dsgn11"  name="country" id="country" required="">
                                            <option selected="true" disabled="disabled">Please Select Your Country </option>
                                            <?php foreach ($countries as $key => $value): ?>
                                                <option value="<?= $value ?>"><?= htmlspecialchars($value) ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <label class="inp_dsgn11"><b>Message</b></label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                        <textarea value="" placeholder="&nbsp;&nbsp; Share With us any information that might help us better respond to your request." class="inp_dsgn11" name="message" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <b style="color: #000; font-size: 20px; "><?= $a ?> + <?= $b ?> =</b>
                                            </span>
                                            <input type="text" class="inp_dsgn11" style="margin-bottom:  20px; padding: 0px;" id="security" placeholder="&nbsp;&nbsp;Security Code" autocomplete="off" required>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                    <p>Your personal details are safe with us. <a href="https://www.reportsmonitor.com/privacypolicy" target="_blank"> privacy</a></p>
                                    <input type="submit" id="submit" class="btn btn-info" value="Submit" disabled>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="pt-10  text-center">
                   <a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"class="btn btn-info" style="border-radius: 10px">
                    <strong>Request a free sample</strong>
                </a>
                <a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px">
                    <strong>Check Discount</strong>
                </a>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($Related): ?>
                        <h3><strong>Related Reports</strong></h3>
                        <?php foreach ($Related as $Rel): ?>
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-2 pad05">
                                        <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.png" class="img-responsive pt-10" alt="<?= str_replace(' ', '-', $Rel['meta_keyword']) ?>"> 
                                    </div>
                                    <div class="col-sm-9 col-xs-10">
                                        <p class="mb-5">
                                            <a style="color: #3C4858 !important" href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>"><strong><?= $Rel['title'] ?></strong></a>
                                        </p>
                                        <p class="paradesc">
                                            <?= substr($Rel['report_desc'], 0, 200) . ' ...' ?><a href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>" class="label label-info">read more</a>
                                        </p>
                                    </div>
                                    <div class="col-sm-2 hidden-xs yrs">
                                        <h4><?= date('F Y', strtotime($Rel['published_date'])) ?></h4>
                                    </div>
                                </div>
                            </blockquote>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>

            </div>
        </div>
        <?php $this->load->view('web/reportsidebar') ?> 
    </div>

</div>


<?php $this->load->view('templates/web_footer') ?>
<script type="text/javascript">
    $(document).ready(function ()
    {
        var res = <?= $c ?>;
        var v = 0;
        var checked_status = 0;
        $("#security").focusout(function () {
            v = this.value;
        });
        /*$("#terms").click(function() {
         checked_status = this.checked;
         
         if (checked_status == true && v == res)
         {
         $("#submit").removeAttr("disabled");
         }
         else
         {
         $("#submit").attr("disabled", "disabled");
         }
     });*/
     $("#contact").keyup(function ()
     {
        inputtxt = this.value;
        var phoneno = /^\+?[0-9]*$/;
//            var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
if (inputtxt.match(phoneno))
{
    $("#error").html("");
    return true;
} else
{
    $("#error").html("Not a valid Phone Number");
    $("#submit").attr("disabled", "disabled");
    return false;
}
});
     $("#security").keyup(function ()
     {
        v = this.value;

        if (v == res)
        {
            $("#submit").removeAttr("disabled");
        } else
        {
            $("#submit").attr("disabled", "disabled");
        }
    });
 });
</script>
<script>
    var myIndex = 0;
    carousel();
    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {
            myIndex = 1
        }
        x[myIndex - 1].style.display = "block";
        setTimeout(carousel, 10000);
    }
</script>

