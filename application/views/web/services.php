<?php $this->load->view('templates/web_header') ?>
<div class="container">
	<div class="row  mt-5  min-mr">
		<div class="col-md-12 col-sm-12 col-xs-12 mb-0 mb-xs-10">
	    	<h1 class="servicestitle">Our Services</h1><br>
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Syndicated Report.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Syndicated Market Reports"><br>
		    		<h2 style="font-weight: bold; text-align: center;">Syndicated Market Reports</h2>
		    		<p>For an effective marketing strategy or strategic planning and R&D phase, you can rely on the reports for a detailed representation of the market. With the range of reports in our data vault, you have a number of options to choose from. They are validated with stringent research methodologies by the publisher to present before you the information and provide a region-wise or a product wise classification of the inputs.</p>
		    	</div>
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Customized.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Customized Research Program"><br>
		    		<h2 style="font-weight: bold;text-align: center;">Customized Research Program</h2>
		    		<p>There are times where you would require a tailor-made study focusing on a particular market or segment within a broader spectrum. We will analyze your niche portfolio of requirements and offer you suitable customized research study proposals.</p>
		    	</div>
		    </div>

		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Domain Specific.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Domain-specific analytics"><br>
		    		<h2 style="font-weight: bold;text-align: center;">Domain-specific analytics</h2>
		    		<p>Reports Monitor also specializes in domain-specific analysis and which is based on your particular area of interest. Our team will help identify the new growth opportunities which will help you in faster factual decision making.</p>
		    	</div>
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Subscription.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Subscription"><br>
		    		<h2 style="font-weight: bold;text-align: center;">Subscription</h2>
		    		<p>We offer highly economical subscription services which enables you to fulfill your product lifecycle management. We work against strict SLAs to be your go-to partner for a market report requirement. This would include high priority based customer care service and timely delivery of projects.</p>
		    	</div>
		    </div>

		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Consulting.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Consulting"><br>
		    		<h2 style="font-weight: bold;text-align: center;">Consulting</h2>
		    		<p>We offer our premium consulting services to cater your needs pertaining to market entry, R&D or customer need analysis. Our experts will guide you through the various changes in the market landscape and empower you with the right knowledge which would enable you to move ahead in the right direction.</p>
		    	</div>
		    	<div class="col-md-6 col-sm-12 col-xs-12">
		    		<img  src="<?php echo base_url().'web_assets/' ?>images/Bench Marking.png"  style="width: 30%; display: block; margin-left: auto; margin-right: auto; " alt="Bench-marking study"><br>
		    		<h2 style="font-weight: bold;text-align: center;">Bench-marking study</h2>
		    		<p>Our product and domain benchmarking study provide facts and metrics based on various frameworks against which the industry standard can be measured and compared with your company's internal research.</p>
		    	</div>
		    </div>
	    </div>
	</div>
</div>
<?php $this->load->view('templates/web_footer') ?>