<?php $this->load->view('templates/web_header') ?>
<?php
$a = mt_rand(0, 9);
$b = mt_rand(0, 9);
$c = $a + $b;
?>
<section class="mt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="bx-sdw ">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 px-4">
                            <h3 class="clr-grey px-4"><b>Order a Free Sample Report</b></h3>
                            <h1 class="px-4" style="font-size: 20px; color: #246A9F;line-height: 1.2;">
                                <strong><a href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>"><?= $Record['title'] ?></a></strong>
                            </h1>
                            <form method="POST" class="req-formnew">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4 input-container" >      
                                    <input type="hidden" name="report" value="<?= $Record['title'] ?>">
                                    <i class="fa fa-user icon"></i>
                                    <input type="text" value="" placeholder="Type Your Name Here" name="name" class="inp_dsgn11" required>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4" > 
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 input-container" >
                                            <i class="fa fa-envelope icon"></i>
                                            <input type="email" value="" placeholder="Business Email Id" name="email" class="inp_dsgn11" required>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 input-container" >
                                            <i class="fa fa-phone icon"></i>
                                            <input type="text" value="" placeholder="Phone Number" name="contact" id="contact" class="inp_dsgn11" required>
                                            <p class="error" id="error" style="color: red;"></p>
                                        </div>     
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4" >  
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 input-container" >
                                            <i class="fa fa-building icon"></i>
                                            <input type="text" value="" placeholder="Company" name="company" class="inp_dsgn11" id="company" required>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 input-container">
                                            <i class="fa fa-briefcase icon"></i>
                                            <input type="text" value="" placeholder="Title / Designation" name="title" class="inp_dsgn11" required>   
                                        </div>
                                    </div>    
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4 input-container" >
                                    <i class="fa fa-globe icon"></i>
                                    <select class="inp_dsgn11"  name="country" id="country" required="">
                                        <option selected="true" disabled="disabled">Please Select Your Country </option>
                                        <?php foreach ($countries as $key => $value): ?>
                                            <option value="<?= $value ?>"><?= htmlspecialchars($value) ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4 input-container">      
                                    <i class="fa fa-comment icon"></i>
                                    <textarea value="" placeholder="Share With us any information that might help us better respond to your request." class="inp_dsgn11" name="message" rows="3"></textarea>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4" >      

                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mt-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <b style="color: #000; font-size: 20px; "><?= $a ?> + <?= $b ?> =</b>
                                            </span>
                                            <span class="input-container">

                                                <i class="fa fa-lock icon"></i>
                                                <input type="text" class="inp_dsgn11" id="security" placeholder="Security Code" autocomplete="off" required>
                                            </span>
                                        </div>
                                        <p class="mt-4">Your personal details are safe with us. <a href="https://www.reportsmonitor.com/privacypolicy" target="_blank"> privacy</a></p>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12 text-center" style="margin-top: 10px;">
                                        <input type="submit" id="submit" class="btn btn-info" value="Submit" disabled>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 bg-lgblue clr-white">
                            <div class="px-4 mt-4">
                                <?php if ($Record['published_date'] == '1970-01-01'): ?>

                                    <b>Published Date</b> : 21 Aug 2018
                                    <?php else: ?>
                                        <b>Published Date</b> : <?= date('d M Y', strtotime($Record['published_date'])) ?>
                                        <?php endif ?> <br>
                                        <strong>Category </strong>: <a href="<?= base_url() . 'categories/' . $Record['cat_id'] ?>" class="clr-white"><?= $this->data_model->get(array('id' => $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></a> 
                                        <br>
                                        <strong> <?= $this->data_model->get(array('id' => $Record['publisher_id']), NULL, array('name'), NULL, 'publisher')[0]['name'] ?></strong>
                                        <br>

                                        <strong>Pages </strong>: <?= ($Record['pages'] > 0) ? $Record['pages'] : 'N/A'; ?>
                                    </div>


                                    <div class="col-sm-12 why-box mb-10 py-4 mt-4">
                                        <h4 class="text-center">Why Choose Us</h4>
                                        <div class="brd-box custom-ul">
                                            <ul class="mt-4">
                                                <li><b>Lowest Price Guarantee</b></li>
                                                We offer the lowest prices for the listed reports
                                                <li><b>Data Security</b></li>
                                                Your data is safe and secure
                                                <li><b>Vast Report Database</b></li>
                                                We have more than 2 Million reports in our database
                                                <li><b>Client Focused</b></li>
                                                Personalized updates and 24*7 support
                                                <li><b>Trusted Source and Quality</b></li>
                                                We only work with reputable partners providing high quality research and support
                                                <li><b>Market Segmentation</b></li>
                                                We provide alternative views of the market to help you identify where the real opportunities lie
                                                <li><b>Bulk Discounts</b></li>
                                                We offer great discounts on purchase of multiple reports
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt5 bg-grey">
        <div class="container mb-10">
            <div class="row key-topic mb-30">
                <div class="col-md-12 text-center py-4">
                    <h3 class="clr-blu"><b>Key Topics Covered</b></h3>
                </div>   
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <ul>
                        <li>Market Factors (Including Drivers and Restraint)</li>
                        <li>Market Trends</li>
                        <li>Market Estimates and Forcasts</li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul>
                        <li>Competitive Analysis</li>
                        <li>Future Market Opportunities</li>
                    </ul>
                </div>             
            </div>
        </div>
    </section>
    <?php $this->load->view('templates/web_footer') ?>
    <script type="text/javascript">
        $(document).ready(function ()
        {
            var res = <?= $c ?>;
            var v = 0;
            var checked_status = 0;
            $("#security").focusout(function () {
                v = this.value;
            });
        /*$("#terms").click(function() {
         checked_status = this.checked;
         
         if (checked_status == true && v == res)
         {
         $("#submit").removeAttr("disabled");
         }
         else
         {
         $("#submit").attr("disabled", "disabled");
         }
         });
         */
         $("#contact").keyup(function ()
         {

            inputtxt = this.value;
            var phoneno = /^\+?[0-9]*$/;
//            var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
if (inputtxt.match(phoneno))
{
    $("#error").html("");
    return true;
} else
{
    $("#error").html("Not a valid Phone Number");
    $("#submit").attr("disabled", "disabled");
    return false;
}

});


         $("#security").keyup(function ()
         {
            v = this.value;
            if (v == res)
            {
                $("#submit").removeAttr("disabled");
            } else
            {
                $("#submit").attr("disabled", "disabled");
            }
        });
     });

 </script>


