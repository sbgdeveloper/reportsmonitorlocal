<?php $this->load->view('templates/web_header') ?>
<?php
$a = mt_rand(0, 9);
$b = mt_rand(0, 9);
$c = $a + $b;
?>
<div class="container">
    <div class="row">

        <div class="col-md-9 col-sm-9 col-xs-12 mt-20">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.jpg" class="img-responsive center-block" alt="<?= $Record['meta_keyword'] ?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10 ttlsubcat1">
                    <h1 style="font-size: 20px; color: #246A9F;"><strong><?= $Record['title'] ?></strong></h1>
                    <ul class="list-inline blginfo pt-10 mb-20" row>
                        <?php if ($Record['published_date'] == '1970-01-01'): ?>

                            <li class="col-md-6"><strong>Published Date </strong>: 21 Aug 2018</li>
                        <?php else: ?>
                            <li class="col-md-6"><strong>Published Date </strong>: <?= date('d M Y', strtotime($Record['published_date'])) ?></li>
                        <?php endif ?>
                        <li class="col-md-6"><strong> <?= $this->data_model->get(array('id' => $Record['publisher_id']), NULL, array('name'), NULL, 'publisher')[0]['name'] ?></strong></li> 
                        <li class="col-md-6"><strong>Category </strong>: <a href="<?= base_url() . 'categories/' . $Record['cat_id'] ?>"><?= $this->data_model->get(array('id' => $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></a></li>
                        <li class="col-md-6"><strong>Pages </strong>: <?= ($Record['pages'] > 0) ? $Record['pages'] : 'N/A'; ?></li>
                        <!-- <li class="col-md-6"><a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px"><strong>Request a free sample</strong></a></li>
                        <li class="col-md-6"><a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px"><strong>Check Discount</strong></a></li> -->
                    </ul>

                </div>
                <div class="col-md-12 mt-20">	
                    <ul class="nav nav-tabs" style="height: 50px; ">
                        <li  style="margin: 0px 2px;"> <a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>" style="background-color:#f1f1f1;height: 50px; padding: 10px;">Report Details </a></li>
                        <?php if ($Record['toc'] != NULL): ?>
                            <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Table of Content</a></li>
                        <?php endif ?>
                        <?php if ($Record['list_tbl_fig'] != NULL): ?>
                            <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>report/<?= $Record['id'] . "/" . str_replace(' ', '-', $Record['meta_keyword']) ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Tables & Figures</a></li>
                        <?php endif ?>
                        <li class="active" style="margin: 0px 2px;"><a  href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"style="background-color:#246A9F; height: 50px; padding: 10px;color: white;" class="">Get Free Sample <i class="fa fa-download" aria-hidden="true" style="margin-left:5px; "></i></a></li>
                        <li style="margin: 0px 2px;"><a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Check Discount</a></li>
                    </ul>                          
                </div>
            </div>
            <br>
            <div class="row" style="border: 1px solid #928F8E;  border-radius: 10px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form method="POST" >
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                            <h3 class="authortitle">Order a Free Sample Report</h3>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Name *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
                                    <input type="hidden" name="report" value="<?= $Record['title'] ?>">
                                    <input type="text" value="" placeholder="&nbsp;&nbsp;Type Your Name Here" name="name" class="inp_dsgn11" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Email *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <input type="email" value="" placeholder="&nbsp;&nbsp;Please Provide Your Business  Email Id" name="email" class="inp_dsgn11" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Phone Number *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <input type="text" value="" placeholder="&nbsp;&nbsp;Direct Line Makes it easier to say Hello" name="contact" class="inp_dsgn11" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Title / Designation *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <input type="text" value="" placeholder="&nbsp;&nbsp;Please Type Here" name="title" class="inp_dsgn11" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Company *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <input type="text" value="" placeholder="&nbsp;&nbsp;Please Type Here" name="company" class="inp_dsgn11" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Country *</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <select class="inp_dsgn11"  name="country">
                                        <option selected="true" disabled="disabled">Please Select Your Country </option>
                                        <?php foreach ($countries as $key => $value): ?>
                                            <option value="<?= $value ?>"><?= htmlspecialchars($value) ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label class="inp_dsgn11"><b>Message</b></label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <textarea value="" placeholder="&nbsp;&nbsp; Share With us any information that might help us better respond to your request." class="inp_dsgn11" name="message" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >		
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" s>
                                    <!-- <div class="g-recaptcha" data-sitekey="6LcvGGYUAAAAAHi9R1s48rTNPSK9CVMOBXHsxTo3" data-callback="enableBtn"></div> -->
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <b style="color: #000; font-size: 20px; "><?= $a ?> + <?= $b ?> =</b>
                                        </span>
                                        <input type="text" class="inp_dsgn11" style="margin-bottom:  20px; padding: 0px;" id="security" placeholder="&nbsp;&nbsp;Security Code" autocomplete="off" required>
                                    </div> 
                                </div>
                            </div>



                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" s>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                            <?= $a ?> + <?= $b ?> =
                                            </span>
                                    <input type="text" class="inp_dsgn11" style="margin-bottom:  20px; padding: 0px;" id="security" placeholder="&nbsp;&nbsp;Security Code" required>
                            </div> -->

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <!-- <label>
                                        <input type="checkbox" id="terms" style="height: 20px; width: 20px; " required>
                                   <strong> I agree to have read and accept the <a target="_blank" href="<?= base_url() ?>terms_conditions">Terms and Conditions</a> of reportsmonitor.com</strong>
                                </label> -->
                                <p>Your personal details are safe with us. <a href="https://www.reportsmonitor.com/privacypolicy" target="_blank"> privacy</a></p>
                                <input type="submit" id="submit" class="btn btn-info" value="Submit" disabled>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="pt-10  text-center">
                    <!-- <a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px">
                            <strong>Request a free sample</strong>
                    </a> -->
                <a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"class="btn btn-info" style="border-radius: 10px">
                    <strong>Request a free sample</strong>
                </a>
                <a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px">
                    <strong>Check Discount</strong>
                </a>
            </div> 
            <div class="row">
                <div class="col-md-12">
                    <?php if ($Related): ?>
                        <h3><strong>Related Reports</strong></h3>
                        <?php foreach ($Related as $Rel): ?>
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-2 pad05">
                                        <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.png" class="img-responsive pt-10" alt="<?= str_replace(' ', '-', $Rel['meta_keyword']) ?>">
                                    </div>
                                    <div class="col-sm-9 col-xs-10">
                                        <p class="mb-5">
                                            <a style="color: #3C4858 !important" href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>"><strong><?= $Rel['title'] ?></strong></a>
                                        </p>
                                        <p class="paradesc">
                                            <?= substr($Rel['report_desc'], 0, 200) . ' ...' ?><a href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>" class="label label-info">read more</a>
                                        </p>
                                    </div>
                                    <div class="col-sm-2 hidden-xs yrs">
                                        <h4><?= date('F Y', strtotime($Rel['published_date'])) ?></h4>
                                    </div>
                                </div>
                            </blockquote>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 mt-20">
            <div class="row">
                <div class="col-sm-12 surdds mb-20">
                    <h4 class="text-center">Choose License Type</h4>
                    <div class="headttl">
                        <form action="<?= base_url() . 'buyNow/' . $Record['id'] ?>" method="get">

                            <select class="form-control slectdrop1" name="price" style="font-weight: bold;">
                                <?php if ($Record['single_price'] != NULL): ?>
                                    <option value="single_price">Single User - $ <?= $Record['single_price'] ?></option>
                                <?php endif ?>
                                <?php if ($Record['multi_price'] != NULL): ?>
                                    <option value="multi_price">
                                    <b>Multi User - $<?= $Record['multi_price'] ?></b>
                                    </option>
                                <?php endif ?>

                                <?php if ($Record['ent_price'] != NULL): ?>
                                    <option value="ent_price">
                                    <b>Enterprise User - $<?= $Record['ent_price'] ?></b>
                                    </option>
                                <?php endif ?>
                            </select>

                            <div class="tooltip1"> 
                                <i class="fa fa-question-circle" style="float:right;font-size: 22px;"></i>
                                <span class="tooltiptext1"><p><b style="color: #06d4d2">Single User - </b>This is a single user license, allowing one specific user access to the product.</p>
                                    <p ><b style="color: #06d4d2">Multi User - </b>This is a 1-5 user licence, allowing up to five users have access to the product.</p>
                                    <p><b style="color: #06d4d2">Enterprise User - </b>This is an enterprise license, allowing all employees within your organisation access to the product. The report will be emailed to you.</p></span>
                            </div>

                            <div class="text-center" style="margin: 0px; padding: 0px;">
                                <button type="submit" style="border-radius: 10px; line-height: 0px;font-weight: bold; " class="btn btn-info" value="Buy Now">Buy Now <i class="fa fa-shopping-cart" style="margin-left:10px ; color: #FFF;"></i></button>
                                <!-- <input type="submit"style="border-radius: 10px;font-weight: bold; line-height: 0px; height: 26px;" class="btn btn-info" value="Buy Now"> -->
                                <img src="<?php echo base_url() . 'web_assets/' ?>img/cards.png" style="float:left; width: 250;" class="img-responsive center-block" alt="Cards For Payment ">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12 surdds mb-20">
                    <img src="<?php echo base_url() . 'web_assets/' ?>img/Badge.png" style="height: 80px; float: left; margin-left: 20px;" class="img-responsive center-block" alt="Trusted Badge">
                    <img src="<?php echo base_url() . 'web_assets/' ?>img/lets.png" style="float: left; height: 80px;" class="img-responsive center-block" alt="SSL Certified">
                   <!--<img src="<?php echo base_url() . 'web_assets/' ?>img/Secured & Verified.png" style="float: left; margin-left: 20px; height: 80px;" class="img-responsive center-block"> -->
                    <img src="<?php echo base_url() . 'web_assets/' ?>img/Premium Quality.png" style="height: 80px;" class="img-responsive center-block" alt="Premium Quality">
                </div>
                <div class="col-sm-12 surdds mb-10">
                    <h4 class="text-center">Why Choose Us</h4>
                    <div class="headttl">
                        <ul style="padding:0px; margin: 15px; line-height: 20px;">
                            <li><b>Lowest Price Guarantee</b></li>
                            We offer the lowest prices for the listed reports
                            <li><b>Data Security</b></li>
                            Your data is safe and secure
                            <li><b>Vast Report Database</b></li>
                            We have more than 2 Million reports in our database
                            <li><b>Client Focused</b></li>
                            Personalized updates and 24*7 support
                            <li><b>Trusted Source and Quality</b></li>
                            We only work with reputable partners providing high quality research and support
                            <li><b>Market Segmentation</b></li>
                            We provide alternative views of the market to help you identify where the real opportunities lie
                            <li><b>Bulk Discounts</b></li>
                            We offer great discounts on purchase of multiple reports
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 surdds mb-10">
                    <h4 class="text-center">How To Reach Us</h4>
                    <div class="headttl">
                        <p>
                            Reach out to our most senior Sales Professional- <b>Jay Matthews</b> <br>
                            <i class="fa fa-phone-square" style="font-size:24px"></i> &nbsp;&nbsp;&nbsp;+1 513 549 5911 (US) <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+44 203 318 2846 (UK)<br>
                            <i class="fa fa-envelope-o" style="font-size:24px"></i><b>&nbsp;&nbsp;&nbsp;<a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a></b> </p>   
                    </div>
                </div>
                <div class="col-sm-12 surdds mb-10">
                    <h4 class="text-center">Our Clients</h4>
                    <div class="headttl">
                        <marquee>
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Intel.png"  style="width: 250px; height: 130px;" alt="Intel">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Samsung.png" style="width: 250px; height: 130px;" alt="Samsung">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/3M.png" style="width: 250px; height: 130px;" alt="3M">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/BCG.png" style="width: 250px; height: 130px;" alt="BCG">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/LEK Consulting.png" style="width: 250px; height: 130px;" alt="LEK Consulting">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/McKinsey.png" style="width: 250px; height: 130px;" alt="McKinsey">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Thyssenkrupp.png" style="width: 250px; height: 130px;" alt="Thyssenkrupp">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/download.jpg" style="width: 250px; height: 130px;" alt="Mogensen">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/BASF.png" style="width: 250px; height: 130px;" alt="BASF">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/IQVIA.png" style="width: 250px; height: 130px;" alt="IQVIA">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Hella.png" style="width: 250px; height: 130px;" alt="Hella">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Oracle.png" style="width: 250px; height: 130px;" alt="Oracle">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Accenture.png" style="width: 250px; height: 130px;" alt="Accenture">
                            <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Saint Gobain.png" style="width: 250px; height: 130px;" alt="Saint Gobain">

                        </marquee>
                    </div>
                </div>
                <div class="col-sm-12 surdds mb-10">
                    <h4 class="text-center">Testimonials</h4>
                    <div class="headttl">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                        <div class="w3-content w3-section" style="max-width:400px">
                            <div class="mySlides">
                                <p> Well structured, the insights they shared with us were very helpful and reliable. Their timely assistance make their services invaluable to us. I would highly recommend them and would definitely use them again in the future if needed. <br>
                                    -<b>VP of a Automotive division in Germany</b></p>
                            </div>
                            <div class="mySlides">
                                <p>The report sent to us was on the point, and its information was quite extensive, well structured, and well researched. More importantly what we valued was your response time and professionalism. As a leading global consulting firm, our clients expect high quality deliverables in short periods of time, so a reliable research partner is essential. For the price that you have charged the quality of your services were exceptional. We look forward to continue our relationship with your team on future engagements <br>
                                    -<b>Product Manager at US based Manufacturer</b></p>
                            </div>
                            <div class="mySlides">
                                <p>Coherent, high-quality, thoroughly-researched reports. We received a very quick response to all our queries which eventually expedited the entire process <br>
                                    -<b>-Marketing Manager at a pharma company in Belgium</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 surdds mb-50">
                    <form method="POST">
                        <h4 class="text-center">Enquire Before Buying</h4>
                        <div class="headttl">
                            <p>Need to ask a quick question before buying? Please fill out this short form:</p>
                            <div class="form-group is-empty">
                                <input type="text" name="name" placeholder="Name" class="form-control" required>
                            </div>
                            <div class="form-group is-empty">
                                <input type="text" name="email" placeholder="Email" class="form-control" required>
                            </div>
                            <div class="form-group is-empty">
                                <input type="text" name="phone" placeholder="Phone" class="form-control" required>
                            </div>
                            <input type="hidden" name="action" value="enquity">
                            <input type="hidden" name="reportId" value="<?= $Record['id'] ?>">
                            <input type="hidden" name="report" value="<?= $Record['title'] ?>">
                            <div class="text-center">
                                <input type="submit" value="Submit" class="btn btn-info" style="border-radius: 10px">
                            </div>
                        </div>
                    </form>
                </div>         

                <!-- <div class="col-sm-12 surdds mb-50">
                        <h4>Order a Free Sample</h4>
                        <div class="headttl">
                                <p>Checkout the sample of this report. We will send a free sample across to your mailbox shortly.</p>                            
                                <p class="mt-20 text-center">
                                        <a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px">
                                                <strong>Request a free sample</strong>
                                        </a>
                                        <a href="#pill4" data-toggle="tab" class="btn btn-info" style="border-radius: 10px">
                        <strong>Request a free sample</strong>
                </a>
                                </p>
                        </div>
                </div> -->
                <!-- <div class="col-sm-12 surdds mb-50">
                        <h4>Check Discount</h4>
                        <div class="headttl">
                                <p>Interested in buying this report?<br/>Know about the current offers OR Ask for a discount!</p>                            
                                <p class="mt-20 text-center"><a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px"><strong>Check Discount</strong></a></p>
                        </div>
                </div> -->

            </div><!--row ends here-->
        </div>
    </div>

</div>
<!-- 
<div id="return-to-top" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
                                <img src="<?= base_url() ?>web_assets/images/RmLogo.png" class="img-responsive logo_img" alt="Market Research Vision">
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 ">
                                        <b class="bar_ttl "><?= $Record['title'] ?></b>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 mp-0">
                                <a href="<?= base_url() . 'check_discount/' . $Record['id'] ?>" class="btn rqst_btn2"><b>Check Discount</b></a>

                                <a href="<?= base_url() . 'request_sample/' . $Record['id'] ?>" class="btn ck_disk_btn blings"><b>Request Sample</b> </a>
                        </div>
                </div>
                
        </div> -->
<?php $this->load->view('templates/web_footer') ?>
<script type="text/javascript">
    $(document).ready(function ()
    {
        var res = <?= $c ?>;
        var v = 0;
        var checked_status = 0;
        $("#security").focusout(function () {
            v = this.value;
        });
        /*$("#terms").click(function() {
         checked_status = this.checked;
         
         if (checked_status == true && v == res)
         {
         $("#submit").removeAttr("disabled");
         }
         else
         {
         $("#submit").attr("disabled", "disabled");
         }
         });
         */
        $("#security").keyup(function ()
        {
            v = this.value;

            if (v == res)
            {
                $("#submit").removeAttr("disabled");
            } else
            {
                $("#submit").attr("disabled", "disabled");
            }
        });
    });
</script>

<script>
    var myIndex = 0;
    carousel();
    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {
            myIndex = 1
        }
        x[myIndex - 1].style.display = "block";
        setTimeout(carousel, 10000);
    }
</script>
