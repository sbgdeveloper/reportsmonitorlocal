<?php $this->load->view('templates/web_header') ?>
	<div class="container">
		<?php if (!isset($Record)): ?>
			<div class="row mt-30">
				<div class="col-md-12 col-sm-12 col-xs-12 mb-30 mb-xs-10">
					<h1 class="cattitle">Categories</h1>
					<p>Our databank covers 14 major Industries and Categories. We offer huge numbers of reports and other information products, covering the front line and major issues in all industry sectors. Reports around here also comprise the profiles of leading organizations around the world. Majority of them include a SWOT analysis. It would be ideal if you browse our categories beneath and feel free to Contact Us for any questions as well as proposals.</p> 
				</div>
			</div><!--row ends here-->
			<div class="row mb-50 mb-xs-10">
				<?php $i=1; ?>
				<?php foreach ($categories as $category): ?>
					<div class="col-md-15 col-sm-3 col-xs-6 <?php if($i<11) echo 'mb-50 mb-xs-10' ?>">
						<?php if ($i==5||$i==10): ?>
							<img src="<?=base_url() ?>web_assets/images/brdrrtbt2.png" class="img-responsive hidden-xs" alt="Border">
						<?php elseif($i==11||$i==12||$i==13||$i==14): ?>
							<img src="<?=base_url() ?>web_assets/images/brdrrtbt3.png" class="img-responsive hidden-xs" alt="Border">
						<?php elseif($i==1||$i==2||$i==3||$i==4||$i==6||$i==7||$i==8||$i==9): ?>
							<img src="<?=base_url() ?>web_assets/images/brdrrtbt.png" class="img-responsive hidden-xs" alt="Border">
						<?php endif ?>
						<div class="brdrrtbt">
							<a href="<?=base_url().'categories/'.$category['id'].'/'.str_replace(" ", "-", $category['name'])?>">
								<img src="<?=base_url()?>uploads/category/<?=$category['img'] ?>" class="img-responsive center-block" alt="<?=$category['name']  ?>">
								<h2 style="font-size: 14px; margin: 0; font-weight: 600;" class="text-center pt-20"><?=$category['name']  ?></h2>
							</a>
						</div>
					</div>
					<?php if ($i==2 || $i==4 || $i==6 || $i==8 || $i==10): ?>
						<div class="hidden-md hidden-sm hidden-lg clearfix"></div>
					<?php endif ?>
					<?php $i++; ?>
				<?php endforeach ?>
			</div>
		<?php endif ?>
		<?php if (isset($Record)): ?>
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12" id="Records">
					<h1 class="enrgytitle"><?=$Record['name'] ?></h1>
					<p>
						<?=$Record['description'] ?>
					</p>

					<h3><strong>List of Reports</strong></h3>
					<?php if ($Records): ?>
						<?php foreach ($Records as $Rec): ?>
							<blockquote>
								<div class="row">
									<div class="col-sm-1 col-xs-2 pad05">
										<img src="<?=base_url() ?>web_assets/images/pinit.png" class="img-responsive pt-10" alt="<?=str_replace(' ', '-', $Rec['meta_keyword']) ?>">
									</div>
									<div class="col-sm-9 col-xs-10">
										<p class="mb-5">
										 	<a style="color: #3C4858 !important" href="<?=base_url().'report/'.$Rec['id'].'/'.str_replace(' ', '-', $Rec['meta_keyword']) ?>">
										 		<strong><?=$Rec['title'] ?></strong>
										 	</a>
										</p>
										<p class="paradesc">
											<?=substr($Rec['report_desc'], 0, 100).' ...' ?><a href="<?=base_url().'report/'.$Rec['id'].'/'.str_replace(' ', '-', $Rec['meta_keyword']) ?>" class="label label-info">read more</a>
										</p>   
									</div>
									<div class="col-sm-2 hidden-xs yrs">
										<p><?=date('F Y', strtotime($Rec['published_date'])) ?></p>
										<!-- <span><strong>$4500</strong></span> -->
									</div>
								</div>
							</blockquote>
						<?php endforeach ?>
					<?php else: ?>
						<h3 class="text-center">No Records Found</h3>
					<?php endif ?>

					<?php $this->load->view('templates/pagination') ?>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<h2 style="font-size: 36px;" class="filterttl">Filters</h2>
					<h3 class="mb-10" style="border-bottom: 1px solid"><strong>Domains</strong></h3>
					<div class="scrollbar" id="style-4">
						<div class="force-overflow">
							<div class="panel-body">
								<?php foreach ($categories as $category): ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" class="catChk" name="catChk" id="<?=$category['id'] ?>" value="<?=$category['id'] ?>" data-toggle="checkbox"<?php if($Record['id']==$category['id']) echo "checked"; ?>>
											<?=$category['name'] ?>
										</label>
									</div>
								<?php endforeach ?>
							</div>
						</div>
					</div>
					<h3 class="mb-10" style="border-bottom: 1px solid"><strong>Geographical</strong></h3>
					<div class="scrollbar" id="style-4">
						<div class="force-overflow">
							<div class="panel-body">
								<?php foreach ($regions as $region): ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" class="regChk" name="regChk" id="<?=$region['id'] ?>" value="<?=$region['id'] ?>" data-toggle="checkbox" <?php if(isset($reg)) echo "checked"; ?>>
											<?=$region['name'] ?>
										</label>
									</div>
								<?php endforeach ?>
							</div>
						</div>
					</div>
					<!-- <h3 class="mb-0" style="border-bottom: 1px solid"><strong>Duration</strong></h3>
					<div class="mb-0" id="style-4">
						<div class="panel-body">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="All" class="durChk" name="durChk" id="All" data-toggle="checkbox" checked="">
									All
								</label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="2015" class="durChk" name="durChk" id="2015" data-toggle="checkbox">
									2015
								</label>
							</div>
		
							<div class="checkbox">
								<label>
								   <input type="checkbox" value="2016" class="durChk" name="durChk" id="2016" data-toggle="checkbox">
								   2016
								</label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="2017" class="durChk" name="durChk" id="2017" data-toggle="checkbox">
									2017
								 </label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="2018" class="durChk" name="durChk" id="2018" data-toggle="checkbox">
									2018
								</label>
							</div>
						</div>
					</div> -->
					<!-- <h3 class="mb-10" style="border-bottom: 1px solid"><strong>Price Range</strong></h3>
					<div class="mb-0" id="style-4">
						<div class="panel-body">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="All" class="priceChk" name="priceChk" data-toggle="checkbox" checked="" id="All">
									All
								</label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="$0 - $1000" class="priceChk" name="priceChk" data-toggle="checkbox" id="$0 - $1000">
									$0 - $1000
								</label>
							</div>
		
							<div class="checkbox">
								<label>
								   <input type="checkbox" value="$1001 - $2000" class="priceChk" name="priceChk" data-toggle="checkbox" id="$1001 - $2000">
								   $1001 - $2000
								</label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="$2001 - $3000" class="priceChk" name="priceChk" data-toggle="checkbox" id="$2001 - $3000">
									$2001 - $3000
								 </label>
							</div>
		
							<div class="checkbox">
								<label>
									<input type="checkbox" value="$3000 and above" class="priceChk" name="priceChk" data-toggle="checkbox" id="$3000 and above">
									$3000 and above
								</label>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		<?php endif ?>
		
	</div>
<?php $this->load->view('templates/web_footer') ?>
<script type="text/javascript">
	$(document).ready(function()
	{
		// $('input:checkbox').change(function ()
		// {
		//     alert('changed');
		// });
		$("input:checkbox").on('change',function()
		{
			var categories = getCheckCategories('catChk');
			var regions = getCheckCategories('regChk');
			var duration = getCheckCategories('durChk');
			var price = getCheckCategories('priceChk');
			// alert(categories+"\n"+regions+"\n"+duration+"\n"+price);
			$.ajax({
				type: 'POST',
				url: '<?=base_url()?>/filter',
				data: { 
					'categories' : categories,
					'regions' : regions,
					'duration' : duration,
					'price' : price
					// 'foo': 'bar', 
					// 'ca$libri': 'no$libri' // <-- the $ sign in the parameter name seems unusual, I would avoid it
				},
				success: function(msg){
					// alert(msg);
					$('#Records').html(msg);
				}
			});
		});

		function getCheckCategories(a)
		{
			var reportList = $("."+a);
			var list = [];
			$.each(reportList,function(i,item){
				if(item.checked == true){
					list.push(item.id);
				}
			});
			return list;
		}
	});
</script>
