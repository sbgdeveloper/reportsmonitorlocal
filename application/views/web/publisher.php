<?php $this->load->view('templates/web_header') ?>
<div class="container">
		<div class="col-md-12 col-sm-12 col-xs-12 mb-20 mb-xs-10" style="margin: 0px;padding: 0px;">
			<h1 class="authortitle">Publisher: <?=$Record['name'] ?></h1>
		</div>
		<!-- <?=print_r($Records) ?> -->
		<?php if ($Records): ?>
			<?php foreach ($Records as $Rec): ?>
				<div class="col-md-12 col-sm-12 col-xs-12 ttlsubpub mb-20 mt-xs-10" style="margin: 0px;padding: 0px;">
					<div class="col-md-1 col-sm-12 col-xs-12" style="margin: 0px;padding: 0px;">
						<br><img src="<?php echo base_url().'web_assets/' ?>images/Market-Research-Report.jpg" style="height:100px;width: 80px;" alt="<?=str_replace(' ', '-', $Rec['meta_keyword'])?>">
					</div>
					<div class="col-md-11 col-sm-12 col-xs-12">
						<h4 style="font-weight: bold;"><a style="color: #3C4858 !important" href="<?=base_url()?>report/<?=$Rec['id'] ?>/<?=str_replace(' ', '-', $Rec['meta_keyword'])?>"><?=$Rec['title'] ?></a></h4>
						<p>
						<?=substr($Rec['report_desc'], 0, 400).' ...' ?>
						<a href="<?=base_url()?>report/<?=$Rec['id'] ?>/<?=str_replace(' ', '-', $Rec['meta_keyword'])?>" class="label label-info">read more</a>
					</p>
					</div>
				</div>
			<?php endforeach ?>
		<?php else: ?>
			<h3 class="text-center">No Records Found</h3>
		<?php endif ?>

		<?php $this->load->view('templates/pagination') ?>
	
</div>
<?php $this->load->view('templates/web_footer') ?>
