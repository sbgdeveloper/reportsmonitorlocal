<?php $this->load->view('templates/web_header') ?>
<!-- Schema Markup Add -->
	<script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "mainEntityOfPage": {
	    "@type": "WebPage",
	    "@id": "https://google.com/article"
	  },
	  "@type" : "Article",
	  "name" : "Reports Monitor",
	  "author" : {
	    "@type" : "Person",
	    "name" : "Research Report"
	  },
	  "dateModified": "2018-11-07",
	  "headline": "Monitor your market through our reports",
	  "datePublished" : "2017-01-07",
	  "image" : "https://www.reportsmonitor.com/web_assets/images/ReportIcon.png",
	  "articleSection" : "Reports",
	  "articleBody" : "At Reports Monitor, we believe you deserve to have research and analysis designed with your needs, situation and category in mind. While we have many proven tools to meet business needs, we don’t force “canned” products or “black boxes.” We offer flexible solutions that drive actionable insights.</P>\n\t\t\t\t<P>Our market research reports empower organisations and brands of all sizes with strategic data, analysis and production trends in a single, affordable resource. Our extensive network of in-country strategic publishers provide the depth of local business information required in today&#39;s international business environment",
	  "url" : "https://www.reportsmonitor.com/",
	  "publisher" : {
	    "@type" : "Organization",
	    "name" : "Reports Monitor",
	    "logo": {
	      "@type": "ImageObject",
	      "url": "https://www.reportsmonitor.com/web_assets/images/logo.png"
	    }
	  },
	  "aggregateRating" : {
	    "@type" : "AggregateRating",
	    "ratingValue" : "5",
	    "bestRating" : "5",
	    "worstRating" : "3.5",
	    "ratingCount" : "1029"
	  }
	}
</script>
	<!-- End schema -->
	<div class="container">
		<div class="row mt-30">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<h2 style="font-size: 36px;" class="abouttitle">About Us</h2>
				<p>Reports Monitor managed to establish itself as a leading provider of market research. Reports Monitor delivers the meaningful results you need to make confident business decisions and drive change throughout your organization. We create intelligent market research, designed to ensure the highest levels of accuracy. We apply deep industry expertise and advanced analytics to see beyond the data and into the behaviours and motivators that drive customer decisions. <a href="<?=base_url() ?>about-us" class="label label-info">more</a></p>
				
				<h2 style="font-size: 36px;" class="wwdtitle mt-50 mt-xs-20">What We Do</h2> 
				
				<div class="carousel slide pt-30 mb-50" id="myCarousel">
					<div  class="whatdo">
						<div class="item">
							<a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/ReportIcon.png" class="img-responsive center-block" width="170" height="114" alt="Syndicated Market Reports" >
								<h3 class="pt-50" style="font-size: 16px;margin: 0px;">Syndicated Market Reports</h3>
							</a>
						</div>
						<div class="item">
							<a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/ResearchIcon.png" class="img-responsive center-block" width="170" height="114" alt="Customized Research Programme">
								<h3 class="pt-50" style="font-size: 15px;margin: 0px;">Customized Research Programme</h3>
							</a>
						</div>
						<div class="item">
						   <a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/DomainIcon.png" class="img-responsive center-block" width="170" height="114" alt="Domain Specific Analytics">
								<h3 class="pt-50" style="font-size: 15px;margin: 0px;">Domain Specific Analytics</h3>
							</a>
						</div>
						<div class="item">
							<a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/ReportIcon.png" class="img-responsive center-block" width="170" height="114" alt="Syndicated Market Reports">
								<h3 class="pt-50" style="font-size: 15px;margin: 0px;">Syndicated Market Reports</h3>
							</a>
						</div>
						<div class="item">
							<a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/ResearchIcon.png" class="img-responsive center-block" width="170" height="114" alt="Customized Research Programme">
								<h3 class="pt-50" style="font-size: 15px;margin: 0px;">Customized Research Programme</h3>
							</a>
						</div>
						<div class="item">
						   <a href="<?=base_url()?>services">
								<img src="<?php echo base_url().'web_assets/' ?>images/DomainIcon.png" class="img-responsive center-block" width="170" height="114" alt="Domain Specific Analytics">
								<h3 class="pt-50" style="font-size: 15px;margin: 0px;">Domain Specific Analytics</h3>
							</a>
						</div>
										
					</div>
				</div>				 
				<p>At Reports Monitor, we believe you deserve to have research and analysis designed with your needs, situation and category in mind. While we have many proven tools to meet business needs, we don’t force “canned” products or “black boxes.” We offer flexible solutions that drive actionable insights.</p>
				<p>Our market research reports empower organisations and brands of all sizes with strategic data, analysis and production trends in a single, affordable resource. Our extensive network of in-country strategic publishers provide the depth of local business information required in today's international business environment. <a href="<?=base_url()?>services" class="label label-info">more</a></p>
					
			</div><!--left home side-->
			<div class="col-md-4 col-sm-4 col-xs-12 ovrflw">
				<h2 style="font-size: 36px;" class="publictitle">Publications</h2>
				
				<ul class="list-inline">
					<li><h2 class="newsttlbrd" style="font-size: 14px; margin-top: 10px;">Latest Reports</h2></li>
				</ul>             
				
				<div id="carousel-example" class="carousel slide" data-ride="carousel">
					<div class="latestnews">
						<div class="item2">
							<div class="row">
							    <?php if ($letestReports): ?>
    								<?php $i=1; ?>
    								<?php foreach ($letestReports as $letestReport): ?>
    									<div class="col-sm-12">
    										<div class="col-item pb-20 pb-xs-10">
    											<h3 style="font-size: 15px; margin-top: 5px;" class="newstitle">
    												<a style="color: #3C4858 !important" href="<?=base_url()?>report/<?=$letestReport['id'].'/'.str_replace(' ', '-', $letestReport['meta_keyword']) ?>"><?=$letestReport['title']?>
    												</a>
    											</h3>
    											<p>
    												<?=substr($letestReport['report_desc'], 0, 200).' ...' ?>
    												<a class="label label-info" href="<?=base_url()?>report/<?=$letestReport['id'].'/'.str_replace(' ', '-', $letestReport['meta_keyword']) ?>">read more</a>
    											</p>
    										</div>
    									</div>
    									<?php if ($i==3): ?>
    										</div>
    											</div>
    											<div class="item2">
    												<div class="row">
    									<?php endif ?>
    									<?php $i++; ?>
    								<?php endforeach ?>
    							<?php endif ?>
							</div>
						</div>
					</div>      
				</div> <!--side bar latest news heds here--> 
			</div><!--right side bar-->
		</div><!--row ends here-->
		
		<h2 style="font-size: 36px;" class="ourftitle mt-50">Our Features</h2> 
		<!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, unknown printer took a galley of type and scrambled it to make a type specimen book. </p> -->
		<div class="row">
			<div class="featurebg">
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/globeIcon.png" class="img-responsive center-block" width="130" height="122" alt="Wide Network of Publishers">
					<p class="text-center"><strong>Wide Network of Publishers</strong></p>
					<p class="text-center">Access to over 1M+ market research reports</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/medalIcon.png" class="img-responsive center-block" width="130" height="122" alt="Trusted by the Best">
					<p class="text-center"><strong>Trusted by the Best</strong></p>
					<p class="text-center">50+ Clients in Fortune 500</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/pricingIcon.png" class="img-responsive center-block" width="130" height="122" alt="Competitive Pricing">
					<p class="text-center"><strong>Competitive Pricing</strong></p>
					<p class="text-center">Our regular promotional offers and discounts make us the most economic seller of market reports</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<img src="<?php echo base_url().'web_assets/' ?>images/cardIcon.png" class="img-responsive center-block" width="130" height="122" alt="Safe and Secure">
					<p class="text-center"><strong>Safe and Secure</strong></p>
					<p class="text-center">Your payment details are protected by us.</p>                
				</div>
			</div>
		</div><!--our feature ends here-->
		
	 
		<div class="carousel slide pt-30 mb-50" id="myCarousel">
			<div  class="brand">
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Accenture.png" class="img-responsive center-block" width="170" height="72" alt="Accenture">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/ArthurDLittle.png" class="img-responsive center-block" width="170" height="72" alt="ArthurDLittle">
				</div>
				<div class="item3 pl-20">
				   	<img src="<?php echo base_url().'web_assets/' ?>images/clients/BCG.png" class="img-responsive center-block" width="170" height="72" alt="BCG">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Caterpillar.png" class="img-responsive center-block" width="170" height="72" alt="Caterpillar">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/DuPont.png" class="img-responsive center-block" width="170" height="72" alt="DuPont">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/HEnergy.png" class="img-responsive center-block" width="170" height="72" alt="HEnergy">
				</div>
				
				<!--<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/hp.png" class="img-responsive center-block" alt="">
				</div>-->
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/InternationalPaper.png" class="img-responsive center-block" width="170" height="72" alt="InternationalPaper">
				</div>
				<div class="item3 pl-20">
				   <img src="<?php echo base_url().'web_assets/' ?>images/clients/Lanxess.png" class="img-responsive center-block" width="170" height="72" alt="Lanxess">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/LincolnUniversity.png" class="img-responsive center-block" width="170" height="72" alt="LincolnUniversity">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Marshall.png" class="img-responsive center-block" width="170" height="72" alt="Marshall">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/MeadowFoods.png" class="img-responsive center-block" width="170" height="72" alt="MeadowFoods">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/NSK.png" class="img-responsive center-block" width="170" height="72" alt="NSK">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Oldworldindustries.png" class="img-responsive center-block" width="170" height="72" alt="Oldworldindustries">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Qualcomm.png" class="img-responsive center-block" width="170" height="72" alt="Qualcomm">
				</div>
				<div class="item3 pl-20">
					<img src="<?php echo base_url().'web_assets/' ?>images/clients/Samsung.png" class="img-responsive center-block" width="170" height="72" alt="Samsung">
				</div>
			</div>
		</div>
	</div><!-- main content container ends here-->
<?php $this->load->view('templates/web_footer') ?>
