<?php $this->load->view('templates/web_header') ?>
<div class="container">
	<div class="row mt-30">
		<div class="col-md-12 col-xs-12 mb-10">

			<h1 class="thanktitle mb-20">Thank You</h1>
			<p>Thank you for contacting <b>Reports Monitor</b>.<br/><br/>

			Your form has been submitted successfully. We will contact you shortly.<br/><br/>
				Warm Regards,<br/>
				Jay Matthews | Corporate Sales Specialist<br/>
				E-mail: <a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a> | Web: <a href="https://www.reportsmonitor.com/">www.reportsmonitor.com</a> <br/>
			</p>
			<div class="error"><strong><?=$this->session->flashdata('flashSuccess')?></strong></div>
		</div>
	</div>
</div>
<?php $this->load->view('templates/web_footer') ?>
<!-- <script type="text/javascript">
	$(document).ready(function()
	{
		window.setTimeout(function(){
			window.location.href = "<?=base_url().'categories'?>";
		}, 10000);
	});
</script> -->