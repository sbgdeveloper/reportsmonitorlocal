<?php $this->load->view('templates/web_header') ?>
<script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type" : "Product",
        "name" : "<?= $Record['title'] ?>",
        "image" : "https://www.reportsmonitor.com/web_assets/images/Market-Research-Report.jpg",
        "description" : "<?php echo(substr(strip_tags($Record['report_desc']), 0, 300)) ?>",
        "sku": "<?= $Record['id'] ?>",
        "mpn": "RM",
        "url" : "https://www.reportsmonitor.com/report/<?= $Record['id'] ?>/<?= str_replace(' ', '-', $Record['meta_keyword']); ?>",
        "brand" : {
        "@type" : "Brand",
        "name" : "Reports Monitor",
        "logo" : "https://www.reportsmonitor.com/web_assets/images/logo.png"
    },
    "review": {
    "@type": "Review",
    "reviewRating": {
    "@type": "Rating",
    "ratingValue": "4",
    "bestRating": "5"
},
"author": {
"@type": "Person",
"name": "Jay Matthews"
}
},
"aggregateRating": {
"@type": "AggregateRating",
"ratingValue": "4.1",
"reviewCount": "119"
},
"offers" : {
"@type" : "Offer",
"url" : "https://www.reportsmonitor.com/report/<?= $Record['id'] ?>/<?= str_replace(' ', '-', $Record['meta_keyword']); ?>",
"price" : "<?= $Record['single_price'] ?>",
"priceCurrency": "USD",
"priceValidUntil": "2020-11-05",
"availability": "https://schema.org/InStock"
}
}
</script>

<?php
$a = mt_rand(0, 9);
$b = mt_rand(0, 9);
$c = $a + $b;
?>
<div class="container">
    <div class="row">

        <div class="col-md-9 col-sm-9 col-xs-12 mt-20">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.jpg" class="img-responsive center-block" alt="<?= $Record['meta_keyword'] ?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10 ttlsubcat1">
                    <h1 style="font-size: 20px; color: #246A9F;"><strong><?= $Record['title'] ?></strong></h1>
                    <ul class="list-inline blginfo pt-10 mb-20" row>
                        <?php if ($Record['published_date'] == '1970-01-01'): ?>

                            <li class="col-md-6"><strong>Published Date </strong>: 21 Aug 2018</li>
                            <?php else: ?>
                                <li class="col-md-6"><strong>Published Date </strong>: <?= date('d M Y', strtotime($Record['published_date'])) ?></li>
                            <?php endif ?>
                            <li class="col-md-6"><strong>  <?= $this->data_model->get(array('id' => $Record['publisher_id']), NULL, array('name'), NULL, 'publisher')[0]['name'] ?></strong></li> 
                            <li class="col-md-6"><strong>Category </strong>: <a href="<?= base_url() . 'categories/' . $Record['cat_id'] ?>"><?= $this->data_model->get(array('id' => $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></a></li>
                            <li class="col-md-6"><strong>Pages </strong>: <?= ($Record['pages'] > 0) ? $Record['pages'] : 'N/A'; ?></li>
                        </ul>
                    </div>
                    <div class="col-md-12 mt-20" >	  
                        <ul class="nav nav-tabs" style="height: 50px; ">
                            <li class="active" style="margin: 0px 2px;"> <a data-toggle="tab" href="#pill1" style="background-color:#f1f1f1;height: 50px; padding: 10px;">Report Details </a></li>
                            <?php if ($Record['toc'] != NULL): ?>
                                <li style="margin: 0px 2px;"><a data-toggle="tab" href="#pill2"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Table of Content</a></li>
                            <?php endif ?>
                            <?php if ($Record['list_tbl_fig'] != NULL): ?>
                                <li style="margin: 0px 2px;"><a data-toggle="tab" href="#pill3"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Tables & Figures</a></li>
                            <?php endif ?>
                            <li style="margin: 0px 2px;"><a  href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"style="background-color:#246A9F; height: 50px; padding: 10px;color: white;" class="">Get Free Sample <i class="fa fa-download" aria-hidden="true" style="margin-left:5px; "></i></a></li>
                            <li style="margin: 0px 2px;"><a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>"style="background-color:#f1f1f1;height: 50px; padding: 10px;">Check Discount</a></li>
                        </ul>                   
                    </div>
                </div>
                <br>
                <div class="row" style="border: 1px solid #928F8E;  border-radius: 10px;">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="tab-content tab-space mt-0">
                            <div class="tab-pane active mt-0" id="pill1" >
                                <div class="scrollbar23 mt-0" id="style-41">
                                    <div class="force-overflo2 mt-0">
                                        <div class="tab-panel mt-0" >
                                            <!-- <div class="covid-19">
                                                <div class="col-md-4 col-sm-4 ">
                                                    <span style="display: flex;">
                                                        <img src="<?= base_url() ?>web_assets/img/covid-19-icon.png" alt="Covid-19" class=" img-responsive covid-img">
                                                    
                                                        <h2 class="m-auto covid-heading"><b>COVID-19</b></h2>
                                                    </span>
                                                </div>
                                                <div class="col-md-8 col-sm-8"style="display: flex;">
                                                     <h6 class="m-auto pt-5"><b>Understand the influence of COVID-19 on the <?=$Record['meta_keyword']  ?> with our analysts monitoring the situation across the globe.</b></h6>
                                                    <div class="float-right " style="padding-top: 20px;">
                                                        <a href="<?= base_url() ?>covid-19-impact-analysis/<?= $Record['id'] ?>" class="btn btn-info covid-btn">Request Now</a>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="covid-19">
                                                <div class="col-md-4 col-sm-4 col-xs-4 matop-30">
                                                    <span class="flax">
                                                        <img src="<?= base_url() ?>web_assets/img/covid-19-icon.png" alt="Covid-19" class=" img-responsive covid-img">

                                                        <h2 class="covid-heading"><b>COVID-19</b></h2>
                                                    </span>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-8 matop-30">
                                                    <span class="key-font"><b>Understand the influence of COVID-19 on the  <?=$Record['meta_keyword']  ?> with our analysts monitoring the situation across the globe.

                                                    </b>
                                                    </span>
                                                    <div class="float-right flax matop1">
                                                        <a href="<?= base_url() ?>covid-19-impact-analysis/<?= $Record['id'] ?>" class="btn covid-btn">Request Now</a>
                                                    </div>  
                                                </div>

                                            </div>
                                            <pre style="background-color: transparent; border: none; font-size: 14px; font-family: 'Open Sans', sans-serif;;white-space: pre-wrap;"><?= $Record['report_desc'] ?>	</pre>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="pill2" >
                                <div class="scrollbar23" id="style-41">
                                    <div class="force-overflo2">
                                        <div class="tab-panel" >
                                            <pre style="background-color: transparent; border: none; font-size: 14px;font-family: 'Open Sans', sans-serif;; white-space: pre-wrap;"><?= $Record['toc'] ?></pre>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="pill3" >
                                <div class="scrollbar23" id="style-41">
                                    <div class="force-overflo2">
                                        <p class="tab-panel" >
                                            <pre style="background-color: transparent; border: none; font-size: 14px;font-family: 'Open Sans', sans-serif;;white-space: pre-wrap;">	<?= $Record['list_tbl_fig'] ?></pre>
                                        </p>

                                    </div>
                                </div>
                            </div><!--tabs ends here-->
                        </div>
                    </div>
                </div>	
                <div class="pt-10  text-center">
                    <a href="<?= base_url() ?>request_sample/<?= $Record['id'] ?>"class="btn btn-info" style="border-radius: 10px">
                        <strong>Request a free sample</strong>
                    </a>
                    <a href="<?= base_url() ?>check_discount/<?= $Record['id'] ?>" class="btn btn-info" style="border-radius: 10px">
                        <strong>Check Discount</strong>
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($Related): ?>
                            <h3><strong>Related Reports</strong></h3>
                            <?php foreach ($Related as $Rel): ?>
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-1 col-xs-2 pad05">
                                            <img src="<?= base_url() ?>web_assets/images/Market-Research-Report.png" class="img-responsive pt-10" alt="<?= str_replace(' ', '-', $Rel['meta_keyword']) ?>">
                                        </div>
                                        <div class="col-sm-9 col-xs-10">
                                            <p class="mb-5">
                                                <a style="color: #3C4858 !important" href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>"><strong><?= $Rel['title'] ?></strong></a>
                                            </p>
                                            <p class="paradesc">
                                                <?= substr($Rel['report_desc'], 0, 200) . ' ...' ?><a href="<?= base_url() . 'report/' . $Rel['id'] . '/' . str_replace(' ', '-', $Rel['meta_keyword']) ?>" class="label label-info">read more</a>
                                            </p>
                                        </div>
                                        <div class="col-sm-2 hidden-xs yrs">
                                            <h4><?= date('F Y', strtotime($Rel['published_date'])) ?></h4>
                                        </div>
                                    </div>
                                </blockquote>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>

                </div>
            </div>
            <?php $this->load->view('web/reportsidebar') ?>             
        </div>
    </div>
    <?php $this->load->view('templates/web_footer') ?>
    <script type="text/javascript">
        $(document).ready(function ()
        {
            var res = <?= $c ?>;
            var v = 0;
            var checked_status = 0;
            $("#security").focusout(function () {
                v = this.value;
            });
            $("#terms").click(function () {
                checked_status = this.checked;

                if (checked_status == true && v == res)
                {
                    $("#submit").removeAttr("disabled");
                } else
                {
                    $("#submit").attr("disabled", "disabled");
                }
            });

            $("#security").keyup(function ()
            {
                v = this.value;

                if (v == res && checked_status == true)
                {
                    $("#submit").removeAttr("disabled");
                } else
                {
                    $("#submit").attr("disabled", "disabled");
                }
            });
        });
    </script>
    <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) {
                myIndex = 1
            }
            x[myIndex - 1].style.display = "block";
            setTimeout(carousel, 10000);
        }
    </script>