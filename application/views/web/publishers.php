<?php $this->load->view('templates/web_header') ?>
<div class="container">
	<div class="row mt-5 min-mr">
		<div class="col-md-12 col-sm-12 col-xs-12 mb-20 mb-xs-10">
	    	<h1 class="servicestitle"><?=$page ?></h1>
	    </div>
	    <div class="row mb-xs-10">
	    	<?php $i=1; ?>
			<?php foreach ($Records as $Record): ?>
				<?php if ($Record['status']): ?>
				<div class="col-md-3 col-sm-3 col-xs-6 mb-50 mb-xs-10">
					<?php if ($i%4==0): ?>
						<img src="<?=base_url() ?>web_assets/images/brdrrtbt2.png" class="img-responsive hidden-xs" alt="border">
					<?php else: ?>
						<img src="<?=base_url() ?>web_assets/images/brdrrtbt.png" class="img-responsive hidden-xs" alt="border">
					<?php endif ?>
					<div class="brdrrtbt pubmedia">
						<a href="<?=base_url()?>publisher/<?=$Record['id']?>">
							<img src="<?=base_url()?>uploads/publisher/<?=$Record['img'] ?>" class="img-responsive center-block" alt="<?=$Record['name'] ?>">
							<p class="text-center pt-20"><?=$Record['name'] ?></p>
						</a>
					</div>
				</div>
	    		<?php $i++; ?>
	    		<?php endif ?>
			<?php endforeach ?>
		</div>
	</div>
</div>
<?php $this->load->view('templates/web_footer') ?>