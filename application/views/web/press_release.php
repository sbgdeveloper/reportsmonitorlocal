<?php $this->load->view('templates/web_header'); ?>

<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "NewsArticle",
    "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?= base_url() . 'press_release/' . $Record['id'] . '/' . str_replace(' ', '-', $Record['meta_keyword']) ?>"
    },
    "headline": "<?= $Record['title']; ?>",
    "image": {
    "@type": "ImageObject",
    "url": "https://www.reportsmonitor.com/web_assets/images/logo.png",
    "height": 647,
    "width": 365  },
    "datePublished": "<?= substr($Record['created_at'], 0, 10) ?>",
    "dateModified": "<?= substr($Record['created_at'], 0, 10) ?>",
    "author": {
    "@type": "Person",
    "name": "Jay Matthews"
    },
    "publisher": {
    "@type": "Organization",
    "name": "Reports Monitor",
    "logo": {
    "@type": "ImageObject",
    "url": "https://www.reportsmonitor.com/web_assets/images/logo.png"
    }
    },
    "description": " <?php echo( substr(strip_tags($Record['desc']), 0, 150) ) ?>"
    }
</script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12 mb-20 mb-xs-10">

            <div class="col-md-12 col-sm-12 col-xs-12 mb-20 mb-xs-10">
                <br><a href="<?php echo base_url(); ?>">Home</a> >> <a href="<?php echo base_url(); ?>pressRelease">Press Release</a> >> <?php print_r($Record['title']); ?>                                  	
            </div> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                    <br>
                    <img src="<?= base_url() ?>web_assets/images/Press Release.jpg" style="width: 80px; height: 100px;" alt="<?php print_r($Record['title']); ?>">

                </div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
                    <h1 style="font-size: 24px;"><b><?= $Record['title'] ?></b></h1>
                    <h4><?= date('M d Y', strtotime($Record['created_at'])) ?></h4>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mp-0">
                <div style="border: 1px solid #928F8E;  border-radius: 10px; padding: 20px; ">
                    <?php print_r($Record['desc']); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 mb-20 mb-xs-10">
            <h3><b>More Press Release</b></h3>
            <?php if ($press): ?>
                <?php foreach ($press as $Rec): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mp-0">

                        <a style="color: #3C4858 !important" href="<?= base_url() . 'press_release/' . $Rec['id'] . '/' . str_replace(' ', '-', $Rec['meta_keyword']) ?>"><b><?= $Rec['title'] ?></b></a>
                        <hr>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>
<?php $this->load->view('templates/web_footer') ?>