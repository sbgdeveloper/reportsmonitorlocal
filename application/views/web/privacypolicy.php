<?php $this->load->view('templates/web_header') ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 howpara">
			<h1 class="termtitle">PRIVACY POLICY</h1>
			
			<p><strong>We take your Privacy very seriously.</strong></p>
			<p>Reports Monitor is a trademarked brand of Straits Business Group and is identified as a Market Research wing of the company.</p>
			<p>This Privacy Policy sets out how Straits Business Groupcollects and uses information from users ("User" or "you") of our website identified by the URL www.reportsmonitor.com and/or any website which is hosted on this domain (together the  "Website") operated by Reports Monitor ( "Reports Monitor",  "our",  "we","Company" or  "us"). This Privacy Policy applies to the Website and all products and services offered by Straits Business Group.</p>
			<p>If you have any questions or comments about the Privacy Policy, please contact us at info@straitsbusinessgroup.com. This Policy is incorporated into and is subject to the Company's Terms and Conditions, which can be accessed at <a href="https://www.reportsmonitor.com/terms-conditions">https://www.reportsmonitor.com/terms-conditions</a>. <br />Your use of the Website and/or Services and any personal information you provide on the Website remains subject to the terms of the Policy and Company's Terms of Use.</p>
			<p>In terms of the Information Technology (Reasonable Security Practices and Procedures and Sensitive Personal Data or Information) Rules, 2011, any grievance or complaint may be intimated to grievance@straitsbusinessgroup.com. The complaint shall be redressed within a period of 1 (one) month.</p>
			<p>The reports or data provided by Reports Monitor are for internal use by the client and cannot be used for general publication or distribution to third parties. Straits Business Group is not responsible for the incorrect information, numbers or analysis received from report producers.</p>
			<p>Any further reproduction, photocopying, transmission in any form, recording or anything else should not be done without permission from the publisher.</p>
			<p><u><strong>What Information Do We Collect?</strong></u></p>
			<p>We may collect the following information:</p>
			<p>(a) Professional information about you including first name, last name, job title, company name, email address and telephone number provided directly from you to us or from publicly available information such as open public sources, third parties or online publications.<br />This information is processed on the grounds of legitimate interest for the purpose of:</p>
			<ul>
				<ul>
					<li>Identifying if you are key-decision maker in any of the sectors in which we operate.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Ensuring that the information we hold about you is relevant, accurate and up to date.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Ensuring that we have the information we reasonably require to provide or offer you with a relevant product or service;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Helping us to provide the best possible service in the most efficient manner; and</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To enhance marketing efficiencies for our customers in the sectors in which we operate.</li>
				</ul>
			</ul>
			<p>(b) <strong>Non-personal information </strong>about Users whenever they interact with our Website. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Website, such as the operating<br />system and the Internet Service Provider's utilised and other similar information.</p>
			<p>(c) <strong>Cookies</strong>- The Company may use standard  "cookie" browser application feature to collect and understand website usability trends. Cookies are text files that we may place in your computer browser to store your preferences.  Cookies are used to record various aspects of your visit and assist us in providing you with uninterrupted service. One of the primary purposes of cookies is to store your preferences and other information on your computer in order to save you time by eliminating the need to repeatedly enter the same information and to display your personalized content. However, no personal information identifying the user is collected nor any data capture mechanisms is employed. The user may change browser settings to accept or reject cookies on personal preference. You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify the browser setting to decline cookies if you so prefer. If you choose to decline cookies, you may not be able to sign in or use other interactive features of the Website that may depend on cookies. If you choose to accept cookies, you also have the ability to later delete cookies that you have accepted. If you choose to delete cookies, any settings and preferences controlled by those cookies, including advertising preferences, will be deleted and may need to be recreated. We process and keep all data for our own use and, if you wish to opt-out from tracking by the Company you can request so by sending an email to grievance@straitsbusinessgroup.com</p>
			<p>(d) <strong>Referrals</strong>- If you select our feature to inform others about our site or about particular products on our site, we ask you for your name and email address and your colleague's name and email address. The Website will automatically send the colleague a one-time email inviting them to visit the site. The Website does store this information and is used for the sole purpose of sending emails and tracking the success of our referral program.</p>
			<p>(e) <strong>Third-party usage tracking analytics</strong>: In addition to our own usage analytics, we use several third-party services to process personal information for the purposes listed above. While some of the services listed beneath may not be enabled at any specific time, services we use includes tools like Google analytics and similar tools used to track user movement and retention on the website. If you wish to receive more information on the type of tools used, you may email us on info@straitsbusinessgroup.com</p>
			<p>(f) <strong>Information Third Parties Provide About You: </strong>We may, from time to time, supplement the information we collect about you through our Website with outside records from third parties obtained rightfully in order to enhance our ability to serve you, to tailor our content to you and to offer you opportunities to use such of our Services that we believe may be of interest to you. We may combine the information we receive from such third-party sources with information we collect through the Website or through independent research conducted by the Company, with your consent. In these cases, we will apply this Privacy Policy to any Personal Information received, unless otherwise provided. We may process such information received from third parties for the legitimate commercial purposes or to enter into contractual obligations with you or to fulfil certain contractual obligations or where you have requested third parties to provide information about yourself to us.</p>
			<p>(g) <strong>Information Collected Automatically</strong>: In addition to any Personal Information or other information that you choose to submit to us, we may use a variety of technologies that automatically (or passively) collect certain information whenever you access the Website ( "Usage Information"). This Usage Information may include the browser that you are using, the URL that referred you to our Website, among other information. Usage Information may be non-identifying or may be associated with you. Whenever we associate Usage Information or a Device Identifier with your Personal Information, we will treat it as Personal Information and the conditions relating to Personal Information under this Privacy Policy will be followed. Traffic data, while anonymous, is gathered and analysed for business needs.</p>
			<p>(h) <strong>E-mail Communications:</strong> To help us make e-mails more useful and interesting, we may receive a confirmation when you open e-mail from Reports Monitor if your computer supports such capabilities.</p>
			<p>(i) <strong>Call Recordings: </strong>Whenever you reach out to us through our phone lines, the call may be recorded for internal quality and training purposes, also to understand and study your requirements better.</p>
			<p><u><strong>How we use collected information</strong></u></p>
			<p>Information about our customers is an important part of our business and we do not share or sell it to third parties. Straits Business Group shares customer information only as described below:<br /></p>
			<p><strong>1. Third Party Service Providers:</strong> We employ other companies and individuals to perform functions on our behalf. Those can include providing customer service, fulfilling orders, processing credit card payments and analysing data. They have access to personal information needed to perform their functions, and may not use it for other purposes. Further, they must process the personal information in accordance with this Privacy Policy and as permitted by applicable data protection laws.</p>
			<p>Some of the personal data may also be shared with designated Service/solution providers who act as sub-processors for designated purposes, such as sending emails, Analytics, monitoring and troubleshooting. Some of the personal and professional data may be shared with Publishers of the actual report in order to facilitate a business transaction.</p>
			<p><strong>2. Business Transfers:</strong> As we continue to develop our business, we might buy or sell subsidiaries or business units. In such transactions, customer information generally is one of the transferred business assets but remains subject to the promises made in any pre-existing Privacy Policy unless the customer consents otherwise.</p>
			<p><strong>3.</strong> <strong>Data Transfer:</strong> We do not transfer personal data to a third party for the third party's own use, but only for our legitimate business purposes as a part of our Services and deliverables.</p>
			<p>In accordance with our legal obligations, we may also transfer Data, subject to a lawful requests and requirements, to public authorities for law enforcement or national security purposes, where required and applicable. We may also disclose information (including any personal data), where otherwise required by law. We may also share non-Personal Information, such as aggregated user statistics and log data, with our business partners for industry analysis, demographic profiling, to deliver targeted advertising about other products or services, or for other business purposes. This information is solely used to analyze company website and understand usage statistics, as mentioned above, is anonymous. The company may share this data with its business partners on anonymous basis.  We do not sell, share, rent or trade the information we have collected about you, including Personal Information, other than as disclosed within this Privacy Policy or at the time you provide your information. We do not share your Personal Information with third parties for those third parties' direct marketing purposes unless you consent to such sharing at the time you provide your Personal Information.</p>
			<p>4. Straits Business Group uses User personal information we have collected for the following purposes:</p>
			<ul>
				<ul>
					<li>To personalise User experience: we may use information individually or aggregated to understand how our Users use the services and resources provided on our Website.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To improve our Website: we continually strive to improve our Website offerings based on the information and feedback we receive from you.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To improve customer service: your information helps us to more effectively respond to your customer service requests and support needs.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To process transactions: we may use the information Users provide when placing an order to service that order, as well as provide relevant information about our other products. We do not automatically share this information with third parties except to the extent necessary to provide the service or when explicitly given permission by the User. The User can at any time change its preferences by contacting us via our Website.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To administer Website features: such as content, promotions and surveys and to send Users information they agreed to receive about</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To contact you via email, telephone, facsimile or mail, or, where requested, by text message, to deliver certain services or information you have requested topics we think will be of interest to them.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To send periodic emails: the email address Users provide will be used to send them information and updates pertaining to their orders as well as (a) respond to their inquiries and requests or (b) provide relevant information about our other products.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Remember the basic information provided by you for effective access;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To confirm your identity in order to determine your eligibility to use the Website and avail the Services;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To notify you about any changes to the Website;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To enable the Company to comply with its legal and regulatory obligations;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>For the purpose of sending administrative notices, Service related alerts and other similar communication with a view to optimizing the efficiency of the Website;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Doing market research, troubleshooting, protection against error, project planning, fraud and other criminal activity; and</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To reinforce the Company's Terms of Use.</li>
				</ul>
			</ul>
			<p>5. <strong>Consulting</strong>: The Company may partner with another party to provide specific services if required. When you sign-up for the Services, the Company will share names, or other personal information that is necessary for the third party to provide these Services. The Company's contractual arrangements with such third parties, restrict these parties from using personally identifiable information except for the explicit purpose of assisting in the provision of these Services. The Company also protects your personal information off-line other than as specifically mentioned in this Privacy Policy. Access to your personal information is limited to employees, agents or partners and third parties, who the Company reasonably believes will need that information to enable the Company to provide Services to you. However, the Company is not responsible for the confidentiality, security or distribution of your own personal information by our partners and third parties outside the scope of our agreement with such partners and third parties.</p>
			<p>6. We may disclose and use personal or non-personal information of a user amongst all our sister concern companies, domains, subsidiaries and brands under the name of Straits Business Group.</p>
			<p><strong>Where will the data be stored and transferred to?</strong></p>
			<p>1. <strong>Storage</strong><br />The Data collected will be stored in the secure servers and systems owned or managed by Straits Business Group.</p>
			<p>Your information will be retained with the Company as long as it is needed by the Company to provide Services to you. If you wish to cancel your account or request that the Company no longer uses your information to provide Services, you may contact the Company at grievance@straitsbusinessgroup.com. The Company will promptly delete the information as requested. The Company will retain and use your information as necessary to comply with its legal obligations, resolve disputes, and enforce its agreements or for other business purposes. When the Company has no ongoing legitimate business need to process your information, we will either delete or anonymize it.</p>
			<p>2. <strong>Data Transfer</strong><br />Your information collected through the Website may be stored and processed in India or any other country in which the Company or its subsidiaries, affiliates or service providers maintain facilities. The Company may transfer information that we collect about you, including Personal Information, to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. These countries may have data protection laws that are different to the laws of your country and, in some cases, may not be as protective. We have taken appropriate safeguards to require that your information will remain protected in accordance with this Privacy Policy.</p>
			<p>3. <strong>Security</strong><br />This website has a number of security measures to ensure there is no loss, misuse or illegal alteration of information collected from its users. Personal Information and Usage Information we collect is securely stored within our databases, and we use standard, industry-wide, commercially reasonable security practices such as encryption, firewalls and SSL (Secure Socket Layers) for protecting your information. However, as effective as encryption technology is, no security system is impenetrable. We cannot guarantee the security of our databases, nor can we guarantee that information you supply won't be intercepted while being transmitted to us over the Internet or wireless communication and any information you transmit to the Company you do at your own risk. If, despite our best efforts, an unauthorised breach of our security measures occurs, resulting in the unauthorised access, duplication, removal or alteration of any Personal Information, Third Party Information, or Usage Information, the Company, its directors, officers, employees, promoters, or affiliates will not be held responsible for any loss resulting from such breach.<br />
				<strong>How long the data be retained in our Systems?</strong>
			</p>
			<ul>
				<ul>
					<li>We apply a general rule of keeping personal data only for as long as required to fulfil the purposes for which it was collected, and in accordance to the applicable legal and regulatory requirements.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>Your information with us will be retained till there is a relevant business purpose and use of the same, unless:</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>The information is removed under specific request from your end and subsequent concurrence on the same from our end.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>There is a genuine and valid requirement from our end for doing so triggered by business or regulatory/legal requirement.</li>
				</ul>
			</ul>
			<p><strong>What rights you have on your Personal Information residing with us?</strong></p>
			<p>Subject to applicable law(s) and regulations, you may have some or all of the following rights available to you in respect of your personal data that is residing with us:</p>
			<ul>
				<ul>
					<li>To obtain a copy of your personal data together with information about how and on what basis that personal data is processed;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To rectify inaccurate personal data (including the right to have incomplete personal data completed);</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To request for erasing your personal data residing with us, subject to specific context and terms and conditions;</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To request restriction on processing of your personal data under certain circumstances.</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To port your data in machine-readable format to a third party (or to you);</li>
				</ul>
			</ul>
			<ul>
				<ul>
					<li>To withdraw your consent to our processing of your personal data (where that processing is based on your consent)</li>
				</ul>
			</ul>
			<p>In relation to any aspects on Privacy, please contact us at info@straitsbusinessgroup.com</p>
			<p>Please note that we may request proof of identity, and we reserve the right to charge a fee where permitted by law, especially if your request is manifestly unfounded or excessive. We will endeavour to respond to your request within all applicable timeframes.</p>
			<p><strong>Advertising</strong></p>
			<p>Ads appearing on our Website may be delivered to Users by advertising partners, who may set cookies. These cookies allow the ad server to recognise your computer each time they send you an online advertisement to compile non-personal identification information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This privacy policy does not cover the use of cookies by any advertisers.</p>
			<p><strong>Changes/Updates to Privacy Policy</strong></p>
			<p>In order to keep up with changing legislation and best practice, we may revise this privacy policy at any time. When we do, we will post a notification on the main page of our Website. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
			<p><strong>Opt Out</strong></p>
			<p>If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Website.</p>
			<p><strong>Your acceptance of these terms</strong></p>
			<p>By using this Website, you signify your acceptance of the policies mentioned above and that of our Terms and Conditions. If you do not agree to this policy, please do not use our Website. Your continued use of the Website following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
			<p>By consenting to the terms under this Privacy Policy, you are expressly granting the Company the right to collect, share, transfer, store, retain, disseminate or use the Personal Information/Usage Information collected by the Company from your usage of the Website in accordance with the terms of the Privacy Policy.</p>
			<p><strong>Contact us</strong></p>
			<p>If you have any questions about this Privacy Policy, the practices of this Website, or your dealings with this Website, please contact us at info@straitsbusinessgroup.com or alternatively through our phone numbers.</p>

		</div>
	</div>
</div>
<?php $this->load->view('templates/web_footer') ?>