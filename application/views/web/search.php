<?php $this->load->view('templates/web_header') ?>
	<div class="container">
		<?php if ($Records): ?>
		<div class="row">
			<h3><strong>Result for :</strong> <?=$searchFor ?></h3>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h3><strong>List of Reports</strong></h3>
					<?php foreach ($Records as $Record): ?>
						<blockquote>
							<div class="row">
								<div class="col-sm-1 col-xs-2 pad05">
									<img src="<?=base_url() ?>web_assets/images/Market-Research-Report.png" class="img-responsive pt-10">
									<!-- <img src="<?=base_url() ?>uploads/category/<?=$cat['img'] ?>" class="img-responsive pt-10"> -->
								</div>
								<div class="col-sm-9 col-xs-10">
									 <a style="color: #3C4858 !important" href="<?=base_url()?>report/<?=$Record['id'] ?>/<?=str_replace(' ', '-', $Record['meta_keyword'])?>"><strong><?=$Record['title'] ?></strong></a>
									 <p class="paradesc">
										<?=substr($Record['report_desc'], 0, 300).' ...' ?><a href="<?=base_url()?>report/<?=$Record['id'] ?>/<?=str_replace(' ', '-', $Record['meta_keyword'])?>" class="label label-info">read more</a>
									 </p>   
								</div>
								<div class="col-sm-2 hidden-xs yrs">
									<?php if ($Record['published_date']=='1970-01-01' ): ?>
							 			<h4>August 2018</h4>
										<span><?=$this->data_model->get(array('id'=> $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></span>
									<?php else: ?>
										<h4><?=date('F Y', strtotime($Record['published_date'])) ?></h4>
									<span><?=$this->data_model->get(array('id'=> $Record['cat_id']), NULL, array('name'), NULL, 'category')[0]['name'] ?></span>
									<?php endif ?>

									
								</div>
							</div>
						</blockquote>
					<?php endforeach ?>
				<?php else: ?>
					<div class="row mt-30">
			
		</div><!--row ends here-->
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h2 class="fortitle">Oops! That Report can’t be found</h2>
				<p>
					Let us know what you are looking for. Please fill the form below and we will get back to you shortly.
				</p>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 bgclr">
				<form method="POST">
					<div class="col-md-1 col-sm-1 "></div>
				<div class="col-md-5 col-sm-5 col-xs-12 mt-10">
					<div class="form-group is-empty">
						<input type="text" value="" placeholder="&nbsp;&nbsp;Name" name="name" class="inp_dsgn" required>
					</div>
					<div class="form-group is-empty">
						<input type="email" value="" placeholder="&nbsp;&nbsp;Email" name="email" class="inp_dsgn" required>
					</div>
					<div class="form-group is-empty">
						<input type="text" value="" placeholder="&nbsp;&nbsp;Contact Number" name="phone" class="inp_dsgn" required>
					</div>
					<div class="form-group is-empty">
						<input type="text" value="" placeholder="&nbsp;&nbsp;Title / Designation" name="job-title" class="inp_dsgn">
					</div>

					<div class="form-group is-empty">
						<input type="text" value="" placeholder="&nbsp;&nbsp;Company" name="company" class="inp_dsgn">
					</div>
					
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12 mt-10">
					<div class="form-group is-empty">
						<input type="text" value="" placeholder="&nbsp;&nbsp;Subject/Report Tittle" name="sub" class="inp_dsgn" required>
					</div>
					<div class="form-group is-empty">
						<textarea placeholder="&nbsp;&nbsp; Your Requirement" class="inp_dsgn" name="msg" rows="4"></textarea>
					</div>
					<div class="form-group is-empty mt-10">
						
							<div class="g-recaptcha" data-sitekey="6LcvGGYUAAAAAHi9R1s48rTNPSK9CVMOBXHsxTo3"></div>
						
					</div>
				</div>
				
				<div class="col-md-12 col-xs-12 mt-10 text-center">
					<input type="submit" id="submit" class="btn btn-info" value="Submit">
				</div>
				</form>


				<?php endif ?>
				<!-- <?php $this->load->view('templates/pagination') ?> -->
			</div>
		</div>
		
	</div>
<?php $this->load->view('templates/web_footer') ?>
<!-- <script type="text/javascript">
	$(document).ready(function() 
	{
		var res = <?=$c?>;
		var v = 0;
		var checked_status = 0;
		$("#security").keyup(function() {
			v = this.value;
		
			if (v == res)
			{
				$("#submit").removeAttr("disabled");
			}
			else
			{
				$("#submit").attr("disabled", "disabled");
			}
		});
	});
</script> -->