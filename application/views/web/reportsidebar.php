<div class="col-md-3 col-sm-3 col-xs-12 mt-20">
    <div class="row">
        <div class="col-sm-12 surdds mb-20">
            <div class="row text-center">
                <img src="<?php echo base_url() . 'web_assets/' ?>images/offer.gif" class="img-responsive" alt="Cards For Payment" style="padding: 1px 15px 0px 15px;">
            </div>
            <div class="headttl">
                <form action="<?= base_url() . 'buyNow/' . $Record['id'] ?>" method="get">

                   <div class="radio ln_hgt">
                    <label>
                        <input type="radio" id="price" name="price" value="single_price" checked><span class="radio-material"></span></span>
                        Single User - <del style="color: red; font-size:12px;">$<?= $Record['single_price'] ?></del> <strong>$<?= round($Record['single_price']*0.85 )?></strong>
                    </label>
                    <?php if ($Record['multi_price'] != 0): ?>
                        <label>
                            <input type="radio" id="price" name="price" value="multi_price"><span class="radio-material"></span></span>
                            Multi User - <del style="color: red; font-size:12px;">$<?= $Record['multi_price'] ?></del> <strong>$<?= round($Record['multi_price']*0.85) ?></strong> 
                        </label>
                    <?php endif ?>
                    <?php if ($Record['ent_price'] != 0): ?>
                        <label>
                            <input type="radio" id="price" name="price" value="ent_price"><span class="radio-material"></span></span>
                            Enterprise - <del style="color: red; font-size:12px;">$<?= $Record['ent_price'] ?></del> <strong>$<?= round($Record['ent_price']*0.85 );?></strong>
                        </label>
                    <?php endif ?>
                </div>

                <div class="tooltip1"> 
                    <i class="fa fa-question-circle" style="float:right;font-size: 22px;"></i>
                    <span class="tooltiptext1"><p><b style="color: #06d4d2">Single User - </b>This is a single user license, allowing one specific user access to the product.</p>
                        <p ><b style="color: #06d4d2">Multi User - </b>This is a 1-5 user licence, allowing up to five users have access to the product.</p>
                        <p><b style="color: #06d4d2">Enterprise User - </b>This is an enterprise license, allowing all employees within your organisation access to the product. The report will be emailed to you.</p></span>
                    </div>

                    <div class="text-center" style="margin: 0px; padding: 0px;">
                        <button type="submit" style="border-radius: 10px; line-height: 0px;font-weight: bold;" class="btn btn-info" value="Buy Now">Buy Now <i class="fa fa-shopping-cart" style="margin-left:10px ; color: #FFF;"></i></button>
                        <!-- <input type="submit"style="border-radius: 10px; line-height: 0px; font-weight: bold; height: 26px;" class="btn btn-info" value="Buy Now"> -->
                        <img src="<?php echo base_url() . 'web_assets/' ?>img/cards.png" style="float:left; width: 250;" class="img-responsive center-block" alt="Cards For Payment ">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 surdds mb-20">
            <img src="<?php echo base_url() . 'web_assets/' ?>img/Badge.png" style="height: 80px; float: left; margin-left: 20px;" class="img-responsive center-block" alt="Trusted Badge">
            <img src="<?php echo base_url() . 'web_assets/' ?>img/lets.png" style="float: left; height: 80px;" class="img-responsive center-block" alt="SSL Certified">
            <!--<img src="<?php echo base_url() . 'web_assets/' ?>img/Secured & Verified.png" style="float: left; margin-left: 20px; height: 80px;" class="img-responsive center-block"> -->
            <img src="<?php echo base_url() . 'web_assets/' ?>img/Premium Quality.png" style="height: 80px;" class="img-responsive center-block" alt="Premium Quality">
        </div>
        <div class="col-sm-12 surdds mb-10">
            <h4 class="text-center">Why Choose Us</h4>
            <div class="headttl">
                <ul style="padding:0px; margin: 15px; line-height: 20px;">
                    <li><b>Lowest Price Guarantee</b></li>
                    We offer the lowest prices for the listed reports
                    <li><b>Data Security</b></li>
                    Your data is safe and secure
                    <li><b>Vast Report Database</b></li>
                    We have more than 2 Million reports in our database
                    <li><b>Client Focused</b></li>
                    Personalized updates and 24*7 support
                    <li><b>Trusted Source and Quality</b></li>
                    We only work with reputable partners providing high quality research and support
                    <li><b>Market Segmentation</b></li>
                    We provide alternative views of the market to help you identify where the real opportunities lie
                    <li><b>Bulk Discounts</b></li>
                    We offer great discounts on purchase of multiple reports
                </ul>
            </div>
        </div>
        <div class="col-sm-12 surdds mb-10">
            <h4 class="text-center">How To Reach Us</h4>
            <div class="headttl">
                <p>
                    Reach out to our most senior Sales Professional- <b>Jay Matthews</b> <br>
                    <i class="fa fa-phone-square" style="font-size:24px"></i> &nbsp;&nbsp;&nbsp;+1 513 549 5911 (US) <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+44 203 318 2846 (UK)<br>
                    <i class="fa fa-envelope-o" style="font-size:24px"></i><b>&nbsp;&nbsp;&nbsp;<a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a></b> </p>   
                </div>
            </div>
            <div class="col-sm-12 surdds mb-10">
                <h4 class="text-center">Our Clients</h4>
                <div class="headttl">
                    <marquee>
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Samsung.png" style="width: 250px; height: 130px;" alt="Samsung">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/3M.png" style="width: 250px; height: 130px;" alt="3M">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/BCG.png" style="width: 250px; height: 130px;" alt="BCG">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/LEK Consulting.png" style="width: 250px; height: 130px;" alt="LEK Consulting">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/McKinsey.png" style="width: 250px; height: 130px;" alt="McKinsey">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Thyssenkrupp.png" style="width: 250px; height: 130px;" alt="Thyssenkrupp">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/download.jpg" style="width: 250px; height: 130px;" alt="Mogensen">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/BASF.png" style="width: 250px; height: 130px;" alt="BASF">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/IQVIA.png" style="width: 250px; height: 130px;" alt="IQVIA">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Hella.png" style="width: 250px; height: 130px;" alt="Hella">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Oracle.png" style="width: 250px; height: 130px;" alt="Oracle">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Accenture.png" style="width: 250px; height: 130px;" alt="Accenture">
                        <img  src="<?php echo base_url() . 'web_assets/' ?>images/clients/client_logo/Saint Gobain.png" style="width: 250px; height: 130px;" alt="Saint Gobain">

                    </marquee>
                </div>
            </div>
            <div class="col-sm-12 surdds mb-10">
                <h4 class="text-center">Testimonials</h4>
                <div class="headttl">
                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                    <div class="w3-content w3-section" style="max-width:400px">
                        <div class="mySlides">
                            <p> Well structured, the insights they shared with us were very helpful and reliable. Their timely assistance make their services invaluable to us. I would highly recommend them and would definitely use them again in the future if needed. <br>
                                -<b>VP of a Automotive division in Germany</b></p>
                            </div>
                            <div class="mySlides">
                                <p>The report sent to us was on the point, and its information was quite extensive, well structured, and well researched. More importantly what we valued was your response time and professionalism. As a leading global consulting firm, our clients expect high quality deliverables in short periods of time, so a reliable research partner is essential. For the price that you have charged the quality of your services were exceptional. We look forward to continue our relationship with your team on future engagements <br>
                                    -<b>Product Manager at US based Manufacturer</b></p>
                                </div>
                                <div class="mySlides">
                                    <p>Coherent, high-quality, thoroughly-researched reports. We received a very quick response to all our queries which eventually expedited the entire process <br>
                                        -<b>-Marketing Manager at a pharma company in Belgium</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div><!--row ends here-->
                </div>