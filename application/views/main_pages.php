<?php header('Content-type: text/xml'); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <!-- Sitemap --> 
    <url>
        <loc><?php echo base_url()?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."about-us"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
     <url>
        <loc><?php echo base_url()."services"?></loc>
        <priority>1</priority>
        <changefreq>monthly</changefreq>
    </url>
     <url>
        <loc><?php echo base_url()."categories"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <!-- Categories Url -->
    <url>
        <loc><?php echo base_url()."categories/1/Aerospace-and-Defence"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/2/Automotive-and-Transportation"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/3/Banking-and-Finance"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/4/Company-Profiles"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/5/Construction-and-Manufacturing"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/6/Consumer-Goods"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/7/Energy-and-Power"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/8/Food-and-Beverages"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/9/ICT-Media"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/10/Machinery-and-Equipments"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/11/Materials-and-Chemicals"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/12/Medical-Devices"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/13/Pharmaceuticals-and-Healthcare"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?php echo base_url()."categories/14/Semiconductor-and-Electronics"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>

     <url>
        <loc><?php echo base_url()."publishers"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
     <url>
        <loc><?php echo base_url()."pressRelease"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
     <url>
        <loc><?php echo base_url()."blogs"?></loc>
        <priority>1</priority>
        <changefreq>daily</changefreq>
    </url>
</urlset>