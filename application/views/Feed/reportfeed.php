<?php  echo '<?xml version="1.0" encoding="iso-8859-1"?>'?>
<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
<channel>
    <title>Reports Monitor</title>
    <link>http://www.reportsmonitor.com/feed</link>
    <description>Reports Monitor is a leading market research organization providing global market research and industry analysis reports. </description>

    <lastBuildDate> <?php echo date(DATE_RFC822);?> </lastBuildDate>
    <language>en-US</language>
    <sy:updatePeriod>hourly</sy:updatePeriod>
    <sy:updateFrequency>1</sy:updateFrequency>        
    <image>
        <url>https://www.reportsmonitor.com/web_assets/images/logo.png</url>
        <title>Reports Monitor</title>
        <link>http://www.reportsmonitor.com</link>
        <width>32</width>
        <height>32</height>
    </image> 
    <?php foreach($report->result() as $post): ?>
    <item>
        <title><?php echo str_replace('&', 'and', $post->title)  ;?></title>
        <link>https://www.reportsmonitor.com/report/<?php echo $post->id ;?></link>
        <guid>https://www.reportsmonitor.com/report/<?php echo $post->id ;?></guid>
        <pubDate><?php $date = new DateTime($post->created_at);
        echo $date->format(DateTime::RFC822);
        ?></pubDate>
        <dc:creator><![CDATA[sales@reportsmonitor.com]]></dc:creator>
        <category><![CDATA[Reports]]></category>
        <description><![CDATA[<?php echo str_replace('?','',substr(strip_tags($post->report_desc),0,250)) ;?>]]></description>
        <content:encoded><![CDATA[<?php echo $post->report_desc;?>]]></content:encoded>
    </item>
   <?php endforeach; ?> 

</channel>
</rss>