<?php header('Content-type: text/xml'); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <!-- Sitemap -->
    <?php if ($items): ?>
        <?php if ($title=='report'): ?>
            <?php foreach($items as $item) { ?>
            <url>
                 <?php   $item['meta_keyword'] = str_replace(' ', '-', $item['meta_keyword']);
                         $item['meta_keyword'] = preg_replace('/[^A-Za-z0-9\-]/', '', $item['meta_keyword']); ?>  

                <loc><?php echo base_url().$title."/".$item['id']."/".str_replace(" ", "-",$item['meta_keyword']);?> </loc>
                <lastmod><?php echo substr($item['created_at'],0,10);?></lastmod>
                <priority>0.9</priority>
                <changefreq>always</changefreq>
            </url>
            <?php } ?>
            <?php else: ?>
                <?php foreach($items as $item) { ?>
                    <url>
                        <loc><?php echo base_url().$title."/".$item->id."/".str_replace(" ", "-",$item->meta_keyword);?> </loc>
                        <lastmod><?php echo date('Y-m-d') ?></lastmod>
                        <priority>0.9</priority>
                        <changefreq>monthly</changefreq>
                    </url>
                <?php } ?>
        <?php endif ?>
    <?php endif ?>
</urlset>