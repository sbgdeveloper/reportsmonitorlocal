<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.4.0
    </div>
    <strong>Copyright &copy; 2018-2020 <a href="#">Reports Monitor</a>.</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
</div>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() . 'assets/' ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() . 'assets/' ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() . 'assets/' ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() . 'assets/' ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() . 'assets/' ?>dist/js/demo.js"></script>

<!-- bootstrap datepicker -->
<script src="<?php echo base_url() . 'assets/' ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url() . 'assets/' ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/' ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/' ?>bower_components/ckeditor/ckeditor.js"></script>

<script>
    $(function () {
        $('#example1').DataTable({
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [-1]
                }]
        })
        $('#example2').DataTable({
            "aaSorting": [],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [-1]
                }]
        })
        $('#datepicker').datepicker({
            autoclose: true
        })

        $('#myModal').on('hidden.bs.modal', function () {
            location.reload();
        })
        var Report_desc = document.getElementById("Report_desc");
        if (Report_desc)
        {
            // CKEDITOR.replace('Report_desc');
            CKEDITOR.addCss('.cke_editable { font-size: 12px; padding: 0; }');

            CKEDITOR.replace('Report_desc', {
                toolbar: [
                    // { name: 'document', items: [ 'Print' ] },
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
                    {name: 'editing', items: ['Scayt']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    '/',
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                    {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']},
                            // { name: 'editing', items: [ 'Scayt' ] }
                ],

                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

                // Adding drag and drop image upload.
                extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
//						filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
//						filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
//						filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//						filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

                filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=files',
                filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
                filebrowserFlashBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=flash',
                filebrowserUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=files',
                filebrowserImageUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=images',
                filebrowserFlashUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=flash',

                height: 250,

                removeDialogTabs: 'image:advanced;link:advanced'
            });
        }

        var Toc = document.getElementById("Toc");
        if (Toc)
        {
            // CKEDITOR.replace('Toc');
            CKEDITOR.addCss('.cke_editable { font-size: 12px; padding: 0; }');

            CKEDITOR.replace('Toc', {
                toolbar: [
                    // { name: 'document', items: [ 'Print' ] },
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
                    {name: 'editing', items: ['Scayt']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    '/',
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                    {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']},
                            // { name: 'editing', items: [ 'Scayt' ] }
                ],

                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

                // Adding drag and drop image upload.
                extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
//                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
//                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
//                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=files',
                filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
                filebrowserFlashBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=flash',
                filebrowserUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=files',
                filebrowserImageUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=images',
                filebrowserFlashUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=flash',

                height: 250,

                removeDialogTabs: 'image:advanced;link:advanced'
            });
        }

        var List_tbl_fig = document.getElementById("List_tbl_fig");
        if (List_tbl_fig)
        {
            // CKEDITOR.replace('List_tbl_fig');
            CKEDITOR.addCss('.cke_editable { font-size: 12px; padding: 0; }');

            CKEDITOR.replace('List_tbl_fig', {
                toolbar: [
                    // { name: 'document', items: [ 'Print' ] },
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
                    {name: 'editing', items: ['Scayt']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    '/',
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                    {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']},
                            // { name: 'editing', items: [ 'Scayt' ] }
                ],

                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

                // Adding drag and drop image upload.
                extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
//                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
//                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
//                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=files',
                filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
                filebrowserFlashBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=flash',
                filebrowserUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=files',
                filebrowserImageUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=images',
                filebrowserFlashUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=flash',

                height: 250,

                removeDialogTabs: 'image:advanced;link:advanced'
            });
        }

        var Desc = document.getElementById("Desc");
        if (Desc)
        {
            // CKEDITOR.replace('Desc');
            CKEDITOR.addCss('.cke_editable { font-size: 12px; padding: 0; }');

            CKEDITOR.replace('Desc', {
                toolbar: [
                    // { name: 'document', items: [ 'Print' ] },
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
                    {name: 'editing', items: ['Scayt']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    '/',
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                    {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']},
                            // { name: 'editing', items: [ 'Scayt' ] }
                ],

                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

                // Adding drag and drop image upload.
                extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
//                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
//                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
//                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

                filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=files',
                filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
                filebrowserFlashBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=flash',
                filebrowserUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=files',
                filebrowserImageUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=images',
                filebrowserFlashUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=flash',

                height: 250,

                removeDialogTabs: 'image:advanced;link:advanced'
            });
        }

        var Page_content = document.getElementById("Page_content");
        if (Page_content)
        {
            // CKEDITOR.replace('Page_content');
            CKEDITOR.addCss('.cke_editable { font-size: 12px; padding: 0; }');

            CKEDITOR.replace('Page_content', {
                toolbar: [
                    // { name: 'document', items: [ 'Print' ] },
                    {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                    {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']},
                    {name: 'editing', items: ['Scayt']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    '/',
                    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                    {name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']},
                            // { name: 'editing', items: [ 'Scayt' ] }
                ],

                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

                // Adding drag and drop image upload.
                extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
//                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
//                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
//                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

                filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=files',
                filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
                filebrowserFlashBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=flash',
                filebrowserUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=files',
                filebrowserImageUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=images',
                filebrowserFlashUploadUrl: '/kcfinder/upload.php?opener=ckeditor&type=flash',

                height: 250,

                removeDialogTabs: 'image:advanced;link:advanced'
            });
        }


        var date = document.getElementById("Published_date");
        if (date)
        {
            $('#Published_date').datepicker({
                autoclose: true
            })
        }

    })
</script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#Email").on("blur", function ()
        {
            var email = $(this).val();
            var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
            if (filter.test(email))
            {
                $("#email-valid").removeClass("hidden");
                $("#email-invalid").addClass("hidden");
                $("#submit").prop('disabled', false);
                em = true;
            } else
            {
                $("#email-invalid").removeClass("hidden");
                $("#email-valid").addClass("hidden");
                $("#submit").prop('disabled', true);
                em = false;
                return false;
            }
            if (mob && em)
            {
                $("#submit").prop('disabled', false);
            }
        });
    });
</script>
</body>
</html>