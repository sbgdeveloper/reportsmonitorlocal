<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="msvalidate.01" content="98B96592A1CF68A7E56E00D608691902" />
        <meta name="google-site-verification" content="3LKdoeDrtakUnRgzd31DzfKEs0M0RwP2vsx0MEMnewo" />
        <meta charset="utf-8" />
        <title><?php echo($page) ?> | Reports Monitor</title>
        <?php if (isset($Record['meta_keyword'])): ?>
            <meta name="keywords" content="<?= $Record['meta_keyword'] ?>">
        <?php endif ?>
        <?php if (isset($Record['meta_desc'])): ?>
            <meta name="description" content="<?= $Record['meta_desc'] ?>">
        <?php endif ?>
        <?php if (isset($key_desc)): ?>
            <meta name="keywords" content="market research report, market survey, market segmentation">
            <meta name="description" content="We offer a huge number of market research reports covering the front line and major issues in all industry sectors. Reports around here also comprise market segmentation and SWOT analysis of leading organizations around the world. This database has been outlined and tailor-made to suit your business prerequisites.">
        <?php endif ?>
        <?php if (isset($Record['canonical'])): ?>
            <link rel="canonical" href="<?= $Record['canonical'] ?>"/>
        <?php endif ?>
        <meta name="naver-site-verification" content="28f4e4d3635769c609a1df5e88fc9d13a1a6d172"/>
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() . 'web_assets/' ?>img/apple-icon.png">
        <link rel="icon" type="image/png" href="<?php echo base_url() . 'web_assets/' ?>img/favicon.png">

        <!-- CSS Files -->
        <link href="<?php echo base_url() . 'web_assets/' ?>css/allinone.css" rel="stylesheet"/>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-91453747-1"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5a6032fdd7591465c706dbe1/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>     
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-91453747-1');
        </script>
    </head>
    <style>
        pre p{
            line-height: 18px !important;
            margin: 0px 0px -25px 0px !important;
            padding: 0px;
        }
        .tab-space {
            padding: 0px 0 20px 0px !important;
        }
    </style>
    <body class="index-page">
        <div class="container-fluid topnav">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 mailclr">
                        <a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <ul class="list-inline continfo pull-right">
                            <li> +1 513 549 5911 (U.S.)</li>
                            <li class="hidden-xs">|</li>
                            <li>+44 203 318 2846 (U.K.)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default" color-on-scroll="" id="sectionsNav">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo base_url() . 'web_assets/' ?>images/logo.png" class="img-responsive" alt="Reports Monitor"></a>				
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url() ?>">Home</a></li>
                        <li><a href="<?= base_url() ?>about-us">About Us</a></li>
                        <li><a href="<?= base_url() ?>services">Our Services</a></li>
                        <li><a href="<?= base_url() ?>categories">Categories</a></li>
                        <li><a href="<?= base_url() ?>publishers">Publishers</a></li>
                        <li><a href="<?= base_url() ?>pressRelease">Press Release</a></li>
                        <li><a href="<?= base_url() ?>blogs">Blogs</a></li>
                        <li><a href="<?= base_url() ?>contact-us">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <?php if ($page_header == 'page-header'): ?>
            <div class="page-header header-filter clear-filter imgradius" data-parallax="false" style="background-image: url('<?php echo base_url() . 'web_assets/' ?>images/homeBanner.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-8 col-xs-12 bannertext">
                            <h2>Search for reports</h2>
                            <form class="form-inline" method="get" action="<?= base_url() ?>search">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="search" <?php if (isset($searchFor)) echo 'value="' . $searchFor . '"'; ?> class="form-control searchform">
                                    </div>
                                </div>
                                <input type="hidden" name="category" value="NULL">
                                <input type="image" src="<?php echo base_url() . 'web_assets/' ?>images/search.png" class="subbtn">
                            </form>
                        </div>
                        <div class="col-md-4 col-md-4 col-xs-12 bannertext1">
                            <h1 style="line-height: 1.2; font-size: 30px;">Monitor your market through our reports</h1>
                            <p>Fresh perspectives, new data and ground breaking insight into the markets that matter. We help you see the current realities, future possibilities for your industry, your customers and your business.</p>
                        </div>
                    </div>
                </div>
            </div>
        <?php elseif ($page_header == 'page-header1'): ?>
            <div class="page-header2 imgradius" data-parallax="false" style="background-image: url('<?php echo base_url() . 'web_assets/' ?>images/categoriesBg.jpg');">
                <div class="container">
                    <div class="row pt-xs-10">            
                        <div class="col-md-12 col-md-12 col-xs-12 cateclr pt-40 pt-xs-10">
                            <form method="get" action="<?= base_url() ?>search">
                                <div class="row">
                                    <div class="col-md-6">     
                                        <input type="text" name="search" placeholder="Search for Reports" <?php if (isset($searchFor)) echo 'value="' . $searchFor . '"'; ?> class="form-control searchform"> 
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control slectdrop" name="category">
                                            <option class="otherst" value="NULL">Select Category</option>
                                            <?php foreach ($categories as $category): ?>
                                                <option class="otherst" value="<?= $category['id'] ?>" <?php if (isset($cat_id) && $cat_id == $category['id']) echo 'selected' ?>><?= $category['name'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" class="btn btn-primary searchbtn" value="Search">
                                    </div>
                                </div>
                            </form>
                        </div>       
                    </div>
                </div><!--serach bar ends here-->
            </div>
        <?php elseif ($page_header == 'page-header4'): ?>
            <div class="page-header3 imgradius1" data-parallax="false" style="background-image: url('<?php echo base_url() . 'web_assets/' ?>images/categoriesBg.jpg');"></div> 
            <p class="" style="color: #06d5d3; font-size: 20px; text-align: center; margin-top: -40px;"><marquee behavior="alternate"><b> 30 mins free consultation for each report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Upto 50% Discounts for Highest selling reports</b></marquee></p>
<?php else: ?>
    <div class="page-header3 imgradius1" data-parallax="false" style="background-image: url('<?php echo base_url() . 'web_assets/' ?>images/categoriesBg.jpg');"></div>
                        	<?php endif ?>