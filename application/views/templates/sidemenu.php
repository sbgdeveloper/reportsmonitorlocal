<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <?php if ($this->session->userdata('logged_in') == 'Admin'): ?>
                <a href="<?php echo base_url() . 'dashboard/Admin' ?>" class="logo">
                <?php else: ?>
                    <a href="<?php echo base_url() . 'dashboard/User' ?>" class="logo">
                    <?php endif ?>
                    <span class="logo-mini"><b>RM</b></span>
                    <span class="logo-lg"><b>Reports</b> Monitor</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url() . 'assets/' ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $this->session->userdata('name') ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url() . 'assets/' ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $this->session->userdata('name') ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="<?php echo base_url() . 'logout/Admin' ?>" class="btn btn-default btn-flat">Log out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <?php // if ($this->session->userdata('logged_in') == 'sales'): ?>


                <?php // endif ?>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php if ($this->session->userdata('logged_in') == 'Admin'): ?>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Users</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/member"><i class="fa fa-circle-o"></i>List Users</a></li>
                                <li><a href="<?php echo base_url(); ?>add/member"><i class="fa fa-circle-o"></i> Add Users</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Publisher</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/publisher"><i class="fa fa-circle-o"></i> List Publisher</a></li>
                                <li><a href="<?php echo base_url(); ?>add/publisher"><i class="fa fa-circle-o"></i> Add Publisher</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Category</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/category"><i class="fa fa-circle-o"></i> List Category</a></li>
                                <li><a href="<?php echo base_url(); ?>add/category"><i class="fa fa-circle-o"></i> Add Category</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Regions</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/region"><i class="fa fa-circle-o"></i> List Regions</a></li>
                                <li><a href="<?php echo base_url(); ?>add/region"><i class="fa fa-circle-o"></i> Add Regions</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Site Content</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>pages/services"><i class="fa fa-circle-o"></i> Services</a></li>
                                <li><a href="<?php echo base_url(); ?>pages/policy"><i class="fa fa-circle-o"></i> Privercy Policy</a></li>
                                <li><a href="<?php echo base_url(); ?>pages/terms"><i class="fa fa-circle-o"></i> Terms and Conditions</a></li>
                            </ul>
                        </li>
                    </ul>
                <?php elseif (($this->session->userdata('logged_in') == 'User' ) && ($this->session->userdata('userid') != '1206' )): ?>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Reports</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>/reportlist"><i class="fa fa-circle-o"></i>List Reports</a></li>
                                <li><a href="<?php echo base_url(); ?>add/report"><i class="fa fa-circle-o"></i> Add Report</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Press Release</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/press_release"><i class="fa fa-circle-o"></i> List Press Release</a></li>
                                <li><a href="<?php echo base_url(); ?>add/press_release"><i class="fa fa-circle-o"></i> Add Press Release</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>Blog</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>list/blog"><i class="fa fa-circle-o"></i> List Blog</a></li>
                                <li><a href="<?php echo base_url(); ?>add/blog"><i class="fa fa-circle-o"></i> Add Blog</a></li>
                            </ul>
                        </li>
                        <!-- Excel Export -->
                        <li>
                            <a href="<?php echo base_url(); ?>user/createXLS"><i class="fa fa-circle-o"></i> Excel Export</a>

                        </li>
                    </ul>
                <?php elseif (($this->session->userdata('logged_in') == 'User' ) && ($this->session->userdata('userid') == '1206' )): ?>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                <span>PayLink Management</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url(); ?>/list/paylink"><i class="fa fa-circle-o"></i>List PayLink</a></li>
                                <li><a href="<?php echo base_url(); ?>add/paylink"><i class="fa fa-circle-o"></i> Add PayLink</a></li>
                            </ul>
                        </li>
                    </ul>
                <?php endif ?>
            </section>
        </aside>