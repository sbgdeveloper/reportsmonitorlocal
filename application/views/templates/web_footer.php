	<a href="javascript:" id="return-to-top1"><i class="fa fa-chevron-up"></i></a>

	<?php if ($page_header != 'page-header'): ?>
		<div class="container">
			<div class="row">
				<h2 style="font-size: 36px;" class="ourftitle mt-50">Our Features</h2> 
			<div class="featurebg">
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/globeIcon.png" class="img-responsive center-block" width="130" height="122" alt="Wide Network of Publishers">
					<p class="text-center"><strong>Wide Network of Publishers</strong></p>
					<p class="text-center">Access to over 1M+ market research reports</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/medalIcon.png" class="img-responsive center-block" width="130" height="122" alt="Trusted by the Best">
					<p class="text-center"><strong>Trusted by the Best</strong></p>
					<p class="text-center">50+ Clients in Fortune 500</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 brdrright">
					<img src="<?php echo base_url().'web_assets/' ?>images/pricingIcon.png" class="img-responsive center-block" width="130" height="122" alt="Competitive Pricing">
					<p class="text-center"><strong>Competitive Pricing</strong></p>
					<p class="text-center">Our regular promotional offers and discounts make us the most economic seller of market reports</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<img src="<?php echo base_url().'web_assets/' ?>images/cardIcon.png" class="img-responsive center-block" width="130" height="122" alt="Safe and Secure">
					<p class="text-center"><strong>Safe and Secure</strong></p>
					<p class="text-center">Your payment details are protected by us.</p>                
				</div>
			</div>
		<!--our feature ends here-->
			</div>
		</div>
		
		<?php endif ?>
		<footer class="footer footer-black footer-big">
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-md-4 mb-xs-30">
							<ul>
								<li><a href="<?=base_url()?>services">Our Services</a></li>
								<li><a href="<?=base_url()?>privacy-policy">Privacy Policy</a></li>
								<li><a href="<?=base_url()?>return-policy">Return Policy</a></li>
								<li><a href="<?=base_url()?>disclaimer">Disclaimer</a></li>
								<li><a href="<?=base_url()?>sitemap">Site Map</a></li>
								<li><a href="<?=base_url()?>terms-conditions">Terms and Conditions</a></li>
								<li><a href="<?=base_url()?>format-delivery">Format and Delivery</a></li>
								<li><a href="<?=base_url()?>how-to-order">How to Order</a></li>
								
							</ul>
						</div>

						<div class="col-md-4 mb-xs-30 center-block">					
							<div class="row mb-10">
								<div class="col-sm-2 col-xs-2">
									<img src="<?php echo base_url().'web_assets/' ?>images/mail.png" class="pt-10" height="30" width="30" alt="email">
								</div>
								<div class="col-sm-10 col-xs-10">
									<a href="mailto:sales@reportsmonitor.com">sales@reportsmonitor.com</a>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-xs-2">
									<img src="<?php echo base_url().'web_assets/' ?>images/phone.png" class="pt-20" height="50" width="30" alt="phone no.">
								</div>
								<div class="col-sm-10 col-xs-10">
									 +1 513 549 5911 (U.S.)<br>
									 +91 8087085354 (India)<br>
									 +44 203 318 2846 (U.K.)
								</div>
							</div>
						</div>					

						<div class="col-md-4 getmsg text-right mtxtleft">
							<h3>Get Message</h3>
							<p>We want to keep you informed with what<br/>is happening at Reports Monitor and tell you<br/>about our latest products and other services.</p>
								<form class="form-inline">
									  <div class="form-group">                        
										<div class="input-group">                          
										  <input type="text" value="" placeholder="" class="form-control newsletterf">                          
										</div>
									  </div>
								  <input type="image" src="<?php echo base_url().'web_assets/' ?>images/go.png" alt="" class="gobtn">
								</form>
								
								<div class="">
									<img src="<?php echo base_url().'web_assets/' ?>images/RmLogo.png" width="120" height="42" alt="Reports Monitor">
								</div>
						 </div>
					</div>
				</div>
			</div>
			
			<hr>
			
			<div class="container-fluid">
				<div class="container">
					<ul class="pull-left list-inline socialprl">
						<li><a href="https://www.linkedin.com/company/reports-monitor/"><img src="<?php echo base_url().'web_assets/' ?>images/linkdin.png" width="50" height="50" alt="linkedin"></a></li>
						<li><a href="https://www.facebook.com/ReportsMonitor/"><img src="<?php echo base_url().'web_assets/' ?>images/facebook.png" width="50" height="50" alt="facebook"></a></li>
						<li><a href="https://plus.google.com/u/0/104187516885663390925"><img src="<?php echo base_url().'web_assets/' ?>images/googlePlus.png" width="50" height="50" alt="google plus"></a></li>
						<li><a href="https://twitter.com/reportsmonitor/"><img src="<?php echo base_url().'web_assets/' ?>images/twitter.png" width="50" height="50" alt="twitter"></a></li>
					</ul>
		
					<div class="copyright pull-right">
						© <script>document.write(new Date().getFullYear())</script> Reports Monitor All Rights Reserved.
					</div>
				</div>
			</div>
		</footer>
	</body>
	<!--   Core JS Files   -->
	<!-- <script src="<?php echo base_url().'web_assets/' ?>js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url().'web_assets/' ?>js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url().'web_assets/' ?>js/material.min.js"></script>	
	<script src="<?php echo base_url().'web_assets/' ?>js/custom.js" type="text/javascript"></script>
	<script src="<?php echo base_url().'web_assets/' ?>js/owl.carousel.min.js" type="text/javascript"></script>
 -->
	
<script src="<?php echo base_url() . 'web_assets/' ?>js/custom2.js" type="text/javascript"></script>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.carousel-inner').owlCarousel({
				singleItem : true,
				slideSpeed : 500,
				pagination: true,
				navigation: true,
				autoPlay:false,
				loop:true,
				responsiveRefreshRate : 200,
				transitionStyle : "fade"
			});			
		});
		jQuery('.carousel-inner').owlCarousel({
			items: 3,
			itemsDesktop: [1199, 3],
			itemsDesktopSmall: [991, 2],
			itemsTablet: [768, 2],
			itemsMobile: [479, 1],
			lazyLoad: true,
			pagination: false,
			navigation: true
		});
		
		jQuery('.latestnews').owlCarousel({
				singleItem : true,
				slideSpeed : 500,
				pagination: true,
				navigation: true,
				autoPlay:false,
				loop:false,
				responsiveRefreshRate : 200,
				transitionStyle : "fade"
			});		
		
		jQuery('.latestnews').owlCarousel({
			items: 3,
			itemsDesktop: [1199, 3],
			itemsDesktopSmall: [991, 1],
			itemsTablet: [768, 1],
			itemsMobile: [479, 1],
			lazyLoad: false,
			pagination: false,
			navigation: false
		});

		jQuery(document).ready(function()
		{
			jQuery('.whatdo').owlCarousel(
			{
				singleItem : true,
				slideSpeed : 500,
				pagination: true,
				navigation: true,
				loop:true,
				autoPlay:true,
				responsiveRefreshRate : 200,
				transitionStyle : "fade",	
				autoplayHoverPause : true,
			});
			jQuery('.brand').owlCarousel(
			{
				singleItem : true,
				slideSpeed : 500,
				pagination: true,
				navigation: true,
				loop:true,
				autoPlay:true,
				responsiveRefreshRate : 200,
				transitionStyle : "fade",	
				autoplayHoverPause : true,
			});
			jQuery('.latestnews').owlCarousel(
			{
				singleItem : true,
				slideSpeed : 500,
				pagination: true,
				navigation: true,
				loop:true,
				autoPlay:false,
				responsiveRefreshRate : 200,
				transitionStyle : "fade",
			});

		});

		jQuery('.whatdo').owlCarousel(
		{
			items: 3,
			itemsDesktop: [1199, 3],
			itemsDesktopSmall: [991, 3],
			itemsTablet: [768, 2],
			itemsMobile: [479, 1],
			lazyLoad: true,
			pagination: false,
			slideSpeed : 200,
			autoPlay:true,
			responsiveRefreshRate : 200,
			transitionStyle : "fade",
			loop:true,
			navigation:true,
			autoplayHoverPause:true
		});
		jQuery('.brand').owlCarousel(
		{
			items: 6,
			itemsDesktop: [1199, 6],
			itemsDesktopSmall: [991, 4],
			itemsTablet: [768, 4],
			itemsMobile: [479, 2],
			lazyLoad: true,
			pagination: false,
			slideSpeed : 200,
			autoPlay:true,
			responsiveRefreshRate : 200,
			transitionStyle : "fade",
			loop:true,
			navigation:false,
			autoplayHoverPause:true
		});


$(window).scroll(function() {
    if ($(this).scrollTop() >= 400) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(400);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(400);   // Else fade out the arrow
    }
});


// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top1').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top1').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top1').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

$(function () {
                        // Get page title
                        var pageTitle = $("title").text();

                        // Change page title on blur
                        $(window).blur(function () {
                            setTimeout(function () {
                                var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
                                link.type = 'image/x-icon';
                                link.rel = 'shortcut icon';
                                link.href = '<?php echo base_url(); ?>/web_assets/img/RM icon.png';
                                document.getElementsByTagName('head')[0].appendChild(link);

                                var isOldTitle = true;
                                var oldTitle = $("title").text();
                                var newTitle = "Continue Reading...";
                                var interval = null;
                                function changeTitle() {
                                    document.title = isOldTitle ? oldTitle : newTitle;
                                    if (isOldTitle) {
                                        $("link[rel*='icon']").prop("href", '<?php echo base_url(); ?>/web_assets/img/favicon.png');
                                    } else {
                                        $("link[rel*='icon']").prop("href", '<?php echo base_url(); ?>/web_assets/img/RM icon.png');
                                    }
                                    isOldTitle = !isOldTitle;
                                }
                                interval = setInterval(changeTitle, 700);

                                $(window).focus(function () {
                                    clearInterval(interval);
                                    $("title").text(oldTitle);
                                    $("link[rel*='icon']").prop("href", '<?php echo base_url(); ?>/web_assets/img/favicon.png');
                                });
                            }, 90000);
                        });

                        // Change page title back on focus
                        $(window).focus(function () {
                            $("title").text(pageTitle);
                        });
                    });
	</script>
</html>