<!doctype html>
<html lang="en">
<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91453747-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-91453747-1');
	</script>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'web_assets/' ?>img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo base_url().'web_assets/' ?>img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> Page Doesn't Exist | Reports Monitor</title>
	<?php if (isset($Record['meta_keyword'])): ?>
		<meta name="keywords" content="<?=$Record['meta_keyword'] ?>">
	<?php endif ?>
	<?php if (isset($Record['meta_desc'])): ?>
		<meta name="description" content="<?=$Record['meta_desc'] ?>">
	<?php endif ?>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<!--     Fonts and icons     -->	
	<link rel="stylesheet" href="<?php echo base_url().'web_assets/' ?>css/font-awesome.min.css" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<!-- CSS Files -->
	<link href="<?php echo base_url().'web_assets/' ?>css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url().'web_assets/' ?>css/mStyle.css" rel="stylesheet"/>	
	<link href="<?php echo base_url().'web_assets/' ?>css/owl.carousel.css" rel="stylesheet"/>
	<link href="<?php echo base_url().'web_assets/' ?>css/owl.transitions.css" rel="stylesheet"/>
	<link href="<?php echo base_url() . 'web_assets/' ?>css/allinone.css" rel="stylesheet"/>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<style>
		.brdrright {
			background: url(<?php echo base_url().'web_assets/' ?>images/line.png);
			background-repeat: no-repeat;
			background-position: right ;
			min-height: 230px ;
		}
	</style>
</head>
<body class="index-page">
	<div class="container-fluid topnav">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12 mailclr">
					<a href="mailto:sales@reportsmonitors.com">sales@reportsmonitor.com</a>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<ul class="list-inline continfo pull-right">
						<li> +1 513 549 5911 (U.S.)</li>
						<li class="hidden-xs">|</li>
						<li>+91 8087085354 (India)</li>
						<li class="hidden-xs">|</li>
						<li>+44 203 318 2846 (U.K.)</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default" color-on-scroll="" id="sectionsNav">
		<div class="container">
			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?php echo base_url().'web_assets/' ?>images/logo.png" class="img-responsive" alt=""></a>
				<!-- <a class="navbar-brand" href="index.html"><img src="<?php echo base_url().'web_assets/' ?>images/logo.png" class="img-responsive" alt=""></a> -->
			</div>

			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url()?>">Home</a></li>
					<li><a href="<?=base_url() ?>about-us">About Us</a></li>
					<li><a href="<?=base_url() ?>services">Our Services</a></li>
					<li><a href="<?=base_url() ?>categories">Categories</a></li>
					<li><a href="<?=base_url() ?>publishers">Publishers</a></li>
					<li><a href="<?=base_url() ?>pressRelease">Press Release</a></li>
					<li><a href="<?=base_url() ?>contact-us">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="page-header2 imgradius" data-parallax="false" style="background-image: url('<?php echo base_url().'web_assets/' ?>images/categoriesBg.jpg');">
		<div class="container">
			<div class="row pt-xs-10">            
				<div class="col-md-12 col-md-12 col-xs-12 cateclr pt-40 pt-xs-10">
					<form method="get" action="<?=base_url()?>search">
						<div class="row">
							<div class="col-md-6">     
								<input type="text" name="search" style="height: 25px;" placeholder="Search for Reports" <?php if(isset($searchFor)) echo 'value="'.$searchFor.'"';?> class="form-control searchform "> 
							</div>
							<div class="col-md-3">
								<select class="form-control slectdrop " style="height: 25px;" name="category">
									<option class="otherst" value="NULL">Select Category</option>
									<?php foreach ($categories as $category): ?>
										<option class="otherst" value="<?=$category['id']?>" <?php if(isset($cat_id) && $cat_id == $category['id'])  echo 'selected' ?>><?=$category['name'] ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="col-md-3">
								<input type="submit" class="btn btn-primary  searchbtn" value="Search">
							</div>
						</div>
					</form>
				</div>       
			</div>
		</div><!--serach bar ends here-->
	</div>
	<?php 
	$categories = $this->data_model->get(NULL, NULL, array('id', 'name'), NULL, 'category');
	?>
	<hr class="mt-0" style="border-bottom:2px solid #ECEFEF">
	<?php 
	$a = mt_rand(0,9);
	$b = mt_rand(0,9);
	$c =$a + $b;
	?>
	<div class="container">       
		<div class="row mt-0">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="fortitle">Oops! That page can’t be found</h1>
				<p>
					We apologize for the inconvenience.
					The website has been updated with the latest research and market reports for improvement.

					In the search box above, search for your desired market research.


					Please fill out the form below and one of our staff will contact you as soon as possible.
					thank you.
				</p>
				<p>불편을 끼쳐 드려 죄송합니다.
					웹 사이트는 개선을위한 최신 연구 및 시장 보고서로 업데이트되었습니다.

					위 검색 창에서 원하는 시장 조사를 검색하십시오.


					아래 양식을 작성해 주시면 담당자 중 한 사람이 가능한 한 빨리 연락 드리겠습니다.
				고맙습니다.</p>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 bgclr">
				<br>
				<?php if ($this->session->flashdata('flashSuccess')): ?>
					<div class="alert alert-danger">
						<strong>Info!</strong> <?php echo $this->session->flashdata('flashSuccess') ?>
					</div>
				<?php endif ?>
				<form method="POST">
					<div class="col-md-1 col-sm-1 "></div>
					<div class="col-md-5 col-sm-5 col-xs-12 mt-10">
						<div class="form-group is-empty">
							<input type="text" placeholder="&nbsp;&nbsp;Name" name="name" class="inp_dsgn" value="<?php echo set_value('name'); ?>" required>
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('name'); ?>

						</div>
						<div class="form-group is-empty">
							<input type="email" placeholder="&nbsp;&nbsp;Email" name="email" class="inp_dsgn" value="<?php echo set_value('email'); ?>" required>
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('email'); ?>
						</div>
						<div class="form-group is-empty">
							<input type="text" placeholder="&nbsp;&nbsp;Contact Number" name="phone" class="inp_dsgn" value="<?php echo set_value('phone'); ?>" required>
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('phone'); ?>
						</div>
						<div class="form-group is-empty">
							<input type="text" placeholder="&nbsp;&nbsp;Title / Designation" name="job_title" class="inp_dsgn" required="" value="<?php echo set_value('job_title'); ?>">
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('job_title'); ?>
						</div>
						<div class="form-group is-empty">
							<input type="text" placeholder="&nbsp;&nbsp;Company" name="company" class="inp_dsgn" required="" value="<?php echo set_value('company'); ?>">
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('company'); ?>
						</div>	
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12 mt-10">
						<div class="form-group is-empty">
							<input type="text" placeholder="&nbsp;&nbsp;Subject/Report Tittle" name="sub" class="inp_dsgn" value="<?php echo set_value('sub'); ?>" required>
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('sub'); ?>
						</div>
						<div class="form-group is-empty">
							<textarea placeholder="&nbsp;&nbsp; Your Requirement" class="inp_dsgn" name="msg" rows="4" required=""><?php echo set_value('msg'); ?></textarea>
							<?php $this->form_validation->set_error_delimiters('<span class=error>', '</span>');
							echo form_error('msg'); ?>
						</div>
						<div class="form-group is-empty mt-10">		
							<!-- <div class="g-recaptcha" data-sitekey="6LcvGGYUAAAAAHi9R1s48rTNPSK9CVMOBXHsxTo3"></div>	 -->
							<div class="g-recaptcha" data-sitekey="6Ld2ziIaAAAAAGpEJ5lmatDxAue0fyyRwshU0mgx"></div>
						</div>
					</div>
					<div class="col-md-12 col-xs-12 mt-10 text-center">
						<input type="submit" id="submit" class="btn btn-info" value="Submit">
					</div>
				</form>
			</div> 
		</div>
	</div>
	<?php $this->load->view('templates/web_footer') ?>
<!-- <script type="text/javascript">
	$(document).ready(function() 
	{
		var res = <?=$c?>;
		var v = 0;
		var checked_status = 0;
		$("#security").keyup(function() {
			v = this.value;
		
			if (v == res)
			{
				$("#submit").removeAttr("disabled");
			}
			else
			{
				$("#submit").attr("disabled", "disabled");
			}
		});
	});
</script> -->

