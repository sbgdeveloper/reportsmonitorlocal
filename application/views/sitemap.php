<?php header('Content-type: text/xml'); ?>
<?php echo'<?xml version="1.0" encoding="UTF-8" ?>' ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc><?php echo base_url() . "main_pages"; ?></loc>
    </sitemap>
    <!-- Category Sitemap -->
    <?php foreach ($items as $item) { ?>
        <?php if ($item->id != 4): ?>
            <?php
            $startDate = time();
            $beforeseven = date('Y-m-d', strtotime('-7 day', $startDate));
            $beforeseven = $beforeseven . " 00:00:00";

//            echo $beforeseven;die();
            $query = $this->db->query('SELECT id FROM report where cat_id = ' . $item->id . ' and created_at > " ' . $beforeseven . ' " order by created_at DESC');
            // print_r(__LINE__);die;
            $count = $query->num_rows();

            if ($count > 10000) {
                $npage = intval($count / 10000);
            } else {
                $npage = 0;
            }
            ?>
            <?php if ($npage < 1): ?>
                <sitemap>
                    <loc>
                        <?php echo base_url() . 'sitemaps' . "/" . $item->id . '/' . str_replace(' ', '_', $item->name) . '.xml' ?>
                    </loc>
                </sitemap>
            <?php else: ?>
                <?php for ($i = 0; $i <= $npage; $i++) : ?>
                    <sitemap>
                        <loc>
                            <?php echo base_url() . 'catgory-sitemap' . "/" . $item->id . "/" . $i . '/' . str_replace(' ', '_', $item->name) . '.xml' ?>
                        </loc>
                    </sitemap>
                <?php endfor; ?>
            <?php endif ?>
        <?php endif ?>

    <?php } ?>
    <!-- End Category -->
    <sitemap>
        <loc><?php echo base_url() . "sitemap_blog"; ?></loc>
    </sitemap>
    <sitemap>
        <loc><?php echo base_url() . "sitemap_pressRelease"; ?></loc>
    </sitemap>
</sitemapindex>