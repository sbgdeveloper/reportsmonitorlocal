<?php

require('config.php');
require('razorpay-php/Razorpay.php');
session_start();

// Create the Razorpay Order

use Razorpay\Api\Api;

$api = new Api($keyId, $keySecret);

//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//
$inDoller = $_POST["amount"];
$inRs = $inDoller * 71.26;
$orderData = [
    'receipt' => 3456,
    'amount' => $inRs * 100, // 2000 rupees in paise
    'currency' => 'INR',
    'payment_capture' => 1 // auto capture
];

$razorpayOrder = $api->order->create($orderData);

$razorpayOrderId = $razorpayOrder['id'];

$_SESSION['razorpay_order_id'] = $razorpayOrderId;

$displayAmount = $amount = $inDoller;
//if ($displayCurrency !== 'INR') {
//    $url = "http://data.fixer.io/api/latest?access_key=39938eee5ce4c1c092ec395c12361842&symbols=USD&base=INR";
//    $exchange = json_decode(file_get_contents($url), true);
//    $dollar = 70;
////    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
//    $displayAmount = $inDoller;
//}

$checkout = 'automatic';

if (isset($_POST['checkout']) and in_array($_POST['checkout'], ['automatic', 'manual'], true)) {
    $checkout = $_POST['checkout'];
}
$data = [
    "key" => $keyId,
    "amount" => round($orderData['amount']),
    "name" => "Yuvraj Jadhav",
    "currency" => "USD",
    "description" => "Report Payment",
    "image" => "https://www.reportsmonitor.com/web_assets/images/logo.png",
    "prefill" => [
        "name" => "Yuvraj Jadhav",
        "email" => "yurvaj.j@straitsresearch.net",
        "contact" => "91 999 999 999",
    ],
    "notes" => [
        "address" => "INIDA",
        "merchant_order_id" => "12312321",
    ],
    "theme" => [
        "color" => "#F37254"
    ],
    "order_id" => $razorpayOrderId,
];

if ($displayCurrency !== 'INR') {
    $data['display_currency'] = $displayCurrency;
    $data['display_amount'] = $displayAmount;
}

$json = json_encode($data);
require("checkout/{$checkout}.php");
