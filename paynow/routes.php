<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * 
 * Default routes
 * 
 * 
 */

$route['default_controller'] = 'web';
$route['404_override'] = 'web/pagenotfound';
$route['translate_uri_dashes'] = FALSE;


/*
 * login routes
 * 
 * 
 */
$route['admin/login'] = 'LoginController/validate_credentials';
$route['admin/logout'] = 'LoginController/logout';
$route['admin/home'] = 'DashboardController';


/*
 * UserManagement Routes
 */

$route['add/user'] = 'UserController/add/$1';
$route['edit/user/(:any)'] = 'UserController/edit/$1';
$route['list/user'] = 'UserController/viewList/$1';

/*
 * Report Management Routes
 */

$route['add/report'] = 'ReportController/add';
$route['edit/report/(:any)'] = 'ReportController/edit/$1';
$route['list/report'] = 'ReportController/viewList/$1';
$route['upload'] = 'ReportController/upload';

/*
 * WebPageRoutes
 */

$route['add/pages/(:any)'] = 'WebPagesController/add/$1';
$route['edit/pages/(:any)'] = 'WebPagesController/edit_pages/$1';
$route['list/pages/(:any)'] = 'WebPagesController/list/$1';
$route['edited/pages/(:any)/(:any)'] = 'WebPagesController/edit_pages/$1/$2';
$route['delete/pages/(:any)/(:any)'] = 'WebPagesController/delete/$1/$2';

/*
 * Category Routes
 */
$route['add/category'] = 'CategoryController/add/$1';
$route['edit/category/(:any)'] = 'CategoryController/edit/$1';
$route['list/category'] = 'CategoryController/viewList/$1';

/*
 * Sub Category Routes
 */
$route['add/subcategory'] = 'SubCategoryController/add/$1';
$route['edit/subcategory/(:any)'] = 'SubCategoryController/edit/$1';
$route['list/subcategory'] = 'SubCategoryController/viewList/$1';

/*
 * Admin panel routes
 * 
 * 
 */

/*
 * Regions routes
 * 
 */

$route['add/regions'] = 'RegionController/add/$1';
$route['edit/regions/(:any)'] = 'RegionController/edit/$1';
$route['list/regions'] = 'RegionController/viewList/$1';
//$route['disable/regions'] = 'RegionController/disable/$1';
//$route['enable/regions'] = 'RegionController/enable/$1';


/*
 * PressRelease routes
 * 
 */

$route['add/pressrelease'] = 'PressReleaseController/add/$1';
$route['edit/pressrelease/(:any)'] = 'PressReleaseController/edit/$1';
$route['list/pressrelease'] = 'PressReleaseController/viewList/$1';
//$route['disable/pressrelease'] = 'PressReleaseController/disable/$1';
//$route['enable/pressrelease'] = 'PressReleaseController/enable/$1';

/*
 * Blogs routes
 * 
 */

$route['add/blogs'] = 'BlogController/add/$1';
$route['edit/blogs/(:any)'] = 'BlogController/edit/$1';
$route['list/blogs'] = 'BlogController/viewList/$1';
//$route['disable/blogs'] = 'BlogController/disable/$1';
//$route['enable/blogs'] = 'BlogController/enable/$1';



/*
 * Blogs routes
 * 
 */

$route['add/client'] = 'ClientController/add/$1';
$route['edit/client/(:any)'] = 'ClientController/edit/$1';
$route['list/client'] = 'ClientController/viewList/$1';
//$route['disable/client'] = 'ClientController/disable/$1';
//$route['enable/client'] = 'ClientController/enable/$1';
//$route['upload/(:any)'] = 'back/upload/$1';
$route['user'] = 'back/index/user';
$route['admin'] = 'LoginController';
$route['navigation'] = 'back/navigation';
$route['press'] = 'back/press';

/* Enable Disable */

$route['enable/(:any)'] = 'CommonController/enable/$1';
$route['disable/(:any)'] = 'CommonController/disable/$1';
$route['delete/(:any)'] = 'CommonController/delete/$1';

/*
 *
 *
 *
 */

/*
 * 
 * Front End Routing
 * 
 *
 */
$route['about-us'] = 'web/about';
$route['service'] = 'ServicesController/services';
$route['service/(:any)'] = 'ServicesController/services/$1';
$route['thankyou'] = 'web/thankyou';
$route['contact-us'] = 'web/contact';
$route['whitepaper'] = 'web/whitepaper';
$route['whitepapers'] = 'web/whitepapers';
$route['case-studies'] = 'web/case_studies';
$route['case-study'] = 'web/case_study';
$route['paymentResponce'] = 'web/paymentResponce';
$route['paypalResp'] = 'web/paypalResp';


/* pages */
$route['term'] = 'web/page/term';
$route['FAQ'] = 'web/page/faq';
$route['disclaimer'] = 'web/page/disclaimer';
$route['policy'] = 'web/page/policy';
$route['return-policy'] = 'web/page/return_policy';
$route['how-to-order'] = 'web/page/how_to_order';
$route['format-delivery'] = 'web/page/format_delivery';
/* End Pages */

/*
 *  Category 
 */

$route['categories'] = 'FilterController/categories';
$route['category/(:any)'] = 'FilterController/category/$1';
$route['category/(:any)/(:any)'] = 'FilterController/category/$1/$2';
$route['category/(:any)/(:any)/(:any)'] = 'FilterController/category/$1/$2/$3';
$route['filter/search'] = 'FilterController/searchData';
$route['filter/filterview'] = 'FilterController/filterData';

/*
 * Blogs
 * 
 */
$route['blogs'] = 'web/blogs';
$route['blogs/(:any)'] = 'web/blogs/$1';
$route['blog/(:any)'] = 'web/blog/$1';
$route['blog/(:any)/(:any)'] = 'web/blog/$1/$2';

/*
 * Press release
 * 
 */
$route['press-release'] = 'web/press_release';
$route['press-release/(:any)'] = 'web/press_release/$1';
$route['press-releases/(:any)'] = 'web/press_releases/$1';
$route['press-releases/(:any)/(:any)'] = 'web/press_releases/$1/$2';

$route['post'] = 'Posts/index';
/*
 * 
 * 
 * Reports
 * 
 */
$route['report'] = 'ReportFrontController/report';
$route['reports/(:any)'] = 'ReportFrontController/reports/$1';
$route['reports/(:any)/(:any)'] = 'ReportFrontController/reports/$1/$2';
$route['request-sample/(:any)'] = 'ReportFrontController/request_sample/$1';
$route['check-discount/(:any)'] = 'ReportFrontController/check_discount/$1';
$route['buynow/(:any)'] = 'ReportFrontController/buynow/$1';
$route['before'] = 'ReportFrontController/before';

/*
 * 
 * Print
 * 
 * 
 */

$route['sampleprint/(:any)/(:any)'] = 'web/sampleprint/$1/$2';
$route['createpdf/(:any)'] = 'Createpdf/create_pdf/$1';
/*
 * 
 * search routes
 * 
 */
$route['search'] = 'web/search';
$route['search/(:any)'] = 'web/search/$1';
$route['search/(:any)/(:any)'] = 'web/search/$1/$2';

/*
 * 
 *  Domain 
 */
$route['domains']     = 'DomainController/domains';
$route['domains/(:any)']     = 'DomainController/domains/$1';
$route['domain/(:any)']     = 'DomainController/domain/$1';
$route['domain/(:any)/(:any)']     = 'DomainController/domain/$1/$2';
/*
 * Common Routes
 */

/*
 * Paypal Routes
 */
$route['paydata'] = 'Paypal/index';
$route['buyNow/(:any)'] = 'web/buyNow/$1';
$route['paypal/getPaymentStatus'] = 'Paypal/getPaymentStatus';
$route['paypal/getPaymentStatus'] = 'Paypal/getPaymentStatus';
$route['paypal/success'] = 'Paypal/success';
$route['paypal/cancel'] = 'Paypal/cancel';
$route['paypal/refund_payment'] = 'Paypal/refund_payment';


/*
 * 
 * SEO
 * 
 */
$route['sitemap\.xml'] = "seo/sitemap";
$route['sitemaps/(:any)/(:any)'] = "seo/sitemaps/$1/$2";
$route['feed'] = 'seo/feed';
$route['(:any)'] = 'seo/$1';

$route['(:any)'] = 'paynow/$1';